'use strict';
angular.module('dakshinHondaWebsiteApp').controller('CardetailCtrl', function ($sessionStorage,$scope, localStorageService, $location, $http, growl, $mdDialog, $rootScope) {
    window.scrollTo(0, 0);
    $scope.dataLoaded = !1;
    $scope.$on('$routeChangeStart', function (scope, next, current) {
        if (next.$$route.controller != "CardetailCtrl") {
            $mdDialog.cancel({})
        }
    });
    $scope.dataToSend = {};
    $scope.allGalleryImages = {};
    $scope.interior = {};
    $scope.exterior = {};
    $scope.slideContent = {};
    $scope.fueltypes = [];
    $scope.variants = {};
    $scope.variantsMenuList = [];
    $scope.finaltransmission = {};
    $scope.finalfueltype = {};
    $scope.variantsToShow = {};
    $scope.downloadBrochure = function (carId) {
        console.log(carId);
        $scope.data = {};
        $scope.data.car_id = carId;
        console.log(JSON.stringify($scope.data));
        $http.post(api_url + "/website/get_broucher_url/", $scope.data).then(function (success) {
            console.log("res",success)
             window.open(success.data.car_brochure_url, '_system', 'location=no');
            
        }, function (error) {
            console.log(JSON.stringify(error))
        })['finally'](function () {
            $scope.test = !1
        })
    }
    $scope.currentIndex = 0;
    $scope.setCurrentSlideIndex = function (index) {
        $scope.currentIndex = index
    };
    $scope.isCurrentSlideIndex = function (index) {
        return $scope.currentIndex === index
    };
    $scope.prevSlide = function () {
        $scope.currentIndex = ($scope.currentIndex < $scope.slideContent.length - 1) ? ++$scope.currentIndex : 0
    };
    $scope.nextSlide = function () {
        $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slideContent.length - 1
    };
    var carId = localStorageService.get('car_id', '');
    var isTaxi = localStorageService.get('isTaxi', '');
    $scope.dataToSend.id = carId;
    $scope.dataToSend.city = "Bengaluru";
    $http.post(api_url + "/website/car_with_location", $scope.dataToSend).then(function (success) {
        $scope.car = success.data.car;
        $scope.allGalleryImages = $scope.car.web_galleries;
        $scope.sortGalleryImages($scope.allGalleryImages);
        var variantsArray = [];
        if (isTaxi == 1) {
            angular.forEach($scope.car.variants, function (key, value) {
                if (key.variant.board == 'yellow') {
                    variantsArray.push(key)
                }
                $scope.variants = variantsArray
            })
        } else {
            angular.forEach($scope.car.variants, function (key, value) {
                if (key.variant.board == 'white') {
                    variantsArray.push(key)
                }
                $scope.variants = variantsArray
            })
        }
        $scope.variantsToShow = $scope.variants;
        $scope.variantsMenu($scope.variantsToShow);
        $scope.sortColorImages($scope.car.web_colors)
    }, function (error) {
        console.log(JSON.stringify(error))
    })['finally'](function () {
        $scope.dataLoaded = !0;
        pageDataLoaded()
    });
    var tempColorList = [];
    $scope.web_colors = {};
    $scope.initialImage = {};
    $scope.sortColorImages = function (colorData) {
        $scope.webColors = colorData;
        var numOfColors = $scope.webColors[0].s3_image_url.length;
        $scope.initialImage = $scope.webColors[0].s3_image_url[0].img1;
        for (var i = 0; i < numOfColors; i++) {
            tempColorList.push({
                'colorImage': $scope.webColors[0].s3_image_url[i].img1,
                'colorPalletImage': $scope.webColors[0].s3_pallet_image_url[i].img2,
                'colorName': $scope.webColors[0].color_name[i].color
            })
        }
        $scope.web_colors = tempColorList
    }
    $scope.changeColor = function (colorData) {
        $scope.initialImage = colorData.colorImage
    }
    var tempImageListInterior = [];
    var tempImageListExterior = [];
    $scope.sortGalleryImages = function (allGalleryImages) {
        $scope.images = allGalleryImages;
        var tempImageList = [];
        $scope.imageList = {};
        var imgLength = $scope.images[0].image_url.length;
        for (var i = 0; i < imgLength; i++) {
            tempImageList.push({
                'image': $scope.images[0].image_url[i],
                'category': $scope.images[0].diff_int_ext[i].category
            })
        }
        $scope.imageList = tempImageList;
        angular.forEach($scope.imageList, function (key, value) {
            if (key.category == "Interior" || key.category == "interior") {
                tempImageListInterior.push({
                    'image': key.image,
                    'category': key.category
                })
            } else if (key.category == "Exterior" || key.category == "exterior") {
                tempImageListExterior.push({
                    'image': key.image,
                    'category': key.category
                })
            }
        });
        $scope.interior = tempImageListInterior;
        $scope.interiorCount = $scope.interior.length;
        $scope.exterior = tempImageListExterior;
        $scope.exteriorCount = $scope.exterior.length;
        $scope.slideContent = $scope.interior;
        $scope.firstInt = $scope.interior[1];
        $scope.firstExt = $scope.exterior[1]
    };
    $scope.changeSliderContent = function (data) {
        if (data == "int") {
            $scope.slideContent = {};
            $scope.slideContent = $scope.interior
        } else {
            $scope.slideContent = {};
            $scope.slideContent = $scope.exterior
        }
    }
    var tempvariantsMenutransmission = [];
    var tempvariantsMenufueltype = [];
    $scope.variantsMenu = function (allvariants) {
        $scope.data = allvariants;
        console.log(JSON.stringify($scope.data));
        angular.forEach($scope.data, function (key, value) {
            console.log(JSON.stringify(key.variant));
            tempvariantsMenutransmission.push({
                'transmission': key.variant.transmission_type
            })
            tempvariantsMenufueltype.push({
                'fuel_type': key.variant.fuel_type
            })
        });
        $scope.finaltransmission = tempvariantsMenutransmission;
        $scope.finalfueltype = tempvariantsMenufueltype
    }
    var temptransmissionlist = [];
    var tempfuel_typeList = [];
    $scope.filter = function (tOrF, category) {
        $scope.data = $scope.variants;
        $scope.variantsToShow = {};
        temptransmissionlist = [];
        tempfuel_typeList = [];
        angular.forEach($scope.data, function (key, value) {
            if (tOrF == 'tr') {
                if (key.variant.transmission_type == category) {
                    temptransmissionlist.push(key)
                }
                $scope.variantsToShow = temptransmissionlist
            } else if (tOrF == 'fu') {
                if (key.variant.fuel_type == category) {
                    tempfuel_typeList.push(key)
                }
                $scope.variantsToShow = tempfuel_typeList
            } else {
                $scope.variantsToShow = $scope.variants
            }
        })
    }
    $scope.showPriceModal = function (ev, priceData) {
        $rootScope.priceData = priceData;
        $mdDialog.show({
            controller: priceController,
            templateUrl: 'views/price.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: !0,
            fullscreen: $scope.customFullscreen
        }).then(function (answer) { }, function () { })
    };

    function priceController($scope, $mdDialog, $sessionStorage) {
        $scope.variantPriceData = {};
        $scope.variantPriceData = $rootScope.priceData.variant_price;
        $scope.closeCarModal = function () {
            $mdDialog.cancel({})
        }
    }
    $scope.showFeatureModal = function (ev, featureData) {
        $rootScope.featureData = featureData;
        $mdDialog.show({
            controller: featureController,
            templateUrl: 'views/feature.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: !0,
            fullscreen: $scope.customFullscreen
        }).then(function (answer) { }, function () { })
    };

    function featureController($scope, $mdDialog, $sessionStorage) {
        $scope.variantfeatureData = {};
        $scope.variantfeatureDataData = $rootScope.featureData;
        $scope.closeCarModal = function () {
            $mdDialog.cancel({})
        }
    }
    $rootScope.$on('afterEmailPrice', function (event, email) {
        var msg = 'Price quote sent to ' + email + '!';
        setTimeout(function () {
            callnotify(msg, 'success')
        }, 1000)
    });

    $scope.getPriceQuote = function (ev, variantsToShow, growl) {
        $rootScope.variantsToShow = variantsToShow;
        $mdDialog.show({
            controller: mailVehiclePrice,
            templateUrl: 'views/priceRequest.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: !0,
            fullscreen: $scope.customFullscreen
        }).then(function (answer) { }, function () { })
    }

    function mailVehiclePrice($scope, $mdDialog, $sessionStorage, growl) {
        $scope.sendEmailData = {};
        $scope.variantsToShow = $rootScope.variantsToShow;
        $scope.sendEmail = function () {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if ($sessionStorage.DHuserSession) {
                $scope.sendEmailData.user_id = $sessionStorage.DHuserSession.id
            } else {
                $scope.sendEmailData.user_id = ""
            }
            if (!$scope.sendEmailData.name) {
                setTimeout(function () {
                    callnotify('Error : Name Required', 'error')
                }, 1000);
                $mdDialog.cancel({})
            } else if (!$scope.sendEmailData.email) {
                setTimeout(function () {
                    callnotify('Error : Email Id Required', 'error')
                }, 1000);
                $mdDialog.cancel({})
            } else if (reg.test($scope.sendEmailData.email) == !1) {
                setTimeout(function () {
                    callnotify('Invalid Email Address', 'error')
                }, 1000);
                $mdDialog.cancel({})
            } else if (!$scope.sendEmailData.mobile_number) {
                setTimeout(function () {
                    callnotify('Error : Mobile Number Required', 'error')
                }, 1000);
                $mdDialog.cancel({})
            } else if (!$scope.sendEmailData.id) {
                setTimeout(function () {
                    callnotify('Error : Select Variant', 'error')
                }, 1000);
                $mdDialog.cancel({})
            } else {
                $scope.send = {};
                $scope.send.enquiry = $scope.sendEmailData;
                $http.post(api_url + "/mobile/email_price", $scope.sendEmailData).then(function (success) {
                    $rootScope.$emit('afterEmailPrice', $scope.sendEmailData.email)
                }, function (error) {
                    console.log(JSON.stringify(error))
                })['finally'](function () {
                    $mdDialog.cancel({})
                })
            }
        }
        $scope.closePriceModal = function () {
            $mdDialog.cancel({})
        }
    }
    $scope.showCompareButton = !1;
    $rootScope.variant_List = [];

    function isInArray(value, array) {
        return array.indexOf(value) > -1
    }
    $scope.isCarSelected = function (variant_id) {
        if (variant_id) {
            if (isInArray(variant_id, $rootScope.variant_List) == !1) {
                $rootScope.variant_List.push(variant_id)
            } else {
                var index = $rootScope.variant_List.indexOf([carId, variant_id]);
                if (index > -1) {
                    $rootScope.variant_List.splice(index, 1)
                }
            }
            if ($rootScope.variant_List.length >= 2 && $rootScope.variant_List.length <= 3) {
                $scope.showCompareButton = !0
            } else {
                if ($rootScope.car_List.length > 3) {
                    callnotify("Maximum 3 cars can be compared.", 'warning')
                }
                $scope.showCompareButton = !1
            }
        }
    }
    $scope.goToCompare = function (carId) {
        $rootScope.query = [];
        $scope.temp = {};
        for (var i = 0; i < $rootScope.variant_List.length; i++) {
            $scope.temp.car_id = carId;
        
            $scope.temp.variant_id = $rootScope.variant_List[i];
            $rootScope.query.push($scope.temp);
            $scope.temp = {}
        }
        $location.path('/carcompare')
    }
    $scope.booknow=function(cardata){
     console.log("cars",cardata);
     $rootScope.booking_car_data=cardata;
          if ($sessionStorage.DHuserSession) {
            $location.path('/manualpayment')
        } else {
            callnotify('Please log in to make payments.', 'information')
        }
    }
        $scope.booktestdrive=function(car_id){
        console.log("carid",car_id);
 $rootScope.frombannerData=car_id;
        $location.path('/testDrive');
    }
})