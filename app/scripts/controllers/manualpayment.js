'use strict';
angular.module('dakshinHondaWebsiteApp').controller('manualpaymentCtrl', function($scope, $location, $localStorage, $sce, $http, growl, $sessionStorage, $rootScope, localStorageService, $filter, $timeout) {
    window.scrollTo(0, 0);
    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src)
    }
    $scope.makepayment = function() {
        $location.path('/manualpayment')
    }
    $scope.paymentdue = function() {
        $location.path('/paymentdue')
    }
    $scope.mypayments = function() {
        $location.path('/mypayments')
    }
    $scope.testDriveFormdata = {};
    var serviceStation = [];
    $scope.manPayOpt = !1;
    $scope.user = {};
    $scope.carsList = {};
    $scope.sendData = {};
    $scope.paymentdata = {};
    $scope.directbooking=false;
    $("#vwheader").show();
    $("#vwnavbar").show();
    $("#vwfooter").show();
    $('.inputText').prop('disabled', !1);


    $scope.checkDrpDwnValue = function() {
        if (!$scope.testDriveFormdata.entity_type) {
            $scope.manPayOpt = !0;
            callnotify('Please select payment for', 'error')
        } else {
            $scope.manPayOpt = !1
        }
    }
    var status;
    if (!$sessionStorage.DHuserSession) {
        $location.path('/main')
    }
    $scope.test = !0;
    $scope.message = 'Loading Service Stations...';
    $scope.dealer_data = {};
    $scope.dealer_data_master = {};
    var auth = '';
    $http.get(api_url + "/website/dealers").then(function(success) {
        var serviceStationData = success.data.dealers;
        $scope.dealer_data_master = success.data.dealers;
        console.log("dealer_data_master",$scope.dealer_data_master);
        for (var i = 0; i < serviceStationData.length; i++) {
            serviceStation.push(serviceStationData[i].dealer.dealer_name)
        }
        $scope.serviceStation = serviceStation
    }, function(error) {})['finally'](function() {
        $scope.test = !1
    });

    function updateDealers() {
        console.log("enterd",$scope.testDriveFormdata.entity_type);
        var type = $scope.testDriveFormdata.entity_type;
        var temp = [];
        $scope.dealer_data = {};
        $scope.merchantdata = {};
        var merchantdataarr = [];

        function checkifObjExists(obj) {
            if (merchantdataarr.length == 0) {
                merchantdataarr.push(obj)
            }
            for (var i = 0; i < merchantdataarr.length; i++) {
                if (JSON.stringify(merchantdataarr[i]) === JSON.stringify(obj)) {} else {
                    merchantdataarr.push(obj)
                }
            }
        }
        var paytype = {
            "payment_type": type
        }
        console.log(JSON.stringify(paytype));
        $http.post("https://dakshin-backend.myridz.com/mobile/merchant_accounts_with_type", paytype, {
            headers: {
                'X-USER-TOKEN': $sessionStorage.DHuserSession.authentication_token,
                'X-USER-EMAIL': $scope.userFeedbcakData.email
            }
        }).then(function(success) {
            var res = success.data;
            console.log("res...",(res));
            for (var i = 0; i < $scope.dealer_data_master.length; i++) {
                for (var j = 0; j < res.dealer_bank_accounts.length; j++) {
                    if ($scope.dealer_data_master[i].dealer_name == res.dealer_bank_accounts[j].name) {
                        if (res.dealer_bank_accounts[j].payment_for == 'Website') {
                            checkifObjExists($scope.dealer_data_master[i])
                        }
                    }
                }
            }
            $scope.dealer_data = merchantdataarr;
            console.log(JSON.stringify($scope.dealer_data))
        }, function(error) {
            console.log(error)
        })
    }

    function updateTestDriveData() {
        $scope.userFeedbcakData = $sessionStorage.DHuserSession.profile;
        $scope.testDriveFormdata.name = $scope.userFeedbcakData.full_name;
        $scope.testDriveFormdata.email = $scope.userFeedbcakData.email;
        $scope.testDriveFormdata.mobile = parseInt($scope.userFeedbcakData.mobile);
        $scope.testDriveFormdata.user_id = $sessionStorage.DHuserSession.id;
        $scope.testDriveFormdata.address = $scope.userFeedbcakData.address
    }
    if ($sessionStorage.DHuserSession) {
        updateTestDriveData()
    }
    setTimeout(function() {
        if ($localStorage.DHpaymentdueid) {
            status = 1;
            $http.post(api_url + "/website/get_payment", JSON.stringify({
                "id": $localStorage.DHpaymentdueid
            })).then(function(success) {
                console.log(JSON.stringify(success.data));
                $scope.paymentdata = success.data;
                var paymentdata = success.data;
                $scope.paymentcity = $scope.paymentdata.merchant_location;
                var carid = $scope.paymentdata.car_id;
                setTimeout(function() {
                    if (typeof(carid) != "undefined" && carid != null) {
                        $scope.testDriveFormdata.car_id = carid.toString()
                    }
                }, 300);
                $scope.testDriveFormdata.amount = parseFloat($scope.paymentdata.amount);
                $scope.dealer_data = $scope.dealer_data_master;
                $scope.testDriveFormdata.dealer_name = $scope.paymentdata.merchant_name;
                $scope.testDriveFormdata.dealer_id = $scope.paymentdata.dealer_id;
                $scope.testDriveFormdata.Comments = $scope.paymentdata.payment_comments;
                $scope.testDriveFormdata.entity_type = $scope.paymentdata.entity_type;
                var type = $scope.paymentdata.entity_type;
                if (type == "Booking Down-payment" || type == "Other") {
                    $scope.test = !0;
                    $scope.message = 'Loading Car Models...'
                    $http.get(api_url + "/website/cars").then(function(success) {
                        $scope.showmodel = !0;
                        $scope.custommodel = !1;
                        console.log(success);
                        $scope.carsList = success.data.cars
                    }, function(error) {})['finally'](function() {
                        $scope.test = !1
                    })
                } else {
                    $scope.sendCarData = JSON.stringify({
                        "user_id": $sessionStorage.DHuserSession.id
                    });
                    $http.post(api_url + "/website/user_cars?auth_token=" + $sessionStorage.DHuserSession.authentication_token, $scope.sendCarData).then(function(success) {
                        console.log(success);
                        $scope.showmodel = !1;
                        $scope.custommodel = !0;
                        if (success.data.status == "Failed") {
                            callnotify(success.data.message, 'error')
                        } else {
                            $scope.carsList = success.data.my_cars;
                            if ($scope.carsList == 0) {
                                callnotify("Add your car to make payment", 'success')
                            }
                        }
                    }, function(error) {})['finally'](function() {
                        $scope.test = !1
                    })
                }
            }, function(error) {})['finally'](function() {
                $scope.test = !1;
                $localStorage.DHpaymentdueid = ''
            });
            $('.inputText').prop('disabled', !0);
            $('.inputTextPhone').prop('disabled', !1);
            $('.inputTextReg').prop('disabled', !1)
        }
    }, 500);
    $rootScope.$on('afterLogin', function(event, data) {
        updateTestDriveData()
    });
    var paymentid;
    var secretkey;
    var paymentid1;
    var paymenturl;
    var app_token;
    var transactionid;
    var transtatus;
    var manualpayid;
    var res;
    var data;
    var apptoken;
    $scope.manualpayment = function() {
        $scope.modelval = !1;
        if (!$scope.testDriveFormdata.car_id && $scope.testDriveFormdata.entity_type != "Other") {
            $scope.modelval = !0;
            return !1
        }
        console.log("loc",$scope.testDriveFormdata)
         $scope.dealer_loc = !1;
        if (!$scope.testDriveFormdata.dealer_id) {
            $scope.dealer_loc = !0;
            return !1
        }
 
        if ($scope.userForm.$valid) {



            callnotify("Requesting, please wait...", 'information');
            if (status == 1) {
                $scope.testDriveFormdata.id = $scope.paymentdata.id
            } else {}
            angular.forEach($scope.dealer_data_master, function(value, key) {
                if ($scope.testDriveFormdata.dealer_id == value.id) {
                    $scope.testDriveFormdata.dealer_name = value.dealer_name
                }
            });
            console.log(($scope.testDriveFormdata));
            $http.post(api_url + "/website/generate_payments", $scope.testDriveFormdata).then(function(success) {
                var result = success.data;
                secretkey = result.secret_token;
                paymentid1 = result.payment_id;
                $sessionStorage.DHpaymentid = result.payment_id;
                $sessionStorage.DHpaymentapptoken = result.app_token;
                $localStorage.DHpaymentphone = result.mobile;
                $localStorage.DHpaymentchecksum = result.checksum;
                app_token = result.app_token;
                manualpayid = result.bill_num;
                apptoken = result.app_token;
                data = {
                    "datetime": result.datetime,
                    "partnerid": result.partner_id,
                    "billno": result.bill_num,
                    "phone": result.mobile,
                    "email": result.email,
                    "name": result.name,
                    "amount": result.amount,
                    "paymentmode": "1",
                    "description": "Bill for  Skyye",
                    "checksum": result.checksum
                };
                makepayment()
            }, function(error) {
                callnotify('Transaction failed. Please try again later', 'error')
            })['finally'](function() {
                $scope.test = !1
            })
        } else {
            return !1
        }
    }

    function makepayment() {
        console.log(JSON.stringify(data));
        $http.post("https://partners.vpaynow.com/web_initiate_transaction", data, {
            headers: {
                'Content-Type': 'application/json',
                'X-App-Token': apptoken
            }
        }).then(function(success) {
            var result = success.data;
            paymenturl = result.payment.transaction_url;
            transactionid = result.payment.transaction_id;
            $localStorage.DHpaymenttrandid = result.payment.transaction_id;
            transtatus = result.request.message;
            var senddata = {
                "secret_key": secretkey,
                "payment_id": paymentid1,
                "transaction_id": transactionid,
                "transaction_url": paymenturl,
                "message": transtatus
            };
            $http.post(api_url + "/website/check_transection_status", senddata).then(function(success) {
            var myLink = document.getElementById('manualpayment');
var google_conversion_id = 825753188;
var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
var google_remarketing_only = false;


myLink.onclick = function(){

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "//www.googleadservices.com/pagead/conversion.js"; 
    document.getElementsByTagName("head")[0].appendChild(script);
    return false;

}
                $sessionStorage.DHpaymentdetails = success.data;
                $localStorage.DHpaymenturl = paymenturl;
                window.location.href = paymenturl;
                return !1;
                if (success.data.status == "Failed") {
                    callnotify(success.data.message, 'error')
                } else {}
            }, function(error) {})['finally'](function() {
                $scope.test = !1
            });
            if (success.data.status == "Failed") {
                callnotify(success.data.message, 'error')
            } else {}
        }, function(error) {
            callnotify("Transaction failed. Try again later", 'error')
        })['finally'](function() {
            $scope.test = !1
        });
        $localStorage.DHpaymentdueid = ''
    }
    $scope.changedealer = function() {
        $scope.customfield = !1;
        var type = $scope.testDriveFormdata.entity_type;
        if (type == "Booking Down-payment" || type == "Other") {
            $scope.test = !0;
            $scope.message = 'Loading Car Models...'
            $http.get(api_url + "/website/cars").then(function(success) {
                $scope.showmodel = !0;
                $scope.custommodel = !1;
                $scope.carsList = success.data.cars
            }, function(error) {})['finally'](function() {
                $scope.test = !1
            })
        } else {
            $scope.sendCarData = JSON.stringify({
                "user_id": $sessionStorage.DHuserSession.id
            });
            $http.post(api_url + "/website/user_cars?auth_token=" + $sessionStorage.DHuserSession.authentication_token, $scope.sendCarData).then(function(success) {
                console.log("car datra",success);
                $scope.showmodel = !1;
                $scope.custommodel = !0;
                if (success.data.status == "Failed") {
                    callnotify(success.data.message, 'error')
                } else {
                    $scope.carsList = success.data.my_cars;
                    if ($scope.carsList == 0) {
                        callnotify("Add My cars to make payment", 'success')
                    }
                    if (success.data.user_cars.length == 1) {
                        $scope.testDriveFormdata.car_id = $scope.carsList[0].id
                    }
                }
            }, function(error) {})['finally'](function() {
                $scope.test = !1
            })
        }
        updateDealers()
    }
    $scope.changemodel = function() {
        var mycarid = $scope.testDriveFormdata.car_id;
        if ($scope.testDriveFormdata.entity_type != "Booking Down-payment" || $scope.testDriveFormdata.entity_type != "Other") {
            $http.get(api_url + "/website/my_cars/" + mycarid + ".json").then(function(success) {
                $scope.testDriveFormdata.regno = success.data.registration_number
            }, function(error) {})['finally'](function() {
                $scope.test = !1
            })
        }
    }

    console.log("$rootScope.booking_car_data",$rootScope.booking_car_data);
    if($rootScope.booking_car_data!=null){
        $http.get(api_url + "/website/dealers").then(function(success) {
            var serviceStationData = success.data.dealers;
            $scope.dealer_data_master = success.data.dealers;
            console.log("dealer_data_master",$scope.dealer_data_master);
            for (var i = 0; i < serviceStationData.length; i++) {
                serviceStation.push(serviceStationData[i].dealer.dealer_name)
            }
            $scope.serviceStation = serviceStation
            
        }, function(error) {})['finally'](function() {
            updateDealers();
            $scope.test = !1
            
        });
         $scope.testDriveFormdata.entity_type="Booking Down-payment";
$scope.testDriveFormdata.car_id=$rootScope.booking_car_data.id.toString();
console.log("booking data found");

 
       
      

$scope.directbooking=true;
       


// $scope.testDriveFormdata.entity_type=$rootScope.booking_car_data.car_name;
    } else {
        console.log("no booking data");
        $scope.directbooking=false;
    }
    
})