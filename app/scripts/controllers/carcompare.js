"use strict";
angular.module("dakshinHondaWebsiteApp").controller("CarcompareCtrl", function($scope, $http, $mdDialog, $sessionStorage, $localStorage, $rootScope, $location, anchorSmoothScroll) {
    window.scrollTo(0, 0);
    $scope.$on("$routeChangeStart", function(scope, next, current) {
        if (next.$$route.controller != "CarcompareCtrl") {
            $mdDialog.cancel({})
        }
    });
    $scope.goToOverview = function(eID) {
        anchorSmoothScroll.scrollTo(eID);
        expandSpecific(eID)
    };
    $scope.overViewArray = [{
        name: "fuel type"
    }, {
        name: "fuel tank capacity"
    }, {
        name: "seating capacity"
    }, {
        name: "transmission type"
    }, {
        name: "engine displacement"
    }, {
        name: "fuel efficiency"
    }, {
        name: "available colors"
    }], $scope.interiorArray = [{
        name: "Seat Upholstery"
    }, {
        name: "Multi Information Display"
    }, {
        name: "Fuel Consumption Display"
    }, {
        name: "Cruising Range Display"
    }, {
        name: "Trip Meter"
    }, {
        name: "Outside Temp Display"
    }, {
        name: "2nd Seat Back Fold Down 60:40 Split"
    }, {
        name: "Rear AC Vents"
    }, {
        name: "Eco Lamp"
    }], $scope.exteriorArray = [{
        name: "Alloy Wheels"
    }, {
        name: "Front Chrome Grill"
    }, {
        name: "Turn Indictor on ORVM"
    }, {
        name: "Antenna"
    }, {
        name: "Body Color Front & Back"
    }, {
        name: "Outdoor Handles"
    }, {
        name: "ORVM Color"
    }, {
        name: "Front & Rear Mudguards"
    }, {
        name: "Head Lamp"
    }, {
        name: "Fog Lamps"
    }, {
        name: "Rear Wiper & Washer"
    }], $scope.dimensionArray = [{
        name: "Overall Length (mm)"
    }, {
        name: "Overall Width (mm)"
    }, {
        name: "Overall Height (mm)"
    }, {
        name: "Wheelbase (mm)"
    }, {
        name: "Kerb Weight MT (kg)"
    }, {
        name: "Tyre Size"
    }], $scope.comfortArray = [{
        name: "Ac with Heater"
    }, {
        name: "Power Steering"
    }, {
        name: "Steering Type"
    }, {
        name: "Electrical Folding ORVM"
    }, {
        name: "Central locking & keyless entry"
    }, {
        name: "Power Window"
    }, {
        name: "Push Start"
    }, {
        name: "Auto Door lock by speed"
    }, {
        name: "Day Night IRVM"
    }, {
        name: "Ignition Key Reminder"
    }, {
        name: "Driver Seat Height Adjustment"
    }, {
        name: "Horn Type"
    }], $scope.safetyArray = [{
        name: "Airbags"
    }, {
        name: "ABS with EBD"
    }, {
        name: "Immobilizer"
    }, {
        name: "Vehicle Stability Assist"
    }, {
        name: "Security Alarm"
    }, {
        name: "Driver Seat Belt Reminder"
    }], $scope.specificationArray = [{
        name: "Displacement (cc)"
    }, {
        name: "Power (ps @ rpm)"
    }, {
        name: "Torque (Nm @ rpm)"
    }, {
        name: "Transmission (Automatic)"
    }, {
        name: "Transmission (Manual)"
    }, {
        name: "Turning Radius"
    }, {
        name: "Suspension (Front)"
    }, {
        name: "Suspension (Rear)"
    }, {
        name: "Brakes (Front)"
    }, {
        name: "Brakes (Rear)"
    }], $scope.entertainmentArray = [{
        name: "Music System"
    }, {
        name: "Steering mounted audio control"
    }, {
        name: "Touch screen"
    }, {
        name: "Aux In"
    }];
    $scope.has3Cars = 0;
    $scope.allCars = {};
    $scope.car3Variant = "";
    $scope.car2Variant = "";
    $scope.car1Variant = "";
    $scope.common = !1;
    $scope.differences = !1;
    var highLightCommonisChecked = !1;
    var highLightNonCommonisChecked = !1;
    $scope.highLightCommon = function() {
        $scope.differences = !1;
        if (highLightCommonisChecked == !1) {
            try {
                if ($scope.has3Cars == 1) {
                    removeHighlight();
                    highLightCommonisChecked = !0;
                    if ($scope.car1.variant_data.variant.fuel_type == $scope.car2.variant_data.variant.fuel_type && $scope.car1.variant_data.variant.fuel_type == $scope.car3.variant_data.variant.fuel_type) {
                        $scope.hasSamefueltype = 1
                    } else {
                        $scope.hasSamefueltype = 0
                    }
                    if ($scope.car1.fuelCapacity.value == $scope.car2.fuelCapacity.value && $scope.car1.fuelCapacity.value == $scope.car3.fuelCapacity.value) {
                        $scope.hasSamefuelCapacity = 1
                    } else {
                        $scope.hasSamefuelCapacity = 0
                    }
                    if ($scope.car1.variant_data.variant.seating == $scope.car2.variant_data.variant.seating && $scope.car1.variant_data.variant.seating == $scope.car3.variant_data.variant.seating) {
                        $scope.hasSameseating = 1
                    } else {
                        $scope.hasSameseating = 0
                    }
                    if ($scope.car1.displacement.value == $scope.car2.displacement.value && $scope.car1.displacement.value == $scope.car3.displacement.value) {
                        $scope.hasSamedisplacement = 1
                    } else {
                        $scope.hasSamedisplacement = 0
                    }
                    if ($scope.car1.fuelEfficiency.value == $scope.car2.fuelEfficiency.value && $scope.car1.fuelEfficiency.value == $scope.car3.fuelEfficiency.value) {
                        $scope.hasSamefuelEfficiency = 1
                    } else {
                        $scope.hasSamefuelEfficiency = 0
                    }
                    if ($scope.car1.variant_data.variant.transmission_type == $scope.car2.variant_data.variant.transmission_type && $scope.car1.variant_data.variant.transmission_type == $scope.car3.variant_data.variant.transmission_type) {
                        $scope.hasSameTransmissiontype = 1
                    } else {
                        $scope.hasSameTransmissiontype = 0
                    }
                    if ($scope.car1.upholstery.description == $scope.car2.upholstery.description && $scope.car1.upholstery.description == $scope.car3.upholstery.description) {
                        $scope.hasSameupholstery = 1
                    } else {
                        $scope.hasSameupholstery = 0
                    }
                    if ($scope.car1.leatherWrappedSteeringWheel.description == $scope.car2.leatherWrappedSteeringWheel.description && $scope.car1.leatherWrappedSteeringWheel.description == $scope.car3.leatherWrappedSteeringWheel.description) {
                        $scope.hasSameleatherWrappedSteeringWheel = 1
                    } else {
                        $scope.hasSameleatherWrappedSteeringWheel = 0
                    }
                    if ($scope.car1.leatherGearShiftKnob.description == $scope.car2.leatherGearShiftKnob.description && $scope.car1.leatherGearShiftKnob.description == $scope.car3.leatherGearShiftKnob.description) {
                        $scope.hasSameleatherGearShiftKnob = 1
                    } else {
                        $scope.hasSameleatherGearShiftKnob = 0
                    }
                    if ($scope.car1.multiInformationDisplay == $scope.car2.multiInformationDisplay && $scope.car1.multiInformationDisplay == $scope.car3.multiInformationDisplay) {
                        $scope.hasSamemultiInformationDisplay = 1
                    } else {
                        $scope.hasSamemultiInformationDisplay = 0
                    }
                    if ($scope.car1.fuelConsumptionDisplay == $scope.car2.fuelConsumptionDisplay && $scope.car1.fuelConsumptionDisplay == $scope.car3.fuelConsumptionDisplay) {
                        $scope.hasSamefuelConsumptionDisplay = 1
                    } else {
                        $scope.hasSamefuelConsumptionDisplay = 0
                    }
                    if ($scope.car1.cruisingRangeDisplay == $scope.car2.cruisingRangeDisplay && $scope.car1.cruisingRangeDisplay == $scope.car3.cruisingRangeDisplay) {
                        $scope.hasSamecruisingRangeDisplay = 1
                    } else {
                        $scope.hasSamecruisingRangeDisplay = 0
                    }
                    if ($scope.car1.tripMeter.description == $scope.car2.tripMeter.description && $scope.car1.tripMeter.description == $scope.car3.tripMeter.description) {
                        $scope.hasSametripMeter = 1
                    } else {
                        $scope.hasSametripMeter = 0
                    }
                    if ($scope.car1.outsideTempDisplay == $scope.car2.outsideTempDisplay && $scope.car1.outsideTempDisplay == $scope.car3.outsideTempDisplay) {
                        $scope.hasSameoutsideTempDisplay = 1
                    } else {
                        $scope.hasSameoutsideTempDisplay = 0
                    }
                    if ($scope.car1.instrumentPanelGarnish == $scope.car2.instrumentPanelGarnish && $scope.car1.instrumentPanelGarnish == $scope.car3.instrumentPanelGarnish) {
                        $scope.hasSameinstrumentPanelGarnish = 1
                    } else {
                        $scope.hasSameinstrumentPanelGarnish = 0
                    }
                    if ($scope.car1.silverInsideDoorHandle.description == $scope.car2.silverInsideDoorHandle.description && $scope.car1.silverInsideDoorHandle.description == $scope.car3.silverInsideDoorHandle.description) {
                        $scope.hasSamesilverInsideDoorHandle = 1
                    } else {
                        $scope.hasSamesilverInsideDoorHandle = 0
                    }
                    if ($scope.car1.seatBackFoldDown.description == $scope.car2.seatBackFoldDown.description && $scope.car1.seatBackFoldDown.description == $scope.car3.seatBackFoldDown.description) {
                        $scope.hasSameseatBackFoldDown = 1
                    } else {
                        $scope.hasSameseatBackFoldDown = 0
                    }
                    if ($scope.car1.rearACVents == $scope.car2.rearACVents && $scope.car1.rearACVents == $scope.car3.rearACVents) {
                        $scope.hasSamerearACVents = 1
                    } else {
                        $scope.hasSamerearACVents = 0
                    }
                    if ($scope.car1.ecoLamps == $scope.car2.ecoLamps && $scope.car1.ecoLamps == $scope.car3.ecoLamps) {
                        $scope.hasSameecoLamps = 1
                    } else {
                        $scope.hasSameecoLamps = 0
                    }
                    if ($scope.car1.alloyWheels == $scope.car2.alloyWheels && $scope.car1.alloyWheels == $scope.car3.alloyWheels) {
                        $scope.hasSamealloyWheels = 1
                    } else {
                        $scope.hasSamealloyWheels = 0
                    }
                    if ($scope.car1.fullTrimWheel.description == $scope.car2.fullTrimWheel.description && $scope.car1.fullTrimWheel.description == $scope.car3.fullTrimWheel.description) {
                        $scope.hasSamefullTrimWheel = 1
                    } else {
                        $scope.hasSamefullTrimWheel = 0
                    }
                    if ($scope.car1.frontChromeGrill.description == $scope.car2.frontChromeGrill.description && $scope.car1.frontChromeGrill.description == $scope.car3.frontChromeGrill.description) {
                        $scope.hasSamefrontChromeGrill = 1
                    } else {
                        $scope.hasSamefrontChromeGrill = 0
                    }
                    if ($scope.car1.turnIndictorOnORVM == $scope.car2.turnIndictorOnORVM && $scope.car1.turnIndictorOnORVM == $scope.car3.turnIndictorOnORVM) {
                        $scope.hasSameturnIndictorOnORVM = 1
                    } else {
                        $scope.hasSameturnIndictorOnORVM = 0
                    }
                    if ($scope.car1.antenna.description == $scope.car2.antenna.description && $scope.car1.antenna.description == $scope.car3.antenna.description) {
                        $scope.hasSameantenna = 1
                    } else {
                        $scope.hasSameantenna = 0
                    }
                    if ($scope.car1.bodyColorFrontBack.description == $scope.car2.bodyColorFrontBack.description && $scope.car1.bodyColorFrontBack.description == $scope.car3.bodyColorFrontBack.description) {
                        $scope.hasSamebodyColorFrontBack = 1
                    } else {
                        $scope.hasSamebodyColorFrontBack = 0
                    }
                    if ($scope.car1.outdoorHandles.description == $scope.car2.outdoorHandles.description && $scope.car1.outdoorHandles.description == $scope.car3.outdoorHandles.description) {
                        $scope.hasSameoutdoorHandles = 1
                    } else {
                        $scope.hasSameoutdoorHandles = 0
                    }
                    if ($scope.car1.oRVMColor.description == $scope.car2.oRVMColor.description && $scope.car1.oRVMColor.description == $scope.car3.oRVMColor.description) {
                        $scope.hasSameoRVMColor = 1
                    } else {
                        $scope.hasSameoRVMColor = 0
                    }
                    if ($scope.car1.frontRearMudguards.description == $scope.car2.frontRearMudguards.description && $scope.car1.frontRearMudguards.description == $scope.car3.frontRearMudguards.description) {
                        $scope.hasSamefrontRearMudguards = 1
                    } else {
                        $scope.hasSamefrontRearMudguards = 0
                    }
                    if ($scope.car1.headLamp.description == $scope.car2.headLamp.description && $scope.car1.headLamp.description == $scope.car3.headLamp.description) {
                        $scope.hasSameheadLamp = 1
                    } else {
                        $scope.hasSameheadLamp = 0
                    }
                    if ($scope.car1.fogLamps == $scope.car2.fogLamps && $scope.car1.fogLamps == $scope.car3.fogLamps) {
                        $scope.hasSamefogLamps = 1
                    } else {
                        $scope.hasSamefogLamps = 0
                    }
                    if ($scope.car1.rearWiperWasher.description == $scope.car2.rearWiperWasher.description && $scope.car1.rearWiperWasher.description == $scope.car3.rearWiperWasher.description) {
                        $scope.hasSamerearWiperWasher = 1
                    } else {
                        $scope.hasSamerearWiperWasher = 0
                    }
                    if ($scope.car1.overallLength.value == $scope.car2.overallLength.value && $scope.car1.overallLength.value == $scope.car3.overallLength.value) {
                        $scope.hasSameoverallLength = 1
                    } else {
                        $scope.hasSameoverallLength = 0
                    }
                    if ($scope.car1.overallWidth.value == $scope.car2.overallWidth.value && $scope.car1.overallWidth.value == $scope.car3.overallWidth.value) {
                        $scope.hasSameoverallWidth = 1
                    } else {
                        $scope.hasSameoverallWidth = 0
                    }
                    if ($scope.car1.overallHeight.value == $scope.car2.overallHeight.value && $scope.car1.overallHeight.value == $scope.car3.overallHeight.value) {
                        $scope.hasSameoverallHeight = 1
                    } else {
                        $scope.hasSameoverallHeight = 0
                    }
                    if ($scope.car1.wheelbase.value == $scope.car2.wheelbase.value && $scope.car1.wheelbase.value == $scope.car3.wheelbase.value) {
                        $scope.hasSamewheelbase = 1
                    } else {
                        $scope.hasSamewheelbase = 0
                    }
                    if ($scope.car1.kerbWeight.value == $scope.car2.kerbWeight.value && $scope.car1.kerbWeight.value == $scope.car3.kerbWeight.value) {
                        $scope.hasSamekerbWeight = 1
                    } else {
                        $scope.hasSamekerbWeight = 0
                    }
                    if ($scope.car1.tyreSize.value == $scope.car2.tyreSize.value && $scope.car1.tyreSize.value == $scope.car3.tyreSize.value) {
                        $scope.hasSametyreSize = 1
                    } else {
                        $scope.hasSametyreSize = 0
                    }
                    if ($scope.car1.steeringSystem.value == $scope.car2.steeringSystem.value && $scope.car1.steeringSystem.value == $scope.car3.steeringSystem.value) {
                        $scope.hasSamesteeringSystem = 1
                    } else {
                        $scope.hasSamesteeringSystem = 0
                    }
                    if ($scope.car1.musicSystem.description == $scope.car2.musicSystem.description && $scope.car1.musicSystem.description == $scope.car3.musicSystem.description) {
                        $scope.hasSamemusicSystem = 1
                    } else {
                        $scope.hasSamemusicSystem = 0
                    }
                    if ($scope.car1.steeringMounted == $scope.car2.steeringMounted && $scope.car1.steeringMounted == $scope.car3.steeringMounted) {
                        $scope.hasSamesteeringMounted = 1
                    } else {
                        $scope.hasSamesteeringMounted = 0
                    }
                    if ($scope.car1.touchScreen.description == $scope.car2.touchScreen.description && $scope.car1.touchScreen.description == $scope.car3.touchScreen.description) {
                        $scope.hasSametouchScreen = 1
                    } else {
                        $scope.hasSametouchScreen = 0
                    }
                    if ($scope.car1.auxIn == $scope.car2.auxIn && $scope.car1.auxIn == $scope.car3.auxIn) {
                        $scope.hasSameauxIn = 1
                    } else {
                        $scope.hasSameauxIn = 0
                    }
                    if ($scope.car1.displacement.value == $scope.car2.displacement.value && $scope.car1.displacement.value == $scope.car3.displacement.value) {
                        $scope.hasSamedisplacement = 1
                    } else {
                        $scope.hasSamedisplacement = 0
                    }
                    if ($scope.car1.power.value == $scope.car2.power.value && $scope.car1.power.value == $scope.car3.power.value) {
                        $scope.hasSamepower = 1
                    } else {
                        $scope.hasSamepower = 0
                    }
                    if ($scope.car1.torque.value == $scope.car2.torque.value && $scope.car1.torque.value == $scope.car3.torque.value) {
                        $scope.hasSametorque = 1
                    } else {
                        $scope.hasSametorque = 0
                    }
                    if ($scope.car1.transAutomatic.value == $scope.car2.transAutomatic.value && $scope.car1.transAutomatic.value == $scope.car3.transAutomatic.value) {
                        $scope.hasSametransAutomatic = 1
                    } else {
                        $scope.hasSametransAutomatic = 0
                    }
                    if ($scope.car1.transManual.value == $scope.car2.transManual.value && $scope.car1.transManual.value == $scope.car3.transManual.value) {
                        $scope.hasSametransManual = 1
                    } else {
                        $scope.hasSametransManual = 0
                    }
                    if ($scope.car1.turningRadius.value == $scope.car2.turningRadius.value) {
                        $scope.hasSameturningRadius = 1
                    } else {
                        $scope.hasSameturningRadius = 0
                    }
                    if ($scope.car1.suspFront.value == $scope.car2.suspFront.value && $scope.car1.suspFront.value == $scope.car3.suspFront.value) {
                        $scope.hasSamesuspFront = 1
                    } else {
                        $scope.hasSamesuspFront = 0
                    }
                    if ($scope.car1.suspRear.value == $scope.car2.suspRear.value && $scope.car1.suspRear.value == $scope.car3.suspRear.value) {
                        $scope.hasSamesuspRear = 1
                    } else {
                        $scope.hasSamesuspRear = 0
                    }
                    if ($scope.car1.brakesFront.value == $scope.car2.brakesFront.value && $scope.car1.brakesFront.value == $scope.car3.brakesFront.value) {
                        $scope.hasSamebrakesFront = 1
                    } else {
                        $scope.hasSamebrakesFront = 0
                    }
                    if ($scope.car1.brakesRear.value == $scope.car2.brakesRear.value && $scope.car1.brakesRear.value == $scope.car3.brakesRear.value) {
                        $scope.hasSamebrakesRear = 1
                    } else {
                        $scope.hasSamebrakesRear = 0
                    }
                    if ($scope.car1.airBags == $scope.car2.airBags && $scope.car1.airBags == $scope.car3.airBags) {
                        $scope.hasSameairBags = 1
                    } else {
                        $scope.hasSameairBags = 0
                    }
                    if ($scope.car1.aBSwithEBD == $scope.car2.aBSwithEBD && $scope.car1.aBSwithEBD == $scope.car3.aBSwithEBD) {
                        $scope.hasSameaBSwithEBD = 1
                    } else {
                        $scope.hasSameaBSwithEBD = 0
                    }
                    if ($scope.car1.immobilizer == $scope.car2.immobilizer && $scope.car1.immobilizer == $scope.car3.immobilizer) {
                        $scope.hasSameimmobilizer = 1
                    } else {
                        $scope.hasSameimmobilizer = 0
                    }
                    if ($scope.car1.vehStabilityAssist.description == $scope.car2.vehStabilityAssist.description && $scope.car1.vehStabilityAssist.description == $scope.car3.vehStabilityAssist.description) {
                        $scope.hasSamevehStabilityAssist = 1
                    } else {
                        $scope.hasSamevehStabilityAssist = 0
                    }
                    if ($scope.car1.securityAlarm == $scope.car2.securityAlarm && $scope.car1.securityAlarm == $scope.car3.securityAlarm) {
                        $scope.hasSamesecurityAlarm = 1
                    } else {
                        $scope.hasSamesecurityAlarm = 0
                    }
                    if ($scope.car1.driverSeatBeltReminder == $scope.car2.driverSeatBeltReminder && $scope.car1.driverSeatBeltReminder == $scope.car3.driverSeatBeltReminder) {
                        $scope.hasSamedriverSeatBeltReminder = 1
                    } else {
                        $scope.hasSamedriverSeatBeltReminder = 0
                    }
                } else {
                    removeHighlight();
                    highLightCommonisChecked = !0;
                    if ($scope.car1.variant_data.variant.fuel_type == $scope.car2.variant_data.variant.fuel_type) {
                        $scope.hasSamefueltype = 1
                    } else {
                        $scope.hasSamefueltype = 0
                    }
                    if ($scope.car1.fuelCapacity.value == $scope.car2.fuelCapacity.value) {
                        $scope.hasSamefuelCapacity = 1
                    } else {
                        $scope.hasSamefuelCapacity = 0
                    }
                    if ($scope.car1.variant_data.variant.seating == $scope.car2.variant_data.variant.seating) {
                        $scope.hasSameseating = 1
                    } else {
                        $scope.hasSameseating = 0
                    }
                    if ($scope.car1.displacement.value == $scope.car2.displacement.value) {
                        $scope.hasSamedisplacement = 1
                    } else {
                        $scope.hasSamedisplacement = 0
                    }
                    if ($scope.car1.fuelEfficiency.value == $scope.car2.fuelEfficiency.value) {
                        $scope.hasSamefuelEfficiency = 1
                    } else {
                        $scope.hasSamefuelEfficiency = 0
                    }
                    if ($scope.car1.variant_data.variant.transmission_type == $scope.car2.variant_data.variant.transmission_type) {
                        $scope.hasSameTransmissiontype = 1
                    } else {
                        $scope.hasSameTransmissiontype = 0
                    }
                    if ($scope.car1.upholstery.description == $scope.car2.upholstery.description) {
                        $scope.hasSameupholstery = 1
                    } else {
                        $scope.hasSameupholstery = 0
                    }
                    if ($scope.car1.leatherWrappedSteeringWheel.description == $scope.car2.leatherWrappedSteeringWheel.description) {
                        $scope.hasSameleatherWrappedSteeringWheel = 1
                    } else {
                        $scope.hasSameleatherWrappedSteeringWheel = 0
                    }
                    if ($scope.car1.leatherGearShiftKnob.description == $scope.car2.leatherGearShiftKnob.description) {
                        $scope.hasSameleatherGearShiftKnob = 1
                    } else {
                        $scope.hasSameleatherGearShiftKnob = 0
                    }
                    if ($scope.car1.multiInformationDisplay == $scope.car2.multiInformationDisplay) {
                        $scope.hasSamemultiInformationDisplay = 1
                    } else {
                        $scope.hasSamemultiInformationDisplay = 0
                    }
                    if ($scope.car1.fuelConsumptionDisplay == $scope.car2.fuelConsumptionDisplay) {
                        $scope.hasSamefuelConsumptionDisplay = 1
                    } else {
                        $scope.hasSamefuelConsumptionDisplay = 0
                    }
                    if ($scope.car1.cruisingRangeDisplay == $scope.car2.cruisingRangeDisplay) {
                        $scope.hasSamecruisingRangeDisplay = 1
                    } else {
                        $scope.hasSamecruisingRangeDisplay = 0
                    }
                    if ($scope.car1.tripMeter.description == $scope.car2.tripMeter.description) {
                        $scope.hasSametripMeter = 1
                    } else {
                        $scope.hasSametripMeter = 0
                    }
                    if ($scope.car1.outsideTempDisplay == $scope.car2.outsideTempDisplay) {
                        $scope.hasSameoutsideTempDisplay = 1
                    } else {
                        $scope.hasSameoutsideTempDisplay = 0
                    }
                    if ($scope.car1.instrumentPanelGarnish == $scope.car2.instrumentPanelGarnish) {
                        $scope.hasSameinstrumentPanelGarnish = 1
                    } else {
                        $scope.hasSameinstrumentPanelGarnish = 0
                    }
                    if ($scope.car1.silverInsideDoorHandle.description == $scope.car2.silverInsideDoorHandle.description) {
                        $scope.hasSamesilverInsideDoorHandle = 1
                    } else {
                        $scope.hasSamesilverInsideDoorHandle = 0
                    }
                    if ($scope.car1.seatBackFoldDown.description == $scope.car2.seatBackFoldDown.description) {
                        $scope.hasSameseatBackFoldDown = 1
                    } else {
                        $scope.hasSameseatBackFoldDown = 0
                    }
                    if ($scope.car1.rearACVents == $scope.car2.rearACVents) {
                        $scope.hasSamerearACVents = 1
                    } else {
                        $scope.hasSamerearACVents = 0
                    }
                    if ($scope.car1.ecoLamps == $scope.car2.ecoLamps) {
                        $scope.hasSameecoLamps = 1
                    } else {
                        $scope.hasSameecoLamps = 0
                    }
                    if ($scope.car1.alloyWheels == $scope.car2.alloyWheels) {
                        $scope.hasSamealloyWheels = 1
                    } else {
                        $scope.hasSamealloyWheels = 0
                    }
                    if ($scope.car1.fullTrimWheel.description == $scope.car2.fullTrimWheel.description) {
                        $scope.hasSamefullTrimWheel = 1
                    } else {
                        $scope.hasSamefullTrimWheel = 0
                    }
                    if ($scope.car1.frontChromeGrill.description == $scope.car2.frontChromeGrill.description) {
                        $scope.hasSamefrontChromeGrill = 1
                    } else {
                        $scope.hasSamefrontChromeGrill = 0
                    }
                    if ($scope.car1.turnIndictorOnORVM == $scope.car2.turnIndictorOnORVM) {
                        $scope.hasSameturnIndictorOnORVM = 1
                    } else {
                        $scope.hasSameturnIndictorOnORVM = 0
                    }
                    if ($scope.car1.antenna.description == $scope.car2.antenna.description) {
                        $scope.hasSameantenna = 1
                    } else {
                        $scope.hasSameantenna = 0
                    }
                    if ($scope.car1.bodyColorFrontBack.description == $scope.car2.bodyColorFrontBack.description) {
                        $scope.hasSamebodyColorFrontBack = 1
                    } else {
                        $scope.hasSamebodyColorFrontBack = 0
                    }
                    if ($scope.car1.outdoorHandles.description == $scope.car2.outdoorHandles.description) {
                        $scope.hasSameoutdoorHandles = 1
                    } else {
                        $scope.hasSameoutdoorHandles = 0
                    }
                    if ($scope.car1.oRVMColor.description == $scope.car2.oRVMColor.description) {
                        $scope.hasSameoRVMColor = 1
                    } else {
                        $scope.hasSameoRVMColor = 0
                    }
                    if ($scope.car1.frontRearMudguards.description == $scope.car2.frontRearMudguards.description) {
                        $scope.hasSamefrontRearMudguards = 1
                    } else {
                        $scope.hasSamefrontRearMudguards = 0
                    }
                    if ($scope.car1.headLamp.description == $scope.car2.headLamp.description) {
                        $scope.hasSameheadLamp = 1
                    } else {
                        $scope.hasSameheadLamp = 0
                    }
                    if ($scope.car1.fogLamps == $scope.car2.fogLamps) {
                        $scope.hasSamefogLamps = 1
                    } else {
                        $scope.hasSamefogLamps = 0
                    }
                    if ($scope.car1.rearWiperWasher.description == $scope.car2.rearWiperWasher.description) {
                        $scope.hasSamerearWiperWasher = 1
                    } else {
                        $scope.hasSamerearWiperWasher = 0
                    }
                    if ($scope.car1.overallLength.value == $scope.car2.overallLength.value) {
                        $scope.hasSameoverallLength = 1
                    } else {
                        $scope.hasSameoverallLength = 0
                    }
                    if ($scope.car1.overallWidth.value == $scope.car2.overallWidth.value) {
                        $scope.hasSameoverallWidth = 1
                    } else {
                        $scope.hasSameoverallWidth = 0
                    }
                    if ($scope.car1.overallHeight.value == $scope.car2.overallHeight.value) {
                        $scope.hasSameoverallHeight = 1
                    } else {
                        $scope.hasSameoverallHeight = 0
                    }
                    if ($scope.car1.wheelbase.value == $scope.car2.wheelbase.value) {
                        $scope.hasSamewheelbase = 1
                    } else {
                        $scope.hasSamewheelbase = 0
                    }
                    if ($scope.car1.kerbWeight.value == $scope.car2.kerbWeight.value) {
                        $scope.hasSamekerbWeight = 1
                    } else {
                        $scope.hasSamekerbWeight = 0
                    }
                    if ($scope.car1.tyreSize.value == $scope.car2.tyreSize.value) {
                        $scope.hasSametyreSize = 1
                    } else {
                        $scope.hasSametyreSize = 0
                    }
                    if ($scope.car1.steeringSystem.value == $scope.car2.steeringSystem.value) {
                        $scope.hasSamesteeringSystem = 1
                    } else {
                        $scope.hasSamesteeringSystem = 0
                    }
                    if ($scope.car1.musicSystem.description == $scope.car2.musicSystem.description) {
                        $scope.hasSamemusicSystem = 1
                    } else {
                        $scope.hasSamemusicSystem = 0
                    }
                    if ($scope.car1.steeringMounted == $scope.car2.steeringMounted) {
                        $scope.hasSamesteeringMounted = 1
                    } else {
                        $scope.hasSamesteeringMounted = 0
                    }
                    if ($scope.car1.touchScreen.description == $scope.car2.touchScreen.description) {
                        $scope.hasSametouchScreen = 1
                    } else {
                        $scope.hasSametouchScreen = 0
                    }
                    if ($scope.car1.auxIn == $scope.car2.auxIn) {
                        $scope.hasSameauxIn = 1
                    } else {
                        $scope.hasSameauxIn = 0
                    }
                    if ($scope.car1.displacement.value == $scope.car2.displacement.value) {
                        $scope.hasSamedisplacement = 1
                    } else {
                        $scope.hasSamedisplacement = 0
                    }
                    if ($scope.car1.power.value == $scope.car2.power.value) {
                        $scope.hasSamepower = 1
                    } else {
                        $scope.hasSamepower = 0
                    }
                    if ($scope.car1.torque.value == $scope.car2.torque.value) {
                        $scope.hasSametorque = 1
                    } else {
                        $scope.hasSametorque = 0
                    }
                    if ($scope.car1.transAutomatic.value == $scope.car2.transAutomatic.value) {
                        $scope.hasSametransAutomatic = 1
                    } else {
                        $scope.hasSametransAutomatic = 0
                    }
                    if ($scope.car1.transManual.value == $scope.car2.transManual.value) {
                        $scope.hasSametransManual = 1
                    } else {
                        $scope.hasSametransManual = 0
                    }
                    if ($scope.car1.turningRadius.value == $scope.car2.turningRadius.value) {
                        $scope.hasSameturningRadius = 1
                    } else {
                        $scope.hasSameturningRadius = 0
                    }
                    if ($scope.car1.suspFront.value == $scope.car2.suspFront.value) {
                        $scope.hasSamesuspFront = 1
                    } else {
                        $scope.hasSamesuspFront = 0
                    }
                    if ($scope.car1.suspRear.value == $scope.car2.suspRear.value) {
                        $scope.hasSamesuspRear = 1
                    } else {
                        $scope.hasSamesuspRear = 0
                    }
                    if ($scope.car1.brakesFront.value == $scope.car2.brakesFront.value) {
                        $scope.hasSamebrakesFront = 1
                    } else {
                        $scope.hasSamebrakesFront = 0
                    }
                    if ($scope.car1.brakesRear.value == $scope.car2.brakesRear.value) {
                        $scope.hasSamebrakesRear = 1
                    } else {
                        $scope.hasSamebrakesRear = 0
                    }
                    if ($scope.car1.airBags == $scope.car2.airBags) {
                        $scope.hasSameairBags = 1
                    } else {
                        $scope.hasSameairBags = 0
                    }
                    if ($scope.car1.aBSwithEBD == $scope.car2.aBSwithEBD) {
                        $scope.hasSameaBSwithEBD = 1
                    } else {
                        $scope.hasSameaBSwithEBD = 0
                    }
                    if ($scope.car1.immobilizer == $scope.car2.immobilizer) {
                        $scope.hasSameimmobilizer = 1
                    } else {
                        $scope.hasSameimmobilizer = 0
                    }
                    if ($scope.car1.vehStabilityAssist.description == $scope.car2.vehStabilityAssist.description) {
                        $scope.hasSamevehStabilityAssist = 1
                    } else {
                        $scope.hasSamevehStabilityAssist = 0
                    }
                    if ($scope.car1.securityAlarm == $scope.car2.securityAlarm) {
                        $scope.hasSamesecurityAlarm = 1
                    } else {
                        $scope.hasSamesecurityAlarm = 0
                    }
                    if ($scope.car1.driverSeatBeltReminder == $scope.car2.driverSeatBeltReminder) {
                        $scope.hasSamedriverSeatBeltReminder = 1
                    } else {
                        $scope.hasSamedriverSeatBeltReminder = 0
                    }
                }
            } catch (e) {}
        } else {
            removeHighlight()
        }
    };
    $scope.highLightUnCommon = function() {
        $scope.common = !1;
        if (highLightNonCommonisChecked == !1) {
            if ($scope.has3Cars == 1) {
                removeHighlight();
                highLightNonCommonisChecked = !0;
                if ($scope.car1.variant_data.variant.fuel_type != $scope.car2.variant_data.variant.fuel_type && $scope.car1.variant_data.variant.fuel_type != $scope.car3.variant_data.variant.fuel_type || $scope.car2.variant_data.variant.fuel_type != $scope.car3.variant_data.variant.fuel_type) {
                    $scope.hasSamefueltype = 1
                } else {
                    $scope.hasSamefueltype = 0
                }
                if ($scope.car1.fuelCapacity.value != $scope.car2.fuelCapacity.value && $scope.car1.fuelCapacity.value != $scope.car3.fuelCapacity.value || $scope.car2.fuelCapacity.value != $scope.car3.fuelCapacity.value) {
                    $scope.hasSamefuelCapacity = 1
                } else {
                    $scope.hasSamefuelCapacity = 0
                }
                if ($scope.car1.variant_data.variant.seating != $scope.car2.variant_data.variant.seating && $scope.car1.variant_data.variant.seating != $scope.car3.variant_data.variant.seating || $scope.car2.variant_data.variant.seating != $scope.car3.variant_data.variant.seating) {
                    $scope.hasSameseating = 1
                } else {
                    $scope.hasSameseating = 0
                }
                if ($scope.car1.displacement.value != $scope.car2.displacement.value && $scope.car1.displacement.value != $scope.car3.displacement.value || $scope.car2.displacement.value != $scope.car3.displacement.value) {
                    $scope.hasSamedisplacement = 1
                } else {
                    $scope.hasSamedisplacement = 0
                }
                if ($scope.car1.fuelEfficiency.value != $scope.car2.fuelEfficiency.value && $scope.car1.fuelEfficiency.value != $scope.car3.fuelEfficiency.value || $scope.car2.fuelEfficiency.value != $scope.car3.fuelEfficiency.value) {
                    $scope.hasSamefuelEfficiency = 1
                } else {
                    $scope.hasSamefuelEfficiency = 0
                }
                if ($scope.car1.variant_data.variant.transmission_type != $scope.car2.variant_data.variant.transmission_type && $scope.car1.variant_data.variant.transmission_type != $scope.car3.variant_data.variant.transmission_type || $scope.car2.variant_data.variant.transmission_type != $scope.car3.variant_data.variant.transmission_type) {
                    $scope.hasSameTransmissiontype = 1
                } else {
                    $scope.hasSameTransmissiontype = 0
                }
                if ($scope.car1.upholstery.description != $scope.car2.upholstery.description && $scope.car1.upholstery.description != $scope.car3.upholstery.description || $scope.car2.upholstery.description != $scope.car3.upholstery.description) {
                    $scope.hasSameupholstery = 1
                } else {
                    $scope.hasSameupholstery = 0
                }
                if ($scope.car1.leatherWrappedSteeringWheel.description != $scope.car2.leatherWrappedSteeringWheel.description && $scope.car1.leatherWrappedSteeringWheel.description != $scope.car3.leatherWrappedSteeringWheel.description || $scope.car2.leatherWrappedSteeringWheel.description != $scope.car3.leatherWrappedSteeringWheel.description) {
                    $scope.hasSameleatherWrappedSteeringWheel = 1
                } else {
                    $scope.hasSameleatherWrappedSteeringWheel = 0
                }
                if ($scope.car1.leatherGearShiftKnob.description != $scope.car2.leatherGearShiftKnob.description && $scope.car1.leatherGearShiftKnob.description != $scope.car3.leatherGearShiftKnob.description || $scope.car2.leatherGearShiftKnob.description != $scope.car3.leatherGearShiftKnob.description) {
                    $scope.hasSameleatherGearShiftKnob = 1
                } else {
                    $scope.hasSameleatherGearShiftKnob = 0
                }
                if ($scope.car1.multiInformationDisplay != $scope.car2.multiInformationDisplay && $scope.car1.multiInformationDisplay != $scope.car3.multiInformationDisplay || $scope.car2.multiInformationDisplay != $scope.car3.multiInformationDisplay) {
                    $scope.hasSamemultiInformationDisplay = 1
                } else {
                    $scope.hasSamemultiInformationDisplay = 0
                }
                if ($scope.car1.fuelConsumptionDisplay != $scope.car2.fuelConsumptionDisplay && $scope.car1.fuelConsumptionDisplay != $scope.car3.fuelConsumptionDisplay || $scope.car2.fuelConsumptionDisplay != $scope.car3.fuelConsumptionDisplay) {
                    $scope.hasSamefuelConsumptionDisplay = 1
                } else {
                    $scope.hasSamefuelConsumptionDisplay = 0
                }
                if ($scope.car1.cruisingRangeDisplay != $scope.car2.cruisingRangeDisplay && $scope.car1.cruisingRangeDisplay != $scope.car3.cruisingRangeDisplay || $scope.car2.cruisingRangeDisplay != $scope.car3.cruisingRangeDisplay) {
                    $scope.hasSamecruisingRangeDisplay = 1
                } else {
                    $scope.hasSamecruisingRangeDisplay = 0
                }
                if ($scope.car1.tripMeter.description != $scope.car2.tripMeter.description && $scope.car1.tripMeter.description != $scope.car3.tripMeter.description || $scope.car2.tripMeter.description != $scope.car3.tripMeter.description) {
                    $scope.hasSametripMeter = 1
                } else {
                    $scope.hasSametripMeter = 0
                }
                if ($scope.car1.outsideTempDisplay != $scope.car2.outsideTempDisplay && $scope.car1.outsideTempDisplay != $scope.car3.outsideTempDisplay || $scope.car2.outsideTempDisplay != $scope.car3.outsideTempDisplay) {
                    $scope.hasSameoutsideTempDisplay = 1
                } else {
                    $scope.hasSameoutsideTempDisplay = 0
                }
                if ($scope.car1.instrumentPanelGarnish != $scope.car2.instrumentPanelGarnish && $scope.car1.instrumentPanelGarnish != $scope.car3.instrumentPanelGarnish || $scope.car2.instrumentPanelGarnish != $scope.car3.instrumentPanelGarnish) {
                    $scope.hasSameinstrumentPanelGarnish = 1
                } else {
                    $scope.hasSameinstrumentPanelGarnish = 0
                }
                if ($scope.car1.silverInsideDoorHandle.description != $scope.car2.silverInsideDoorHandle.description && $scope.car1.silverInsideDoorHandle.description != $scope.car3.silverInsideDoorHandle.description || $scope.car2.silverInsideDoorHandle.description != $scope.car3.silverInsideDoorHandle.description) {
                    $scope.hasSamesilverInsideDoorHandle = 1
                } else {
                    $scope.hasSamesilverInsideDoorHandle = 0
                }
                if ($scope.car1.seatBackFoldDown.description != $scope.car2.seatBackFoldDown.description && $scope.car1.seatBackFoldDown.description != $scope.car3.seatBackFoldDown.description || $scope.car2.seatBackFoldDown.description != $scope.car3.seatBackFoldDown.description) {
                    $scope.hasSameseatBackFoldDown = 1
                } else {
                    $scope.hasSameseatBackFoldDown = 0
                }
                if ($scope.car1.rearACVents != $scope.car2.rearACVents && $scope.car1.rearACVents != $scope.car3.rearACVents || $scope.car2.rearACVents != $scope.car3.rearACVents) {
                    $scope.hasSamerearACVents = 1
                } else {
                    $scope.hasSamerearACVents = 0
                }
                if ($scope.car1.ecoLamps != $scope.car2.ecoLamps && $scope.car1.ecoLamps != $scope.car3.ecoLamps || $scope.car2.ecoLamps != $scope.car3.ecoLamps) {
                    $scope.hasSameecoLamps = 1
                } else {
                    $scope.hasSameecoLamps = 0
                }
                if ($scope.car1.alloyWheels != $scope.car2.alloyWheels && $scope.car1.alloyWheels != $scope.car3.alloyWheels || $scope.car2.alloyWheels != $scope.car3.alloyWheels) {
                    $scope.hasSamealloyWheels = 1
                } else {
                    $scope.hasSamealloyWheels = 0
                }
                if ($scope.car1.fullTrimWheel.description != $scope.car2.fullTrimWheel.description && $scope.car1.fullTrimWheel.description != $scope.car3.fullTrimWheel.description || $scope.car2.fullTrimWheel.description != $scope.car3.fullTrimWheel.description) {
                    $scope.hasSamefullTrimWheel = 1
                } else {
                    $scope.hasSamefullTrimWheel = 0
                }
                if ($scope.car1.frontChromeGrill.description != $scope.car2.frontChromeGrill.description && $scope.car1.frontChromeGrill.description != $scope.car3.frontChromeGrill.description || $scope.car2.frontChromeGrill.description != $scope.car3.frontChromeGrill.description) {
                    $scope.hasSamefrontChromeGrill = 1
                } else {
                    $scope.hasSamefrontChromeGrill = 0
                }
                if ($scope.car1.turnIndictorOnORVM != $scope.car2.turnIndictorOnORVM && $scope.car1.turnIndictorOnORVM != $scope.car3.turnIndictorOnORVM || $scope.car2.turnIndictorOnORVM != $scope.car3.turnIndictorOnORVM) {
                    $scope.hasSameturnIndictorOnORVM = 1
                } else {
                    $scope.hasSameturnIndictorOnORVM = 0
                }
                if ($scope.car1.antenna.description != $scope.car2.antenna.description && $scope.car1.antenna.description != $scope.car3.antenna.description || $scope.car2.antenna.description != $scope.car3.antenna.description) {
                    $scope.hasSameantenna = 1
                } else {
                    $scope.hasSameantenna = 0
                }
                if ($scope.car1.bodyColorFrontBack.description != $scope.car2.bodyColorFrontBack.description && $scope.car1.bodyColorFrontBack.description != $scope.car3.bodyColorFrontBack.description || $scope.car2.bodyColorFrontBack.description != $scope.car3.bodyColorFrontBack.description) {
                    $scope.hasSamebodyColorFrontBack = 1
                } else {
                    $scope.hasSamebodyColorFrontBack = 0
                }
                if ($scope.car1.outdoorHandles.description != $scope.car2.outdoorHandles.description && $scope.car1.outdoorHandles.description != $scope.car3.outdoorHandles.description || $scope.car2.outdoorHandles.description != $scope.car3.outdoorHandles.description) {
                    $scope.hasSameoutdoorHandles = 1
                } else {
                    $scope.hasSameoutdoorHandles = 0
                }
                if ($scope.car1.oRVMColor.description != $scope.car2.oRVMColor.description && $scope.car1.oRVMColor.description != $scope.car3.oRVMColor.description || $scope.car2.oRVMColor.description != $scope.car3.oRVMColor.description) {
                    $scope.hasSameoRVMColor = 1
                } else {
                    $scope.hasSameoRVMColor = 0
                }
                if ($scope.car1.frontRearMudguards.description != $scope.car2.frontRearMudguards.description && $scope.car1.frontRearMudguards.description != $scope.car3.frontRearMudguards.description || $scope.car2.frontRearMudguards.description != $scope.car3.frontRearMudguards.description) {
                    $scope.hasSamefrontRearMudguards = 1
                } else {
                    $scope.hasSamefrontRearMudguards = 0
                }
                if ($scope.car1.headLamp.description != $scope.car2.headLamp.description && $scope.car1.headLamp.description != $scope.car3.headLamp.description || $scope.car2.headLamp.description != $scope.car3.headLamp.description) {
                    $scope.hasSameheadLamp = 1
                } else {
                    $scope.hasSameheadLamp = 0
                }
                if ($scope.car1.fogLamps != $scope.car2.fogLamps && $scope.car1.fogLamps != $scope.car3.fogLamps || $scope.car2.fogLamps != $scope.car3.fogLamps) {
                    $scope.hasSamefogLamps = 1
                } else {
                    $scope.hasSamefogLamps = 0
                }
                if ($scope.car1.rearWiperWasher.description != $scope.car2.rearWiperWasher.description && $scope.car1.rearWiperWasher.description != $scope.car3.rearWiperWasher.description || $scope.car2.rearWiperWasher.description != $scope.car3.rearWiperWasher.description) {
                    $scope.hasSamerearWiperWasher = 1
                } else {
                    $scope.hasSamerearWiperWasher = 0
                }
                if ($scope.car1.overallLength.value != $scope.car2.overallLength.value && $scope.car1.overallLength.value != $scope.car3.overallLength.value || $scope.car2.overallLength.value != $scope.car3.overallLength.value) {
                    $scope.hasSameoverallLength = 1
                } else {
                    $scope.hasSameoverallLength = 0
                }
                if ($scope.car1.overallWidth.value != $scope.car2.overallWidth.value && $scope.car1.overallWidth.value != $scope.car3.overallWidth.value || $scope.car2.overallWidth.value != $scope.car3.overallWidth.value) {
                    $scope.hasSameoverallWidth = 1
                } else {
                    $scope.hasSameoverallWidth = 0
                }
                if ($scope.car1.overallHeight.value != $scope.car2.overallHeight.value && $scope.car1.overallHeight.value != $scope.car3.overallHeight.value || $scope.car2.overallHeight.value != $scope.car3.overallHeight.value) {
                    $scope.hasSameoverallHeight = 1
                } else {
                    $scope.hasSameoverallHeight = 0
                }
                if ($scope.car1.wheelbase.value != $scope.car2.wheelbase.value && $scope.car1.wheelbase.value != $scope.car3.wheelbase.value || $scope.car2.wheelbase.value != $scope.car3.wheelbase.value) {
                    $scope.hasSamewheelbase = 1
                } else {
                    $scope.hasSamewheelbase = 0
                }
                if ($scope.car1.kerbWeight.value != $scope.car2.kerbWeight.value && $scope.car1.kerbWeight.value != $scope.car3.kerbWeight.value || $scope.car2.kerbWeight.value != $scope.car3.kerbWeight.value) {
                    $scope.hasSamekerbWeight = 1
                } else {
                    $scope.hasSamekerbWeight = 0
                }
                if ($scope.car1.tyreSize.value != $scope.car2.tyreSize.value && $scope.car1.tyreSize.value != $scope.car3.tyreSize.value || $scope.car2.tyreSize.value != $scope.car3.tyreSize.value) {
                    $scope.hasSametyreSize = 1
                } else {
                    $scope.hasSametyreSize = 0
                }
                if ($scope.car1.steeringSystem.value != $scope.car2.steeringSystem.value && $scope.car1.steeringSystem.value != $scope.car3.steeringSystem.value || $scope.car2.steeringSystem.value != $scope.car3.steeringSystem.value) {
                    $scope.hasSamesteeringSystem = 1
                } else {
                    $scope.hasSamesteeringSystem = 0
                }
                if ($scope.car1.musicSystem.description != $scope.car2.musicSystem.description && $scope.car1.musicSystem.description != $scope.car3.musicSystem.description || $scope.car2.musicSystem.description != $scope.car3.musicSystem.description) {
                    $scope.hasSamemusicSystem = 1
                } else {
                    $scope.hasSamemusicSystem = 0
                }
                if ($scope.car1.steeringMounted != $scope.car2.steeringMounted && $scope.car1.steeringMounted != $scope.car3.steeringMounted || $scope.car2.steeringMounted != $scope.car3.steeringMounted) {
                    $scope.hasSamesteeringMounted = 1
                } else {
                    $scope.hasSamesteeringMounted = 0
                }
                if ($scope.car1.touchScreen.description != $scope.car2.touchScreen.description && $scope.car1.touchScreen.description != $scope.car3.touchScreen.description || $scope.car3.touchScreen.description != $scope.car2.touchScreen.description) {
                    $scope.hasSametouchScreen = 1
                } else {
                    $scope.hasSametouchScreen = 0
                }
                if ($scope.car1.auxIn != $scope.car2.auxIn && $scope.car1.auxIn != $scope.car3.auxIn || $scope.car3.auxIn != $scope.car2.auxIn) {
                    $scope.hasSameauxIn = 1
                } else {
                    $scope.hasSameauxIn = 0
                }
                if ($scope.car1.displacement.value != $scope.car2.displacement.value && $scope.car1.displacement.value != $scope.car3.displacement.value || $scope.car2.displacement.value != $scope.car3.displacement.value) {
                    $scope.hasSamedisplacement = 1
                } else {
                    $scope.hasSamedisplacement = 0
                }
                if ($scope.car1.power.value != $scope.car2.power.value && $scope.car1.power.value != $scope.car3.power.value || $scope.car3.power.value != $scope.car2.power.value) {
                    $scope.hasSamepower = 1
                } else {
                    $scope.hasSamepower = 0
                }
                if ($scope.car1.torque.value != $scope.car2.torque.value && $scope.car1.torque.value != $scope.car3.torque.value || $scope.car3.torque.value != $scope.car2.torque.value) {
                    $scope.hasSametorque = 1
                } else {
                    $scope.hasSametorque = 0
                }
                if ($scope.car1.transAutomatic.value != $scope.car2.transAutomatic.value && $scope.car1.transAutomatic.value != $scope.car3.transAutomatic.value || $scope.car3.transAutomatic.value != $scope.car2.transAutomatic.value) {
                    $scope.hasSametransAutomatic = 1
                } else {
                    $scope.hasSametransAutomatic = 0
                }
                if ($scope.car1.transManual.value != $scope.car2.transManual.value && $scope.car1.transManual.value != $scope.car3.transManual.value || $scope.car3.transManual.value != $scope.car2.transManual.value) {
                    $scope.hasSametransManual = 1
                } else {
                    $scope.hasSametransManual = 0
                }
                if ($scope.car1.turningRadius.value != $scope.car2.turningRadius.value && $scope.car1.turningRadius.value != $scope.car3.turningRadius.value || $scope.car3.turningRadius.value != $scope.car2.turningRadius.value) {
                    $scope.hasSameturningRadius = 1
                } else {
                    $scope.hasSameturningRadius = 0
                }
                if ($scope.car1.suspFront.value != $scope.car2.suspFront.value && $scope.car1.suspFront.value != $scope.car3.suspFront.value || $scope.car3.suspFront.value != $scope.car2.suspFront.value) {
                    $scope.hasSamesuspFront = 1
                } else {
                    $scope.hasSamesuspFront = 0
                }
                if ($scope.car1.suspRear.value != $scope.car2.suspRear.value && $scope.car1.suspRear.value != $scope.car3.suspRear.value || $scope.car3.suspRear.value != $scope.car2.suspRear.value) {
                    $scope.hasSamesuspRear = 1
                } else {
                    $scope.hasSamesuspRear = 0
                }
                if ($scope.car1.brakesFront.value != $scope.car2.brakesFront.value && $scope.car1.brakesFront.value != $scope.car3.brakesFront.value || $scope.car3.brakesFront.value != $scope.car2.brakesFront.value) {
                    $scope.hasSamebrakesFront = 1
                } else {
                    $scope.hasSamebrakesFront = 0
                }
                if ($scope.car1.brakesRear.value != $scope.car2.brakesRear.value && $scope.car1.brakesRear.value != $scope.car3.brakesRear.value || $scope.car3.brakesRear.value != $scope.car2.brakesRear.value) {
                    $scope.hasSamebrakesRear = 1
                } else {
                    $scope.hasSamebrakesRear = 0
                }
                if ($scope.car1.airBags != $scope.car2.airBags && $scope.car1.airBags != $scope.car3.airBags || $scope.car3.airBags != $scope.car2.airBags) {
                    $scope.hasSameairBags = 1
                } else {
                    $scope.hasSameairBags = 0
                }
                if ($scope.car1.aBSwithEBD != $scope.car2.aBSwithEBD && $scope.car1.aBSwithEBD != $scope.car3.aBSwithEBD || $scope.car3.aBSwithEBD != $scope.car2.aBSwithEBD) {
                    $scope.hasSameaBSwithEBD = 1
                } else {
                    $scope.hasSameaBSwithEBD = 0
                }
                if ($scope.car1.immobilizer != $scope.car2.immobilizer && $scope.car1.immobilizer != $scope.car3.immobilizer || $scope.car3.immobilizer != $scope.car2.immobilizer) {
                    $scope.hasSameimmobilizer = 1
                } else {
                    $scope.hasSameimmobilizer = 0
                }
                if ($scope.car1.vehStabilityAssist.description != $scope.car2.vehStabilityAssist.description && $scope.car1.vehStabilityAssist.description != $scope.car3.vehStabilityAssist.description || $scope.car3.vehStabilityAssist.description != $scope.car2.vehStabilityAssist.description) {
                    $scope.hasSamevehStabilityAssist = 1
                } else {
                    $scope.hasSamevehStabilityAssist = 0
                }
                if ($scope.car1.securityAlarm != $scope.car2.securityAlarm && $scope.car1.securityAlarm != $scope.car3.securityAlarm || $scope.car3.securityAlarm != $scope.car2.securityAlarm) {
                    $scope.hasSamesecurityAlarm = 1
                } else {
                    $scope.hasSamesecurityAlarm = 0
                }
                if ($scope.car1.driverSeatBeltReminder != $scope.car2.driverSeatBeltReminder && $scope.car1.driverSeatBeltReminder != $scope.car3.driverSeatBeltReminder || $scope.car3.driverSeatBeltReminder != $scope.car2.driverSeatBeltReminder) {
                    $scope.hasSamedriverSeatBeltReminder = 1
                } else {
                    $scope.hasSamedriverSeatBeltReminder = 0
                }
            } else {
                removeHighlight();
                highLightNonCommonisChecked = !0;
                if ($scope.car1.variant_data.variant.fuel_type != $scope.car2.variant_data.variant.fuel_type) {
                    $scope.hasSamefueltype = 1
                } else {
                    $scope.hasSamefueltype = 0
                }
                if ($scope.car1.fuelCapacity.value != $scope.car2.fuelCapacity.value) {
                    $scope.hasSamefueltype = 1
                } else {
                    $scope.hasSamefueltype = 0
                }
                if ($scope.car1.variant_data.variant.seating != $scope.car2.variant_data.variant.seating) {
                    $scope.hasSameseating = 1
                } else {
                    $scope.hasSameseating = 0
                }
                if ($scope.car1.displacement.value != $scope.car2.displacement.value) {
                    $scope.hasSamedisplacement = 1
                } else {
                    $scope.hasSamedisplacement = 0
                }
                if ($scope.car1.fuelEfficiency.value != $scope.car2.fuelEfficiency.value) {
                    $scope.hasSamefuelEfficiency = 1
                } else {
                    $scope.hasSamefuelEfficiency = 0
                }
                if ($scope.car1.variant_data.variant.transmission_type != $scope.car2.variant_data.variant.transmission_type) {
                    $scope.hasSameTransmissiontype = 1
                } else {
                    $scope.hasSameTransmissiontype = 0
                }
                if ($scope.car1.upholstery.description != $scope.car2.upholstery.description) {
                    $scope.hasSameupholstery = 1
                } else {
                    $scope.hasSameupholstery = 0
                }
                if ($scope.car1.leatherWrappedSteeringWheel.description != $scope.car2.leatherWrappedSteeringWheel.description) {
                    $scope.hasSameleatherWrappedSteeringWheel = 1
                } else {
                    $scope.hasSameleatherWrappedSteeringWheel = 0
                }
                if ($scope.car1.leatherGearShiftKnob.description != $scope.car2.leatherGearShiftKnob.description) {
                    $scope.hasSameleatherGearShiftKnob = 1
                } else {
                    $scope.hasSameleatherGearShiftKnob = 0
                }
                if ($scope.car1.multiInformationDisplay != $scope.car2.multiInformationDisplay) {
                    $scope.hasSamemultiInformationDisplay = 1
                } else {
                    $scope.hasSamemultiInformationDisplay = 0
                }
                if ($scope.car1.fuelConsumptionDisplay != $scope.car2.fuelConsumptionDisplay) {
                    $scope.hasSamefuelConsumptionDisplay = 1
                } else {
                    $scope.hasSamefuelConsumptionDisplay = 0
                }
                if ($scope.car1.cruisingRangeDisplay != $scope.car2.cruisingRangeDisplay) {
                    $scope.hasSamecruisingRangeDisplay = 1
                } else {
                    $scope.hasSamecruisingRangeDisplay = 0
                }
                if ($scope.car1.tripMeter.description != $scope.car2.tripMeter.description) {
                    $scope.hasSametripMeter = 1
                } else {
                    $scope.hasSametripMeter = 0
                }
                if ($scope.car1.outsideTempDisplay != $scope.car2.outsideTempDisplay) {
                    $scope.hasSameoutsideTempDisplay = 1
                } else {
                    $scope.hasSameoutsideTempDisplay = 0
                }
                if ($scope.car1.instrumentPanelGarnish != $scope.car2.instrumentPanelGarnish) {
                    $scope.hasSameinstrumentPanelGarnish = 1
                } else {
                    $scope.hasSameinstrumentPanelGarnish = 0
                }
                if ($scope.car1.silverInsideDoorHandle.description != $scope.car2.silverInsideDoorHandle.description) {
                    $scope.hasSamesilverInsideDoorHandle = 1
                } else {
                    $scope.hasSamesilverInsideDoorHandle = 0
                }
                if ($scope.car1.seatBackFoldDown.description != $scope.car2.seatBackFoldDown.description) {
                    $scope.hasSameseatBackFoldDown = 1
                } else {
                    $scope.hasSameseatBackFoldDown = 0
                }
                if ($scope.car1.rearACVents != $scope.car2.rearACVents) {
                    $scope.hasSamerearACVents = 1
                } else {
                    $scope.hasSamerearACVents = 0
                }
                if ($scope.car1.ecoLamps != $scope.car2.ecoLamps) {
                    $scope.hasSameecoLamps = 1
                } else {
                    $scope.hasSameecoLamps = 0
                }
                if ($scope.car1.alloyWheels != $scope.car2.alloyWheels) {
                    $scope.hasSamealloyWheels = 1
                } else {
                    $scope.hasSamealloyWheels = 0
                }
                if ($scope.car1.fullTrimWheel.description != $scope.car2.fullTrimWheel.description) {
                    $scope.hasSamefullTrimWheel = 1
                } else {
                    $scope.hasSamefullTrimWheel = 0
                }
                if ($scope.car1.frontChromeGrill.description != $scope.car2.frontChromeGrill.description) {
                    $scope.hasSamefrontChromeGrill = 1
                } else {
                    $scope.hasSamefrontChromeGrill = 0
                }
                if ($scope.car1.turnIndictorOnORVM != $scope.car2.turnIndictorOnORVM) {
                    $scope.hasSameturnIndictorOnORVM = 1
                } else {
                    $scope.hasSameturnIndictorOnORVM = 0
                }
                if ($scope.car1.antenna.description != $scope.car2.antenna.description) {
                    $scope.hasSameantenna = 1
                } else {
                    $scope.hasSameantenna = 0
                }
                if ($scope.car1.bodyColorFrontBack.description != $scope.car2.bodyColorFrontBack.description) {
                    $scope.hasSamebodyColorFrontBack = 1
                } else {
                    $scope.hasSamebodyColorFrontBack = 0
                }
                if ($scope.car1.outdoorHandles.description != $scope.car2.outdoorHandles.description) {
                    $scope.hasSameoutdoorHandles = 1
                } else {
                    $scope.hasSameoutdoorHandles = 0
                }
                if ($scope.car1.oRVMColor.description != $scope.car2.oRVMColor.description) {
                    $scope.hasSameoRVMColor = 1
                } else {
                    $scope.hasSameoRVMColor = 0
                }
                if ($scope.car1.frontRearMudguards.description != $scope.car2.frontRearMudguards.description) {
                    $scope.hasSamefrontRearMudguards = 1
                } else {
                    $scope.hasSamefrontRearMudguards = 0
                }
                if ($scope.car1.headLamp.description != $scope.car2.headLamp.description) {
                    $scope.hasSameheadLamp = 1
                } else {
                    $scope.hasSameheadLamp = 0
                }
                if ($scope.car1.fogLamps != $scope.car2.fogLamps) {
                    $scope.hasSamefogLamps = 1
                } else {
                    $scope.hasSamefogLamps = 0
                }
                if ($scope.car1.rearWiperWasher.description != $scope.car2.rearWiperWasher.description) {
                    $scope.hasSamerearWiperWasher = 1
                } else {
                    $scope.hasSamerearWiperWasher = 0
                }
                if ($scope.car1.overallLength.value != $scope.car2.overallLength.value) {
                    $scope.hasSameoverallLength = 1
                } else {
                    $scope.hasSameoverallLength = 0
                }
                if ($scope.car1.overallWidth.value != $scope.car2.overallWidth.value) {
                    $scope.hasSameoverallWidth = 1
                } else {
                    $scope.hasSameoverallWidth = 0
                }
                if ($scope.car1.overallHeight.value != $scope.car2.overallHeight.value) {
                    $scope.hasSameoverallHeight = 1
                } else {
                    $scope.hasSameoverallHeight = 0
                }
                if ($scope.car1.wheelbase.value != $scope.car2.wheelbase.value) {
                    $scope.hasSamewheelbase = 1
                } else {
                    $scope.hasSamewheelbase = 0
                }
                if ($scope.car1.kerbWeight.value != $scope.car2.kerbWeight.value) {
                    $scope.hasSamekerbWeight = 1
                } else {
                    $scope.hasSamekerbWeight = 0
                }
                if ($scope.car1.tyreSize.value != $scope.car2.tyreSize.value) {
                    $scope.hasSametyreSize = 1
                } else {
                    $scope.hasSametyreSize = 0
                }
                if ($scope.car1.steeringSystem.value != $scope.car2.steeringSystem.value) {
                    $scope.hasSamesteeringSystem = 1
                } else {
                    $scope.hasSamesteeringSystem = 0
                }
                if ($scope.car1.steeringSystem.value != $scope.car2.steeringSystem.value) {
                    $scope.hasSamesteeringSystem = 1
                } else {
                    $scope.hasSamesteeringSystem = 0
                }
                if ($scope.car1.musicSystem.description != $scope.car2.musicSystem.description) {
                    $scope.hasSamemusicSystem = 1
                } else {
                    $scope.hasSamemusicSystem = 0
                }
                if ($scope.car1.steeringMounted != $scope.car2.steeringMounted) {
                    $scope.hasSamesteeringMounted = 1
                } else {
                    $scope.hasSamesteeringMounted = 0
                }
                if ($scope.car1.touchScreen.description != $scope.car2.touchScreen.description) {
                    $scope.hasSametouchScreen = 1
                } else {
                    $scope.hasSametouchScreen = 0
                }
                if ($scope.car1.auxIn != $scope.car2.auxIn) {
                    $scope.hasSameauxIn = 1
                } else {
                    $scope.hasSameauxIn = 0
                }
                if ($scope.car1.displacement.value != $scope.car2.displacement.value) {
                    $scope.hasSamedisplacement = 1
                } else {
                    $scope.hasSamedisplacement = 0
                }
                if ($scope.car1.power.value != $scope.car2.power.value) {
                    $scope.hasSamepower = 1
                } else {
                    $scope.hasSamepower = 0
                }
                if ($scope.car1.torque.value != $scope.car2.torque.value) {
                    $scope.hasSametorque = 1
                } else {
                    $scope.hasSametorque = 0
                }
                if ($scope.car1.transAutomatic.value != $scope.car2.transAutomatic.value) {
                    $scope.hasSametransAutomatic = 1
                } else {
                    $scope.hasSametransAutomatic = 0
                }
                if ($scope.car1.transManual.value != $scope.car2.transManual.value) {
                    $scope.hasSametransManual = 1
                } else {
                    $scope.hasSametransManual = 0
                }
                if ($scope.car1.turningRadius.value != $scope.car2.turningRadius.value) {
                    $scope.hasSameturningRadius = 1
                } else {
                    $scope.hasSameturningRadius = 0
                }
                if ($scope.car1.suspFront.value != $scope.car2.suspFront.value) {
                    $scope.hasSamesuspFront = 1
                } else {
                    $scope.hasSamesuspFront = 0
                }
                if ($scope.car1.suspRear.value != $scope.car2.suspRear.value) {
                    $scope.hasSamesuspRear = 1
                } else {
                    $scope.hasSamesuspRear = 0
                }
                if ($scope.car1.brakesFront.value != $scope.car2.brakesFront.value) {
                    $scope.hasSamebrakesFront = 1
                } else {
                    $scope.hasSamebrakesFront = 0
                }
                if ($scope.car1.brakesRear.value != $scope.car2.brakesRear.value) {
                    $scope.hasSamebrakesRear = 1
                } else {
                    $scope.hasSamebrakesRear = 0
                }
                if ($scope.car1.airBags != $scope.car2.airBags) {
                    $scope.hasSameairBags = 1
                } else {
                    $scope.hasSameairBags = 0
                }
                if ($scope.car1.aBSwithEBD != $scope.car2.aBSwithEBD) {
                    $scope.hasSameaBSwithEBD = 1
                } else {
                    $scope.hasSameaBSwithEBD = 0
                }
                if ($scope.car1.immobilizer != $scope.car2.immobilizer) {
                    $scope.hasSameimmobilizer = 1
                } else {
                    $scope.hasSameimmobilizer = 0
                }
                if ($scope.car1.vehStabilityAssist.description != $scope.car2.vehStabilityAssist.description) {
                    $scope.hasSamevehStabilityAssist = 1
                } else {
                    $scope.hasSamevehStabilityAssist = 0
                }
                if ($scope.car1.securityAlarm != $scope.car2.securityAlarm) {
                    $scope.hasSamesecurityAlarm = 1
                } else {
                    $scope.hasSamesecurityAlarm = 0
                }
                if ($scope.car1.driverSeatBeltReminder != $scope.car2.driverSeatBeltReminder) {
                    $scope.hasSamedriverSeatBeltReminder = 1
                } else {
                    $scope.hasSamedriverSeatBeltReminder = 0
                }
            }
        } else {
            removeHighlight()
        }
    };

    function removeHighlight() {
        try {
            $scope.hasSamefueltype = 0;
            $scope.hasSamefuelCapacity = 0;
            $scope.hasSameseating = 0;
            $scope.hasSamedisplacement = 0;
            $scope.hasSamefuelEfficiency = 0;
            $scope.hasSameTransmissiontype = 0;
            $scope.hasSameupholstery = 0;
            $scope.hasSameleatherWrappedSteeringWheel = 0;
            $scope.hasSameleatherGearShiftKnob = 0;
            $scope.hasSamemultiInformationDisplay = 0;
            $scope.hasSamefuelConsumptionDisplay = 0;
            $scope.hasSamecruisingRangeDisplay = 0;
            $scope.hasSametripMeter = 0;
            $scope.hasSameoutsideTempDisplay = 0;
            $scope.hasSameinstrumentPanelGarnish = 0;
            $scope.hasSamesilverInsideDoorHandle = 0;
            $scope.hasSameseatBackFoldDown = 0;
            $scope.hasSamerearACVents = 0;
            $scope.hasSameecoLamps = 0;
            $scope.hasSameoverallLength = 0;
            $scope.hasSameoverallWidth = 0;
            $scope.hasSameoverallHeight = 0;
            $scope.hasSamewheelbase = 0;
            $scope.hasSamekerbWeight = 0;
            $scope.hasSametyreSize = 0;
            $scope.hasSamesteeringSystem = 0;
            $scope.hasSamemusicSystem = 0;
            $scope.hasSamesteeringMounted = 0;
            $scope.hasSametouchScreen = 0;
            $scope.hasSameauxIn = 0;
            $scope.hasSamedisplacement = 0;
            $scope.hasSamevalves = 0;
            $scope.hasSamepower = 0;
            $scope.hasSametorque = 0;
            $scope.hasSametransAutomatic = 0;
            $scope.hasSametransManual = 0;
            $scope.hasSameturningRadius = 0;
            $scope.hasSamesuspFront = 0;
            $scope.hasSamesuspRear = 0;
            $scope.hasSamebrakesFront = 0;
            $scope.hasSamebrakesRear = 0;
            $scope.hasSameairBags = 0;
            $scope.hasSameaBSwithEBD = 0;
            $scope.hasSameimmobilizer = 0;
            $scope.hasSamevehStabilityAssist = 0;
            $scope.hasSamedualHorn = 0;
            $scope.hasSamesecurityAlarm = 0;
            $scope.hasSamedriverSeatBeltReminder = 0;
            $scope.hasSamealloyWheels = 0;
            $scope.hasSamefullTrimWheel = 0;
            $scope.hasSamefrontChromeGrill = 0;
            $scope.hasSameturnIndictorOnORVM = 0;
            $scope.hasSameantenna = 0;
            $scope.hasSamebodyColorFrontBack = 0;
            $scope.hasSameoutdoorHandles = 0;
            $scope.hasSameoRVMColor = 0;
            $scope.hasSamefrontRearMudguards = 0;
            $scope.hasSameheadLamp = 0;
            $scope.hasSamefogLamps = 0;
            $scope.hasSamerearWiperWasher = 0;
            $scope.hasSameacWithHeater = 0;
            $scope.hasSamepowerSteering = 0;
            $scope.hasSamesteeringType = 0;
            $scope.hasSameelectricalFoldingORVM = 0;
            $scope.hasSamecLKE = 0;
            $scope.hasSamepowerWindow = 0;
            $scope.hasSamerearParcelTray = 0;
            $scope.hasSamecargoLight = 0;
            $scope.hasSamepushStart = 0;
            $scope.hasSamerearACVents = 0;
            $scope.hasSameautoDoorLockBySpeed = 0;
            $scope.hasSamecupHolders = 0;
            $scope.hasSamedayNightIRVM = 0;
            $scope.hasSameignitionKeyReminder = 0;
            $scope.hasSamedriverSeatHeightAdjustment = 0;
            $scope.hasSamerearSeatCenterArmrest = 0;
            $scope.hasSamehornType = 0;
            $scope.hasSameaceBody = 0;
            $scope.hasSamehighMountStopLamp = 0;
            highLightCommonisChecked = !1;
            highLightNonCommonisChecked = !1
        } catch (e) {}
    }
    $scope.getData = function getData(query) {
        removeHighlight();
        highLightCommonisChecked = !1;
        highLightNonCommonisChecked = !1;
        $scope.common = !1;
        $scope.differences = !1;
        $scope.test = !0;
        $scope.message = "Loading Cars...";
        $localStorage.query = query;
        $http.post(api_url + "/website/cars_data", query).then(function(success) {
            $scope.data = success.data.overview;
            var tempArray = [];
            angular.forEach($scope.data, function(key, value) {
                tempArray.push(key)
            });
            $scope.car1 = tempArray[0];
            angular.forEach($scope.car1.techinical_specification, function(key, value) {
                try {
                    if (key.specification_name_id == 21) {
                        $scope.car1.fuelCapacity = key
                    }
                    if (key.specification_name_id == 8) {
                        $scope.car1.displacement = key
                    }
                    if (key.specification_name_id == 12) {
                        $scope.car1.fuelEfficiency = key
                    }
                    if (key.specification_name_id == 15) {
                        $scope.car1.overallLength = key
                    }
                    if (key.specification_name_id == 16) {
                        $scope.car1.overallWidth = key
                    }
                    if (key.specification_name_id == 17) {
                        $scope.car1.overallHeight = key
                    }
                    if (key.specification_name_id == 18) {
                        $scope.car1.wheelbase = key
                    }
                    if (key.specification_name_id == 19) {
                        $scope.car1.kerbWeight = key
                    }
                    if (key.specification_name_id == 20) {
                        $scope.car1.tyreSize = key
                    }
                    if (key.specification_name_id == 23) {
                        $scope.car1.steeringSystem = key
                    }
                    if (key.specification_name_id == 8) {
                        $scope.car1.displacement = key
                    }
                    if (key.specification_name_id == 9) {
                        $scope.car1.valves = key
                    }
                    if (key.specification_name_id == 10) {
                        $scope.car1.power = key
                    }
                    if (key.specification_name_id == 11) {
                        $scope.car1.torque = key
                    }
                    if (key.specification_name_id == 13) {
                        $scope.car1.transAutomatic = key
                    }
                    if (key.specification_name_id == 14) {
                        $scope.car1.transManual = key
                    }
                    if (key.specification_name_id == 24) {
                        $scope.car1.turningRadius = key
                    }
                    if (key.specification_name_id == 25) {
                        $scope.car1.suspFront = key
                    }
                    if (key.specification_name_id == 26) {
                        $scope.car1.suspRear = key
                    }
                    if (key.specification_name_id == 29) {
                        $scope.car1.brakesFront = key
                    }
                    if (key.specification_name_id == 30) {
                        $scope.car1.brakesRear = key
                    }
                } catch (e) {}
            });
            angular.forEach($scope.car1.variant_data.variant_feature_showcases, function(key, value) {
                try {
                    if (key.key_feature_name_id == 8) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.alloyWheels = !0
                        } else {
                            $scope.car1.alloyWheels = !1
                        }
                    }
                    if (key.key_feature_name_id == 9) {
                        $scope.car1.fullTrimWheel = key
                    }
                    if (key.key_feature_name_id == 10) {
                        $scope.car1.frontChromeGrill = key
                    }
                    if (key.key_feature_name_id == 11) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.turnIndictorOnORVM = !0
                        } else {
                            $scope.car1.turnIndictorOnORVM = !1
                        }
                    }
                    if (key.key_feature_name_id == 12) {
                        $scope.car1.antenna = key
                    }
                    if (key.key_feature_name_id == 13) {
                        $scope.car1.bodyColorFrontBack = key
                    }
                    if (key.key_feature_name_id == 14) {
                        $scope.car1.outdoorHandles = key
                    }
                    if (key.key_feature_name_id == 15) {
                        $scope.car1.oRVMColor = key
                    }
                    if (key.key_feature_name_id == 16) {
                        $scope.car1.frontRearMudguards = key
                    }
                    if (key.key_feature_name_id == 17) {
                        $scope.car1.headLamp = key
                    }
                    if (key.key_feature_name_id == 18) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.fogLamps = !0
                        } else {
                            $scope.car1.fogLamps = !1
                        }
                    }
                    if (key.key_feature_name_id == 19) {
                        $scope.car1.rearWiperWasher = key
                    }
                    if (key.key_feature_name_id == 20) {
                        $scope.car1.upholstery = key
                    }
                    if (key.key_feature_name_id == 21) {
                        $scope.car1.leatherWrappedSteeringWheel = key
                    }
                    if (key.key_feature_name_id == 22) {
                        $scope.car1.leatherGearShiftKnob = key
                    }
                    if (key.key_feature_name_id == 23) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.multiInformationDisplay = !0
                        } else {
                            $scope.car1.multiInformationDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 24) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.fuelConsumptionDisplay = !0
                        } else {
                            $scope.car1.fuelConsumptionDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 25) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.cruisingRangeDisplay = !0
                        } else {
                            $scope.car1.cruisingRangeDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 26) {
                        $scope.car1.tripMeter = key
                    }
                    if (key.key_feature_name_id == 27) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.outsideTempDisplay = !0
                        } else {
                            $scope.car1.outsideTempDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 28) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.instrumentPanelGarnish = !0
                        } else {
                            $scope.car1.instrumentPanelGarnish = !1
                        }
                    }
                    if (key.key_feature_name_id == 29) {
                        $scope.car1.silverInsideDoorHandle = key
                    }
                    if (key.key_feature_name_id == 30) {
                        $scope.car1.seatBackFoldDown = key
                    }
                    if (key.key_feature_name_id == 31) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.rearACVents = !0
                        } else {
                            $scope.car1.rearACVents = !1
                        }
                    }
                    if (key.key_feature_name_id == 32) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.ecoLamps = !0
                        } else {
                            $scope.car1.ecoLamps = !1
                        }
                    }
                    if (key.key_feature_name_id == 39) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.acWithHeater = !0
                        } else {
                            $scope.car1.acWithHeater = !1
                        }
                    }
                    if (key.key_feature_name_id == 40) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.powerSteering = !0
                        } else {
                            $scope.car1.powerSteering = !1
                        }
                    }
                    if (key.key_feature_name_id == 41) {
                        $scope.car1.steeringType = key
                    }
                    if (key.key_feature_name_id == 42) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.electricalFoldingORVM = !0
                        } else {
                            $scope.car1.electricalFoldingORVM = !1
                        }
                    }
                    if (key.key_feature_name_id == 43) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.cLKE = !0
                        } else {
                            $scope.car1.cLKE = !1
                        }
                    }
                    if (key.key_feature_name_id == 44) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.powerWindow = !0
                        } else {
                            $scope.car1.powerWindow = !1
                        }
                    }
                    if (key.key_feature_name_id == 45) {
                        $scope.car1.rearParcelTray = key
                    }
                    if (key.key_feature_name_id == 46) {
                        $scope.car1.cargoLight = key
                    }
                    if (key.key_feature_name_id == 47) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.pushStart = !0
                        } else {
                            $scope.car1.pushStart = !1
                        }
                    }
                    if (key.key_feature_name_id == 48) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.rearACVents = !0
                        } else {
                            $scope.car1.rearACVents = !1
                        }
                    }
                    if (key.key_feature_name_id == 49) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.autoDoorLockBySpeed = !0
                        } else {
                            $scope.car1.autoDoorLockBySpeed = !1
                        }
                    }
                    if (key.key_feature_name_id == 50) {
                        $scope.car1.cupHolders = key
                    }
                    if (key.key_feature_name_id == 51) {
                        $scope.car1.dayNightIRVM = key
                    }
                    if (key.key_feature_name_id == 52) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.ignitionKeyReminder = !0
                        } else {
                            $scope.car1.ignitionKeyReminder = !1
                        }
                    }
                    if (key.key_feature_name_id == 53) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.driverSeatHeightAdjustment = !0
                        } else {
                            $scope.car1.driverSeatHeightAdjustment = !1
                        }
                    }
                    if (key.key_feature_name_id == 54) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.rearSeatCenterArmrest = !0
                        } else {
                            $scope.car1.rearSeatCenterArmrest = !1
                        }
                    }
                    if (key.key_feature_name_id == 55) {
                        $scope.car1.hornType = key
                    }
                    if (key.key_feature_name_id == 56) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.aceBody = !0
                        } else {
                            $scope.car1.aceBody = !1
                        }
                    }
                    if (key.key_feature_name_id == 57) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.airBags = !0
                        } else {
                            $scope.car1.airBags = !1
                        }
                    }
                    if (key.key_feature_name_id == 58) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.aBSwithEBD = !0
                        } else {
                            $scope.car1.aBSwithEBD = !1
                        }
                    }
                    if (key.key_feature_name_id == 59) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.immobilizer = !0
                        } else {
                            $scope.car1.immobilizer = !1
                        }
                    }
                    if (key.key_feature_name_id == 60) {
                        $scope.car1.vehStabilityAssist = key
                    }
                    if (key.key_feature_name_id == 61) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.dualHorn = !0
                        } else {
                            $scope.car1.dualHorn = !1
                        }
                    }
                    if (key.key_feature_name_id == 62) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.securityAlarm = !0
                        } else {
                            $scope.car1.securityAlarm = !1
                        }
                    }
                    if (key.key_feature_name_id == 63) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.driverSeatBeltReminder = !0
                        } else {
                            $scope.car1.driverSeatBeltReminder = !1
                        }
                    }
                    if (key.key_feature_name_id == 64) {
                        $scope.car1.highMountStopLamp = key
                    }
                    if (key.key_feature_name_id == 33) {
                        $scope.car1.musicSystem = key
                    }
                    if (key.key_feature_name_id == 34) {
                        $scope.car1.bluetoothAudio = key
                    }
                    if (key.key_feature_name_id == 35) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.steeringMounted = !0
                        } else {
                            $scope.car1.steeringMounted = !1
                        }
                    }
                    if (key.key_feature_name_id == 36) {
                        $scope.car1.touchScreen = key
                    }
                    if (key.key_feature_name_id == 37) {
                        $scope.car1.speakers = key
                    }
                    if (key.key_feature_name_id == 38) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car1.auxIn = !0
                        } else {
                            $scope.car1.auxIn = !1
                        }
                    }
                } catch (e) {}
            });
            
            $scope.car2 = tempArray[1];
            angular.forEach($scope.car2.techinical_specification, function(key, value) {
                try {
                    if (key.specification_name_id == 21) {
                        $scope.car2.fuelCapacity = key
                    }
                    if (key.specification_name_id == 8) {
                        $scope.car2.displacement = key
                    }
                    if (key.specification_name_id == 12) {
                        $scope.car2.fuelEfficiency = key
                    }
                    if (key.specification_name_id == 15) {
                        $scope.car2.overallLength = key
                    }
                    if (key.specification_name_id == 16) {
                        $scope.car2.overallWidth = key
                    }
                    if (key.specification_name_id == 17) {
                        $scope.car2.overallHeight = key
                    }
                    if (key.specification_name_id == 18) {
                        $scope.car2.wheelbase = key
                    }
                    if (key.specification_name_id == 19) {
                        $scope.car2.kerbWeight = key
                    }
                    if (key.specification_name_id == 20) {
                        $scope.car2.tyreSize = key
                    }
                    if (key.specification_name_id == 23) {
                        $scope.car2.steeringSystem = key
                    }
                    if (key.specification_name_id == 8) {
                        $scope.car2.displacement = key
                    }
                    if (key.specification_name_id == 9) {
                        $scope.car2.valves = key
                    }
                    if (key.specification_name_id == 10) {
                        $scope.car2.power = key
                    }
                    if (key.specification_name_id == 11) {
                        $scope.car2.torque = key
                    }
                    if (key.specification_name_id == 13) {
                        $scope.car2.transAutomatic = key
                    }
                    if (key.specification_name_id == 14) {
                        $scope.car2.transManual = key
                    }
                    if (key.specification_name_id == 24) {
                        $scope.car2.turningRadius = key
                    }
                    if (key.specification_name_id == 25) {
                        $scope.car2.suspFront = key
                    }
                    if (key.specification_name_id == 26) {
                        $scope.car2.suspRear = key
                    }
                    if (key.specification_name_id == 29) {
                        $scope.car2.brakesFront = key
                    }
                    if (key.specification_name_id == 30) {
                        $scope.car2.brakesRear = key
                    }
                } catch (e) {}
            });
            angular.forEach($scope.car2.variant_data.variant_feature_showcases, function(key, value) {
                try {
                    if (key.key_feature_name_id == 8) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.alloyWheels = !0
                        } else {
                            $scope.car2.alloyWheels = !1
                        }
                    }
                    if (key.key_feature_name_id == 9) {
                        $scope.car2.fullTrimWheel = key
                    }
                    if (key.key_feature_name_id == 10) {
                        $scope.car2.frontChromeGrill = key
                    }
                    if (key.key_feature_name_id == 11) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.turnIndictorOnORVM = !0
                        } else {
                            $scope.car2.turnIndictorOnORVM = !1
                        }
                    }
                    if (key.key_feature_name_id == 12) {
                        $scope.car2.antenna = key
                    }
                    if (key.key_feature_name_id == 13) {
                        $scope.car2.bodyColorFrontBack = key
                    }
                    if (key.key_feature_name_id == 14) {
                        $scope.car2.outdoorHandles = key
                    }
                    if (key.key_feature_name_id == 15) {
                        $scope.car2.oRVMColor = key
                    }
                    if (key.key_feature_name_id == 16) {
                        $scope.car2.frontRearMudguards = key
                    }
                    if (key.key_feature_name_id == 17) {
                        $scope.car2.headLamp = key
                    }
                    if (key.key_feature_name_id == 18) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.fogLamps = !0
                        } else {
                            $scope.car2.fogLamps = !1
                        }
                    }
                    if (key.key_feature_name_id == 19) {
                        $scope.car2.rearWiperWasher = key
                    }
                    if (key.key_feature_name_id == 20) {
                        $scope.car2.upholstery = key
                    }
                    if (key.key_feature_name_id == 21) {
                        $scope.car2.leatherWrappedSteeringWheel = key
                    }
                    if (key.key_feature_name_id == 22) {
                        $scope.car2.leatherGearShiftKnob = key
                    }
                    if (key.key_feature_name_id == 23) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.multiInformationDisplay = !0
                        } else {
                            $scope.car2.multiInformationDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 24) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.fuelConsumptionDisplay = !0
                        } else {
                            $scope.car2.fuelConsumptionDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 25) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.cruisingRangeDisplay = !0
                        } else {
                            $scope.car2.cruisingRangeDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 26) {
                        $scope.car2.tripMeter = key
                    }
                    if (key.key_feature_name_id == 27) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.outsideTempDisplay = !0
                        } else {
                            $scope.car2.outsideTempDisplay = !1
                        }
                    }
                    if (key.key_feature_name_id == 28) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.instrumentPanelGarnish = !0
                        } else {
                            $scope.car2.instrumentPanelGarnish = !1
                        }
                    }
                    if (key.key_feature_name_id == 29) {
                        $scope.car2.silverInsideDoorHandle = key
                    }
                    if (key.key_feature_name_id == 30) {
                        $scope.car2.seatBackFoldDown = key
                    }
                    if (key.key_feature_name_id == 31) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.rearACVents = !0
                        } else {
                            $scope.car2.rearACVents = !1
                        }
                    }
                    if (key.key_feature_name_id == 32) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.ecoLamps = !0
                        } else {
                            $scope.car2.ecoLamps = !1
                        }
                    }
                    if (key.key_feature_name_id == 39) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.acWithHeater = !0
                        } else {
                            $scope.car2.acWithHeater = !1
                        }
                    }
                    if (key.key_feature_name_id == 40) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.powerSteering = !0
                        } else {
                            $scope.car2.powerSteering = !1
                        }
                    }
                    if (key.key_feature_name_id == 41) {
                        $scope.car2.steeringType = key
                    }
                    if (key.key_feature_name_id == 42) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.electricalFoldingORVM = !0
                        } else {
                            $scope.car2.electricalFoldingORVM = !1
                        }
                    }
                    if (key.key_feature_name_id == 43) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.cLKE = !0
                        } else {
                            $scope.car2.cLKE = !1
                        }
                    }
                    if (key.key_feature_name_id == 44) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.powerWindow = !0
                        } else {
                            $scope.car2.powerWindow = !1
                        }
                    }
                    if (key.key_feature_name_id == 45) {
                        $scope.car2.rearParcelTray = key
                    }
                    if (key.key_feature_name_id == 46) {
                        $scope.car2.cargoLight = key
                    }
                    if (key.key_feature_name_id == 47) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.pushStart = !0
                        } else {
                            $scope.car2.pushStart = !1
                        }
                    }
                    if (key.key_feature_name_id == 48) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.rearACVents = !0
                        } else {
                            $scope.car2.rearACVents = !1
                        }
                    }
                    if (key.key_feature_name_id == 49) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.autoDoorLockBySpeed = !0
                        } else {
                            $scope.car2.autoDoorLockBySpeed = !1
                        }
                    }
                    if (key.key_feature_name_id == 50) {
                        $scope.car2.cupHolders = key
                    }
                    if (key.key_feature_name_id == 51) {
                        $scope.car2.dayNightIRVM = key
                    }
                    if (key.key_feature_name_id == 52) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.ignitionKeyReminder = !0
                        } else {
                            $scope.car2.ignitionKeyReminder = !1
                        }
                    }
                    if (key.key_feature_name_id == 53) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.driverSeatHeightAdjustment = !0
                        } else {
                            $scope.car2.driverSeatHeightAdjustment = !1
                        }
                    }
                    if (key.key_feature_name_id == 54) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.rearSeatCenterArmrest = !0
                        } else {
                            $scope.car2.rearSeatCenterArmrest = !1
                        }
                    }
                    if (key.key_feature_name_id == 55) {
                        $scope.car2.hornType = key
                    }
                    if (key.key_feature_name_id == 56) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.aceBody = !0
                        } else {
                            $scope.car2.aceBody = !1
                        }
                    }
                    if (key.key_feature_name_id == 57) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.airBags = !0
                        } else {
                            $scope.car2.airBags = !1
                        }
                    }
                    if (key.key_feature_name_id == 58) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.aBSwithEBD = !0
                        } else {
                            $scope.car2.aBSwithEBD = !1
                        }
                    }
                    if (key.key_feature_name_id == 59) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.immobilizer = !0
                        } else {
                            $scope.car2.immobilizer = !1
                        }
                    }
                    if (key.key_feature_name_id == 60) {
                        $scope.car2.vehStabilityAssist = key
                    }
                    if (key.key_feature_name_id == 61) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.dualHorn = !0
                        } else {
                            $scope.car2.dualHorn = !1
                        }
                    }
                    if (key.key_feature_name_id == 62) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.securityAlarm = !0
                        } else {
                            $scope.car2.securityAlarm = !1
                        }
                    }
                    if (key.key_feature_name_id == 63) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.driverSeatBeltReminder = !0
                        } else {
                            $scope.car2.driverSeatBeltReminder = !1
                        }
                    }
                    if (key.key_feature_name_id == 64) {
                        $scope.car2.highMountStopLamp = key
                    }
                    if (key.key_feature_name_id == 33) {
                        $scope.car2.musicSystem = key
                    }
                    if (key.key_feature_name_id == 34) {
                        $scope.car2.bluetoothAudio = key
                    }
                    if (key.key_feature_name_id == 35) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.steeringMounted = !0
                        } else {
                            $scope.car2.steeringMounted = !1
                        }
                    }
                    if (key.key_feature_name_id == 36) {
                        $scope.car2.touchScreen = key
                    }
                    if (key.key_feature_name_id == 37) {
                        $scope.car2.speakers = key
                    }
                    if (key.key_feature_name_id == 38) {
                        if (key.description.toLowerCase() == "yes") {
                            $scope.car2.auxIn = !0
                        } else {
                            $scope.car2.auxIn = !1
                        }
                    }
                } catch (e) {}
            });
            console.log("$scope.car1",$scope.car1,"**", $scope.car2);
            if (tempArray.length == 3) {
                $scope.car3 = tempArray[2];
                $scope.has3Cars = 1;
                angular.forEach($scope.car3.techinical_specification, function(key, value) {
                    try {
                        if (key.specification_name_id == 21) {
                            $scope.car3.fuelCapacity = key
                        }
                        if (key.specification_name_id == 8) {
                            $scope.car3.displacement = key
                        }
                        if (key.specification_name_id == 12) {
                            $scope.car3.fuelEfficiency = key
                        }
                        if (key.specification_name_id == 15) {
                            $scope.car3.overallLength = key
                        }
                        if (key.specification_name_id == 16) {
                            $scope.car3.overallWidth = key
                        }
                        if (key.specification_name_id == 17) {
                            $scope.car3.overallHeight = key
                        }
                        if (key.specification_name_id == 18) {
                            $scope.car3.wheelbase = key
                        }
                        if (key.specification_name_id == 19) {
                            $scope.car3.kerbWeight = key
                        }
                        if (key.specification_name_id == 20) {
                            $scope.car3.tyreSize = key
                        }
                        if (key.specification_name_id == 23) {
                            $scope.car3.steeringSystem = key
                        }
                        if (key.specification_name_id == 8) {
                            $scope.car3.displacement = key
                        }
                        if (key.specification_name_id == 9) {
                            $scope.car3.valves = key
                        }
                        if (key.specification_name_id == 10) {
                            $scope.car3.power = key
                        }
                        if (key.specification_name_id == 11) {
                            $scope.car3.torque = key
                        }
                        if (key.specification_name_id == 13) {
                            $scope.car3.transAutomatic = key
                        }
                        if (key.specification_name_id == 14) {
                            $scope.car3.transManual = key
                        }
                        if (key.specification_name_id == 24) {
                            $scope.car3.turningRadius = key
                        }
                        if (key.specification_name_id == 25) {
                            $scope.car3.suspFront = key
                        }
                        if (key.specification_name_id == 26) {
                            $scope.car3.suspRear = key
                        }
                        if (key.specification_name_id == 29) {
                            $scope.car3.brakesFront = key
                        }
                        if (key.specification_name_id == 30) {
                            $scope.car3.brakesRear = key
                        }
                    } catch (e) {}
                });
                angular.forEach($scope.car3.variant_data.variant_feature_showcases, function(key, value) {
                    try {
                        if (key.key_feature_name_id == 8) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.alloyWheels = !0
                            } else {
                                $scope.car3.alloyWheels = !1
                            }
                        }
                        if (key.key_feature_name_id == 9) {
                            $scope.car3.fullTrimWheel = key
                        }
                        if (key.key_feature_name_id == 10) {
                            $scope.car3.frontChromeGrill = key
                        }
                        if (key.key_feature_name_id == 11) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.turnIndictorOnORVM = !0
                            } else {
                                $scope.car3.turnIndictorOnORVM = !1
                            }
                        }
                        if (key.key_feature_name_id == 12) {
                            $scope.car3.antenna = key
                        }
                        if (key.key_feature_name_id == 13) {
                            $scope.car3.bodyColorFrontBack = key
                        }
                        if (key.key_feature_name_id == 14) {
                            $scope.car3.outdoorHandles = key
                        }
                        if (key.key_feature_name_id == 15) {
                            $scope.car3.oRVMColor = key
                        }
                        if (key.key_feature_name_id == 16) {
                            $scope.car3.frontRearMudguards = key
                        }
                        if (key.key_feature_name_id == 17) {
                            $scope.car3.headLamp = key
                        }
                        if (key.key_feature_name_id == 18) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.fogLamps = !0
                            } else {
                                $scope.car3.fogLamps = !1
                            }
                        }
                        if (key.key_feature_name_id == 19) {
                            $scope.car3.rearWiperWasher = key
                        }
                        if (key.key_feature_name_id == 20) {
                            $scope.car3.upholstery = key
                        }
                        if (key.key_feature_name_id == 21) {
                            $scope.car3.leatherWrappedSteeringWheel = key
                        }
                        if (key.key_feature_name_id == 22) {
                            $scope.car3.leatherGearShiftKnob = key
                        }
                        if (key.key_feature_name_id == 23) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.multiInformationDisplay = !0
                            } else {
                                $scope.car3.multiInformationDisplay = !1
                            }
                        }
                        if (key.key_feature_name_id == 24) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.fuelConsumptionDisplay = !0
                            } else {
                                $scope.car3.fuelConsumptionDisplay = !1
                            }
                        }
                        if (key.key_feature_name_id == 25) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.cruisingRangeDisplay = !0
                            } else {
                                $scope.car3.cruisingRangeDisplay = !1
                            }
                        }
                        if (key.key_feature_name_id == 26) {
                            $scope.car3.tripMeter = key
                        }
                        if (key.key_feature_name_id == 27) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.outsideTempDisplay = !0
                            } else {
                                $scope.car3.outsideTempDisplay = !1
                            }
                        }
                        if (key.key_feature_name_id == 28) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.instrumentPanelGarnish = !0
                            } else {
                                $scope.car3.instrumentPanelGarnish = !1
                            }
                        }
                        if (key.key_feature_name_id == 29) {
                            $scope.car3.silverInsideDoorHandle = key
                        }
                        if (key.key_feature_name_id == 30) {
                            $scope.car3.seatBackFoldDown = key
                        }
                        if (key.key_feature_name_id == 31) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.rearACVents = !0
                            } else {
                                $scope.car3.rearACVents = !1
                            }
                        }
                        if (key.key_feature_name_id == 32) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.ecoLamps = !0
                            } else {
                                $scope.car3.ecoLamps = !1
                            }
                        }
                        if (key.key_feature_name_id == 39) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.acWithHeater = !0
                            } else {
                                $scope.car3.acWithHeater = !1
                            }
                        }
                        if (key.key_feature_name_id == 40) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.powerSteering = !0
                            } else {
                                $scope.car3.powerSteering = !1
                            }
                        }
                        if (key.key_feature_name_id == 41) {
                            $scope.car3.steeringType = key
                        }
                        if (key.key_feature_name_id == 42) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.electricalFoldingORVM = !0
                            } else {
                                $scope.car3.electricalFoldingORVM = !1
                            }
                        }
                        if (key.key_feature_name_id == 43) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.cLKE = !0
                            } else {
                                $scope.car3.cLKE = !1
                            }
                        }
                        if (key.key_feature_name_id == 44) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.powerWindow = !0
                            } else {
                                $scope.car3.powerWindow = !1
                            }
                        }
                        if (key.key_feature_name_id == 45) {
                            $scope.car3.rearParcelTray = key
                        }
                        if (key.key_feature_name_id == 46) {
                            $scope.car3.cargoLight = key
                        }
                        if (key.key_feature_name_id == 47) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.pushStart = !0
                            } else {
                                $scope.car3.pushStart = !1
                            }
                        }
                        if (key.key_feature_name_id == 48) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.rearACVents = !0
                            } else {
                                $scope.car3.rearACVents = !1
                            }
                        }
                        if (key.key_feature_name_id == 49) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.autoDoorLockBySpeed = !0
                            } else {
                                $scope.car3.autoDoorLockBySpeed = !1
                            }
                        }
                        if (key.key_feature_name_id == 50) {
                            $scope.car3.cupHolders = key
                        }
                        if (key.key_feature_name_id == 51) {
                            $scope.car3.dayNightIRVM = key
                        }
                        if (key.key_feature_name_id == 52) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.ignitionKeyReminder = !0
                            } else {
                                $scope.car3.ignitionKeyReminder = !1
                            }
                        }
                        if (key.key_feature_name_id == 53) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.driverSeatHeightAdjustment = !0
                            } else {
                                $scope.car3.driverSeatHeightAdjustment = !1
                            }
                        }
                        if (key.key_feature_name_id == 54) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.rearSeatCenterArmrest = !0
                            } else {
                                $scope.car3.rearSeatCenterArmrest = !1
                            }
                        }
                        if (key.key_feature_name_id == 55) {
                            $scope.car3.hornType = key
                        }
                        if (key.key_feature_name_id == 56) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.aceBody = !0
                            } else {
                                $scope.car3.aceBody = !1
                            }
                        }
                        if (key.key_feature_name_id == 57) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.airBags = !0
                            } else {
                                $scope.car3.airBags = !1
                            }
                        }
                        if (key.key_feature_name_id == 58) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.aBSwithEBD = !0
                            } else {
                                $scope.car3.aBSwithEBD = !1
                            }
                        }
                        if (key.key_feature_name_id == 59) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.immobilizer = !0
                            } else {
                                $scope.car3.immobilizer = !1
                            }
                        }
                        if (key.key_feature_name_id == 60) {
                            $scope.car3.vehStabilityAssist = key
                        }
                        if (key.key_feature_name_id == 61) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.dualHorn = !0
                            } else {
                                $scope.car3.dualHorn = !1
                            }
                        }
                        if (key.key_feature_name_id == 62) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.securityAlarm = !0
                            } else {
                                $scope.car3.securityAlarm = !1
                            }
                        }
                        if (key.key_feature_name_id == 63) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.driverSeatBeltReminder = !0
                            } else {
                                $scope.car3.driverSeatBeltReminder = !1
                            }
                        }
                        if (key.key_feature_name_id == 64) {
                            $scope.car3.highMountStopLamp = key
                        }
                        if (key.key_feature_name_id == 33) {
                            $scope.car3.musicSystem = key
                        }
                        if (key.key_feature_name_id == 34) {
                            $scope.car3.bluetoothAudio = key
                        }
                        if (key.key_feature_name_id == 35) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.steeringMounted = !0
                            } else {
                                $scope.car3.steeringMounted = !1
                            }
                        }
                        if (key.key_feature_name_id == 36) {
                            $scope.car3.touchScreen = key
                        }
                        if (key.key_feature_name_id == 37) {
                            $scope.car3.speakers = key
                        }
                        if (key.key_feature_name_id == 38) {
                            if (key.description.toLowerCase() == "yes") {
                                $scope.car3.auxIn = !0
                            } else {
                                $scope.car3.auxIn = !1
                            }
                        }
                    } catch (e) {}
                })
            }
        }, function(error) {})["finally"](function() {
            pageDataLoaded()
        })
    };
    $rootScope.$on("getData", function(event, query) {
        $rootScope.query = query;
        $scope.getData($rootScope.query)
    });
    if ($rootScope.query) {
        $scope.getData($rootScope.query);
        $localStorage.query = $rootScope.query
    }
    if ($localStorage.query) {
        $scope.getData($localStorage.query)
    }
    $scope.changeVariant = function(car) {
        removeHighlight();
        highLightCommonisChecked = !1;
        highLightNonCommonisChecked = !1;
        $scope.common = !1;
        $scope.differences = !1;
        var varid = "";
        var temp = [];
        $scope.tempVar = {};
        varid = document.getElementById("car" + car + "Select").value;
        if (varid != "" && car) {
            Array.prototype.remove = function(index, data) {
                try {
                    this.splice(index, 1, data)
                } catch (e) {}
            };
            temp = $localStorage.query;
            $scope.tempVar = temp[car - 1];
            $scope.tempVar.variant_id = varid;
            temp.remove(car - 1, $scope.tempVar);
            $scope.query = temp;
            $rootScope.$broadcast("getData", $scope.query)
        }
    };
    $scope.closeCar = function(car, ev) {
        var temp = [];
        removeHighlight();
        highLightCommonisChecked = !1;
        highLightNonCommonisChecked = !1;
        $scope.common = !1;
        $scope.differences = !1;
        Array.prototype.remove = function(index, data) {
            try {
                this.splice(index, 1, data)
            } catch (e) {}
        };
        temp = $localStorage.query;
        if (car == 1) {
            if ($scope.has3Cars == 0) {
                $scope.changeCar(ev, 1, 1)
            } else if ($scope.has3Cars == 1) {
                temp.remove(0);
                for (var i = temp.length - 1; i >= 0; i--) {
                    if (temp[i] == null || temp[i] == undefined) {
                        var index = temp.indexOf(temp[i]);
                        if (index > -1) {
                            temp.splice(index, 1)
                        }
                    }
                }
                $scope.car1 = $scope.car2;
                $scope.car2 = {};
                $scope.car2 = $scope.car3;
                $scope.car3 = {};
                $scope.has3Cars = 0
            } else {}
        }
        if (car == 2) {
            if ($scope.has3Cars == 0) {
                $scope.changeCar(ev, 2, 1)
            } else if ($scope.has3Cars == 1) {
                temp.remove(1);
                for (var i = temp.length - 1; i >= 0; i--) {
                    if (temp[i] == null || temp[i] == undefined) {
                        var index = temp.indexOf(temp[i]);
                        if (index > -1) {
                            temp.splice(index, 1)
                        }
                    }
                }
                $scope.car2 = {};
                $scope.car2 = $scope.car3;
                $scope.car3 = {};
                $scope.has3Cars = 0
            } else {}
        }
        if (car == 3) {
            temp.pop(2);
            for (var i = temp.length - 1; i >= 0; i--) {
                if (temp[i] == null || temp[i] == undefined) {
                    var index = temp.indexOf(temp[i]);
                    if (index > -1) {
                        temp.splice(index, 1)
                    }
                }
            }
            $scope.car3 = {};
            $scope.has3Cars = 0
        }
        $scope.query = temp;
        $rootScope.$broadcast("getData", $scope.query)
    };
    $scope.changeCar = function(ev, car, changeCar) {
        removeHighlight();
        highLightCommonisChecked = !1;
        highLightNonCommonisChecked = !1;
        $scope.common = !1;
        $scope.differences = !1;
        $rootScope.carChangeNumber = car;
        $rootScope.changeCar = changeCar;
        if (car) {
            $mdDialog.show({
                controller: changeCarController,
                templateUrl: "views/changeCar.tmpl.html",
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: !0,
                fullscreen: $scope.customFullscreen
            }).then(function(answer) {}, function() {})
        }
    };
 
    function changeCarController($scope, $mdDialog, $sessionStorage) {
        $scope.test = !0;
        $scope.message = "Loading Cars...";
        $http.get(api_url + "/website/cars").then(function(success) {
            $scope.carList = success.data.cars;
            
        }, function(error) {})["finally"](function() {
            $scope.test = !1
        });
        $scope.modelChange = function() {
            $scope.dataToSend = {};
            if ($scope.model) {
                $scope.dataToSend.id = $scope.model;
                $scope.dataToSend.city = "Bengaluru";
                $http.post(api_url + "/website/car_with_location", $scope.dataToSend).then(function(success) {
                    $scope.car = success.data.car;
                    $scope.variantsToShow = $scope.car.variants
                }, function(error) {})["finally"](function() {
                    $scope.test = !1
                })
            }
        };
        $scope.closeDialogue = function() {
            $mdDialog.hide()
        };
        $scope.variantChange = function() {
            $scope.tempVar = {};
            var temp = [];
            if ($rootScope.changeCar) {
                if ($scope.model && $scope.variant) {
                    Array.prototype.remove = function(index, data) {
                        try {
                            this.splice(index, 1, data)
                        } catch (e) {}
                    };
                    temp = $localStorage.query;
                    $scope.tempVar.car_id = $scope.model;
                    $scope.tempVar.variant_id = $scope.variant;
                    temp.remove($rootScope.carChangeNumber - 1, $scope.tempVar);
                    $scope.query = temp;
                    $rootScope.$broadcast("getData", $scope.query)
                }
            } else {
                if ($scope.model && $scope.variant) {
                    temp = $localStorage.query;
                    $scope.tempVar.car_id = $scope.model;
                    $scope.tempVar.variant_id = $scope.variant;
                    temp.push($scope.tempVar);
                    $scope.query = temp;
                    $rootScope.$broadcast("getData", $scope.query)
                }
            }
            $mdDialog.hide()
        }
    }
    $scope.getPriceQuote = function(ev, variantsToShow) {
        $rootScope.variantsToShow = variantsToShow;
        $mdDialog.show({
            controller: mailVehiclePrice,
            templateUrl: "views/priceRequest1.tmpl.html",
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: !0,
            fullscreen: $scope.customFullscreen
        }).then(function(answer) {}, function() {})
    };
    $rootScope.$on("afterEmailPrice", function(event, email) {
        var msg = "Price quote sent to " + email + "!";
        setTimeout(function() {
            callnotify(msg, "success")
        }, 1e3)
    });

    function mailVehiclePrice($scope, $mdDialog, $sessionStorage, growl) {
        $scope.sendEmailData = {};
        $scope.variantsToShow = $rootScope.variantsToShow;
        console.log(JSON.stringify($scope.variantsToShow));
        $scope.sendEmail = function() {
            if ($sessionStorage.DHuserSession) {
                $scope.sendEmailData.user_id = $sessionStorage.DHuserSession.id
            } else {
                $scope.sendEmailData.user_id = ""
            }
            $http.post(api_url + "/mobile/email_price", $scope.sendEmailData).then(function(success) {
                $rootScope.$emit("afterEmailPrice", $scope.sendEmailData.email)
            }, function(error) {
                console.log(JSON.stringify(error))
            })["finally"](function() {
                $mdDialog.cancel({})
            })
        };
        $scope.closePriceModal = function() {
            $mdDialog.cancel({})
        }
    }
}).service("anchorSmoothScroll", function() {
    this.scrollTo = function(eID) {
        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY) leapY = stopY;
                timer++
            }
            return
        }
        for (var i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY) leapY = stopY;
            timer++
        }

        function currentYPosition() {
            if (self.pageYOffset) return self.pageYOffset;
            if (document.documentElement && document.documentElement.scrollTop) return document.documentElement.scrollTop;
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0
        }

        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop
            }
            return y
        }
    }
});