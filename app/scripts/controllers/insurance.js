'use strict';
angular.module('dakshinHondaWebsiteApp').controller('insuranceCtrl', function($scope, $location, $http, growl, $rootScope, localStorageService, $sessionStorage) {
    window.scrollTo(0, 0);
    $scope.hideLogin = !1;
    $scope.testDrivebooking = function() {
        $location.path('/testDrive')
    }
    $scope.servicebooking = function() {
        $location.path('/servicebooking')
    }
    $scope.insurancebooking = function() {
        $location.path('/insurance')
    }
    $scope.emicalculatorServices = function() {
        $location.path('/emicalculator')
    }
    $scope.onRoadPriceQuoteServices = function() {
        $location.path('/onRoadPriceQuote')
    }
    $scope.feedback = function() {
        $location.path('/feedback')
    }
    $scope.contactUsServices = function() {
        $location.path('/contactUs')
    }
    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat
    }
    var dat = new Date();
    $scope.minDate = dat.toDateString();
    $scope.changeDate = function(date) {
        var x = date.split(',', 2);
        $scope.insData.date_of_purchase = x[0]
    }
    $scope.changeInsDate = function(date) {
        var x = date.split(',', 2);
        $scope.insNewData.date_of_purchase = x[0]
    }
    $scope.user = {};
    $scope.carsList = {};
    $scope.sendData = {};
    $scope.sendData.book_test_drife = {};
    var insuranceCarModel = [];
    var insuranceData = {};
    var insuranceModelAllData = {};
    $scope.insuranceIndexData = [];
    $scope.insData = {};
    $scope.insNewData = {};
    $scope.insurance = {};
    $scope.insuranceBanner = {
        "category": "Insurence Renewal Booking"
    };
    $scope.getCarModel = function() {
        $scope.dataToSend = {};
        if ($sessionStorage.DHuserSession) {
            $scope.dataToSend.user_id = $sessionStorage.DHuserSession.id;
            $http.post(api_url + "/website/user_cars?auth_token=" + $sessionStorage.DHuserSession.authentication_token, $scope.dataToSend).then(function(success) {
                $scope.mycars = success.data.my_cars;
                insuranceCarModel = [];
                angular.forEach($scope.mycars, function(key, value) {
                    console.log(JSON.stringify(key));
                    insuranceCarModel.push({
                        'model': key.model,
                        'registration': key.registration_number,
                        'insuranceCompany': key.insurance_provider,
                        'policy': key.insurance_number,
                        'kms': key.number_of_kms,
                        'expiry_date': key.insurance_expiry,
                    })
                });
                if ($scope.mycars.length == 0) {
                    callnotify("Please add car(s), in your profile, to continue with the booking!!", 'warning')
                }
                if ($scope.mycars.length >= 0) {
                    $scope.showInsDropdown = !0
                }
                if ($scope.mycars.length == 1) {
                    $scope.firstValue = $scope.mycars[0];
                    setTimeout(function() {
                        var reg = document.getElementById('regNumber1')
                        reg.value = $scope.mycars[0].registration_number;
                        var kms = document.getElementById('userKms')
                        kms.value = $scope.mycars[0].number_of_kms;
                        var date = document.getElementById('date')
                        date.value = $scope.mycars[0].date_of_purchase;
                        var userInsCompany = document.getElementById('userInsCompany')
                        userInsCompany.value = $scope.mycars[0].insurance_provider;
                        var userPolicy = document.getElementById('userPolicy')
                        userPolicy.value = $scope.mycars[0].policy_number;
                        var exDate = document.getElementById('exDate')
                        exDate.value = $scope.mycars[0].insurance_expiry
                    }, 200)
                }
                $scope.modalServiceBookingData($scope.modelData)
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {})
        }
    }
    $rootScope.$on('onLogin', function(event, data) {
        $scope.getCarModel()
    });
    $scope.modalInsuranceRenewData = function(model) {
        $scope.insData = model;
        setTimeout(function() {
            var reg = document.getElementById('regNumber1')
            reg.value = model.registration_number;
            var kms = document.getElementById('userKms')
            kms.value = model.number_of_kms;
            var date = document.getElementById('date')
            date.value = model.date_of_purchase;
            var userInsCompany = document.getElementById('userInsCompany')
            userInsCompany.value = model.insurance_provider;
            var userPolicy = document.getElementById('userPolicy')
            userPolicy.value = model.policy_number;
            var exDate = document.getElementById('exDate')
            exDate.value = model.insurance_expiry
        }, 200)
    }
    $scope.insuranceAllPage = {};

    function updateInsuranceData() {
        $scope.userInsuranceData = $sessionStorage.DHuserSession;
        $scope.insuranceAllPage.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.insuranceAllPage.email = $sessionStorage.DHuserSession.profile.email;
        $scope.insuranceAllPage.address = $sessionStorage.DHuserSession.profile.address;
        $scope.insuranceAllPage.user_id = $sessionStorage.DHuserSession.profile.id;
        $scope.insuranceAllPage.mobile = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $scope.insurance.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.insurance.email = $sessionStorage.DHuserSession.profile.email;
        $scope.insurance.address = $sessionStorage.DHuserSession.profile.address;
        $scope.insurance.user_id = $sessionStorage.DHuserSession.profile.id;
        $scope.insurance.mobile = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $scope.hideLogin = !0;
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token
    }
    if ($sessionStorage.DHuserSession) {
        updateInsuranceData()
    }
    $rootScope.$on('afterLogin', function(event, data) {
        updateInsuranceData()
    });
    $scope.newInsurance = function() {
        console.log("$scope.insurance",$scope.insurance);
        $scope.dataToSend = {};
        if ($sessionStorage.DHuserSession) {
            $scope.insuranceAllPage.name = $scope.insurance.name;
            $scope.insuranceAllPage.email = $scope.insurance.email;
            $scope.insuranceAllPage.address = $scope.insuranceAllPage.address;
            $scope.insuranceAllPage.user_id = $scope.insurance.user_id;
            $scope.insuranceAllPage.mobile = $scope.insuranceAllPage.mobile;
            console.log("   $scope.insuranceAllPage",JSON.stringify($scope.insuranceAllPage));
            
        } 
        if ($scope.insNewData.model) {
            $scope.insuranceAllPage.model = $scope.insNewData.model
        } else {
            console.log("else",$scope.firstValue);
            $scope.insuranceAllPage.model = $scope.firstValue.model;
        }
        

        var reg = document.getElementById('regNumber1').value
        
        var kms = document.getElementById('userKms').value
        
        var date = document.getElementById('date').value
     
        var userInsCompany = document.getElementById('userInsCompany').value
      
        var userPolicy = document.getElementById('userPolicy').value
       
        var exDate = document.getElementById('exDate').value
        
        console.log("reg\t",reg,"\n kms \t",kms,"\n date \t",date,"\n userInsCompany \t ", userInsCompany,"\n userPolicy \t ",userPolicy,"\n exDate \t",exDate);
        $scope.insuranceAllPage.registration_number = reg;
        $scope.insuranceAllPage.kms = kms;
        $scope.insuranceAllPage.purchase_date = date;
        $scope.insuranceAllPage.insurance_company = userInsCompany;
        $scope.insuranceAllPage.expiry_date = exDate;
        $scope.insuranceAllPage.policy_number = userPolicy; 
        // if($scope.userForm.valRenewReg)
    if($scope.insuranceAllPage.name && $scope.insuranceAllPage.email && $scope.insuranceAllPage.mobile && $scope.insuranceAllPage.model && $scope.insuranceAllPage.registration_number && $scope.insuranceAllPage.kms && $scope.insuranceAllPage.expiry_date){
        
        $scope.userForm.$valid=true;
        console.log("if",$scope.userForm);
    } else{
        
        $scope.userForm.$valid=false;
        console.log("else",$scope.userForm);
    }
        console.log("   $scope.userForm", ($scope.userForm));
        if ($scope.userForm.$valid) {
            $scope.dataToSend.insurance_renewal = $scope.insuranceAllPage;
            console.log("valid.......................");
//             var myLink = document.getElementById('insurance');
// var google_conversion_id = 825753188;
// var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
// var google_remarketing_only = false;
// myLink.onclick = function(){

//     var script = document.createElement("script");
//     script.type = "text/javascript";
//     script.src = "//www.googleadservices.com/pagead/conversion.js"; 
//     document.getElementsByTagName("head")[0].appendChild(script);
//     return false;

// }

            callnotify('Requesting insurance renewal, please wait...', 'information');
            $http.post(api_url + "/website/insurance_renewals", $scope.dataToSend).then(function(success) {
                callnotify('Insurance requested!', 'success');
                $location.path('/main')
                // console.log(success);
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {})
        } else {
            callnotify('Form Incomplete!!', 'error')
        }
    }
})