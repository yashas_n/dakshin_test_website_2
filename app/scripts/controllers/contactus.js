'use strict';
angular.module('dakshinHondaWebsiteApp').controller('ContactusCtrl', function($scope, $location, $http, growl, $sessionStorage, $rootScope, localStorageService) {
    window.scrollTo(0, 0);
    $scope.hideLogin = !1;
    $scope.testDrivebooking = function() {
        $location.path('/testDrive')
    }
    $scope.servicebooking = function() {
        $location.path('/servicebooking')
    }
    $scope.insurancebooking = function() {
        $location.path('/insurance')
    }
    $scope.emicalculatorServices = function() {
        $location.path('/emicalculator')
    }
    $scope.onRoadPriceQuoteServices = function() {
        $location.path('/onRoadPriceQuote')
    }
    $scope.feedback = function() {
        $location.path('/feedback')
    }
    $scope.contactUsServices = function() {
        $location.path('/contactUs')
    }
    $scope.contactus = {
        "category": "ContactUs"
    };
    $scope.maplocation = {};
    $scope.changedealers = function() {
        var formtype = $scope.contactFormData.category;
        if (formtype == "Sales" || formtype == "Service") {
            $scope.formtypefilter = {
                "city": 'Bengaluru',
                "type": formtype
            };
            console.log(JSON.stringify($scope.formtypefilter));
            $http.post(api_url + "/mobile/dealer_type_location", $scope.formtypefilter).then(function(success) {
                console.log(JSON.stringify(success));
                $scope.dealerList = success.data.dealers
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {})
        } else if (formtype == "Others") {
            $scope.maplocation = {
                "city": 'Bengaluru'
            };
            console.log(JSON.stringify($scope.maplocation));
            $http.post(api_url + "/mobile/dealer_location", $scope.maplocation).then(function(success) {
                console.log(JSON.stringify(success));
                $scope.dealerList = success.data.dealers
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {})
        }
    };
    $scope.contactFormData = {};

    function updateContactData() {
        $scope.contactFormData.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.contactFormData.email = $sessionStorage.DHuserSession.profile.email;
        $scope.contactFormData.phone = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $scope.contactFormData.address = $sessionStorage.DHuserSession.profile.address;
        $scope.hideLogin = !0;
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token
    }
    if ($sessionStorage.DHuserSession) {
        updateContactData()
    }
    $rootScope.$on('afterLogin', function(event, data) {
        updateContactData()
    });
    $scope.contactUs = function() {
        $scope.dataToSend = {};
        if ($scope.userForm.$valid) {
//             var myLink = document.getElementById('contactus');
// var google_conversion_id = 825753188;
// var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
// var google_remarketing_only = false;
// myLink.onclick = function(){

//     var script = document.createElement("script");
//     script.type = "text/javascript";
//     script.src = "//www.googleadservices.com/pagead/conversion.js"; 
//     document.getElementsByTagName("head")[0].appendChild(script);
//     return false;

// }
            $scope.dataToSend.enquiry = $scope.contactFormData;
            callnotify('Submitting enquiry, please wait...', 'information');
            $http.post(api_url + "/website/enquiries", $scope.dataToSend).then(function(success) {
                callnotify('Enquiry Submitted!', 'success');
                $location.path('/main')
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {
                $scope.test = !1
            })
        } else {
            callnotify('Form Incomplete!!', 'error')
        }
    }
})