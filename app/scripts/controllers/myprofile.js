'use strict';
angular.module('dakshinHondaWebsiteApp').controller('MyprofileCtrl', function($scope, $sessionStorage, $http, $location, $filter, $localStorage) {
    window.scrollTo(0, 0);
    $scope.sendData = {};
    $scope.sendData.user = {};
    if ($sessionStorage.DHuserSession) {
        console.log(JSON.stringify($sessionStorage.DHuserSession));
        $scope.sendData = $sessionStorage.DHuserSession.profile;
        if ($localStorage.isSocial == 1) {
            if ($localStorage.provider == "google") {
                var img = $localStorage.socialImage;
                var res = img.split("?");
                $scope.displayUserImage = res[0];
                $localStorage.socialImage = res[0]
            } else {
                $scope.displayUserImage = $localStorage.socialImage
            }
        } else if ($sessionStorage.DHuserSession.profile.profile_image.url != null) {
            $scope.displayUserImage = api_url + $scope.userDetails.profile_image.url + "?" + $scope.sendData.user.updated_at
        } else {
            $scope.displayUserImage = "images/profile.png"
        }
    } else {
        $location.path('/')
    }
    $scope.goMyAccount = function() {
        $location.path('/myaccount')
    }
})