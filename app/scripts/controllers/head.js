angular.module('dakshinHondaWebsiteApp').controller('headCtrl', function($scope, $route, $location, $http, growl, $mdDialog, $sessionStorage, $rootScope, $timeout, localStorageService, $window, $localStorage) {
    $scope.user = {};
    $scope.displayUserName = "";
    $scope.newuser = {};
    $scope.displayUserImage = {};
    $scope.DHuserData = {};
    $scope.DHuserData.user = {};
    $scope.DHuserData.user.updated_at = {};
    $scope.paymentcount = {};
    $scope.totalcount = !1;
    if ($sessionStorage.DHuserSession) {
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token;
        console.log(JSON.stringify($sessionStorage.DHuserSession));
        $scope.test = !0;
        $scope.message = 'Loading...';
        $http.post(api_url + "/website/get_payment_dues?auth_token=" + $sessionStorage.DHuserSession.authentication_token, JSON.stringify({
            "user_id": $sessionStorage.DHuserSession.id
        })).then(function(success) {
            console.log(success.data);
            $scope.totalcount = !0;
            $scope.paymentcount = success.data.dealer_bank_accounts.length;
            console.log($scope.paymentcount)
        }, function(error) {
            $scope.test = !1;
            console.log(error)
        })
    }
    $scope.goToCars = function(option) {
        localStorage.setItem("carOption", option)
    }
    $scope.paymentdue = function(count) {
        console.log(count);
        if (count > 0) {
            $location.path('/paymentdue')
        } else {
            growl.info("No payment due !", {
                disableCountDown: !0
            })
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email)
    }
    var name = "";
    var nameArray = [];
    if ($sessionStorage.DHuserSession) {
        $scope.DHuserData = $sessionStorage.DHuserSession;
        name = $scope.DHuserData.profile.full_name;
        nameArray = name.split(" ");
        $scope.displayUserName = nameArray[0];
        if ($localStorage.isSocial == 1) {
            $scope.isSocial = 1;
            if ($localStorage.provider == "google") {
                var img = $localStorage.socialImage;
                var res = img.split("?");
                $scope.displayUserImage = res[0];
                $localStorage.socialImage = res[0]
            } else {
                $scope.displayUserImage = $localStorage.socialImage
            }
        } else if ($scope.DHuserData.profile.profile_image.url == null && $localStorage.isSocial == 0) {
            $scope.isSocial = 0;
            $scope.displayUserImage = "images/profile.png"
        } else if ($scope.DHuserData.profile.profile_image.url != null) {
            $scope.isSocial = 0;
            $scope.displayUserImage = api_url + $scope.DHuserData.profile.profile_image.url + "?" + $sessionStorage.DHuserSession.updated_at
        } else {
            $scope.isSocial = 0;
            $scope.displayUserImage = "images/profile.png"
        }
    }
    $scope.goToHome = function() {
        $location.path('/');
        window.scrollTo(0, 0)
    }
    $rootScope.$on('afterLogin', function(event, data) {
        // setTimeout(function() {
        //     window.location.reload()
        // }, 2500);
        localStorageService.set('logout', '');
        $scope.DHuserData = data;
         console.log("$scope.sendData",$scope.sendData);
         console.log("$scope.DHuserData",$scope.DHuserData);
         $localStorage.profile_ID = $scope.DHuserData.profile.id;
         $localStorage.user_ID = $scope.DHuserData.profile.user_id;
        var name = $scope.DHuserData.profile.full_name;
        name = name.split(" ");
        $scope.displayUserName = name[0];
        if ($localStorage.isSocial == 1) {
            $scope.isSocial = 1;
            if ($localStorage.provider == "google") {
                var img = $localStorage.socialImage;
                var res = img.split("?");
                $scope.displayUserImage = res[0];
                $localStorage.socialImage = res[0]
            } else {
                $scope.displayUserImage = $localStorage.socialImage
            }
        } else if ($scope.DHuserData.profile.profile_image.url == null && $localStorage.isSocial == 0) {
            $scope.isSocial = 0;
            $scope.displayUserImage = "images/profile.png"
        } else if ($scope.DHuserData.profile.profile_image.url != null) {
            $scope.isSocial = 0;
            $scope.displayUserImage = api_url + $scope.DHuserData.profile.profile_image.url + "?" + $scope.DHuserData.profile.updated_at
        } else {
            $scope.isSocial = 0;
            $scope.displayUserImage = "images/profile.png"
        }
        localStorageService.set('loginUserFormData', $scope.DHuserData)
    });
    $rootScope.$on('event:social-sign-out-success', function(event, logoutStatus) {
        $sessionStorage.DHuserSession = "";
        $scope.displayUserName = ""
    })
    $scope.goTOAccessories = function(index, car) {
        $localStorage.selectedCarIndex = index;
        $localStorage.accessoriesCarName = car.car_name;
        $localStorage.accessoriesCarId = car.id;
        $location.path('/accessories');
        $rootScope.$emit('afterCarAccessory')
    }
    $scope.logout = function() {
        $sessionStorage.DHuserSession = "";
        $scope.displayUserName = "";
        localStorageService.set('loginUserFormData', '');
        localStorageService.set('logout', 'done');
        localStorageService.set('activeUserId', '')
        $localStorage.isSocial = 0;
        $localStorage.provider = "";
        $localStorage.socialImage = "";
        $scope.displayUserImage = "";
        $scope.userImage = "";
        $route.reload();
        $location.path('/');
        callnotify("Logging out.. Please wait!", 'success');
        setTimeout(function() {
            window.location.reload()
        }, 1500)
    }
    $scope.showLogin = function(ev) {
        setTimeout(function() {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'views/loginPopUp.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: !0,
                fullscreen: $scope.customFullscreen
            }).then(function(answer) {}, function() {})
        }, 200)
    }; 

    function DialogController($scope, $mdDialog, $sessionStorage) {
        $scope.user = {};
        $scope.closeModal = function() {
            $mdDialog.hide({})
        }
        $scope.signInForm = function() {
            $scope.dataToSend = {};
            var validEmail = validateEmail($scope.user.email);
            if (!validEmail) {
                callnotify('Inalid Email!', 'error');
                return
            } else {
                $scope.dataToSend.session = $scope.user;
                $http.post(api_url + "/website/sessions", $scope.dataToSend).then(function(success) {
                    if (success.status == 200) {
                        $sessionStorage.DHuserSession = success.data.user;
                        $scope.DHuserData = $sessionStorage.DHuserSession;
                        $rootScope.$emit('afterLogin', $scope.DHuserData);
                        setTimeout(function() {
                            callnotify("Logging in.. Please wait!", 'success')
                        }, 200);
                        $localStorage.isSocial = 0;
                        $mdDialog.hide()
                    } else {
                        callnotify('Error logging in!!', 'error')
                    }
                }, function(error) {
                    console.log(JSON.stringify(error));
                    callnotify(error.data.errors, 'error')
                })['finally'](function() {
                    $scope.test = !1
                })
            }
        }
        $scope.showForgotPasswordModal = function(ev) {
            $mdDialog.hide();
            setTimeout(function() {
                $mdDialog.show({
                    controller: DialogControllerForgotPassword,
                    templateUrl: 'views/forgotpassword.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: !0,
                    fullscreen: $scope.customFullscreen
                }).then(function(answer) {}, function() {})
            }, 600)
        }

        function DialogControllerForgotPassword($scope, $mdDialog) {
            $scope.data = {};
            $scope.sendResetLink = function() {
                $http.post(api_url + "/mobile/passwords", $scope.data).then(function(success) {
                    callnotify("Please check your email for further instructions.", 'success')
                }, function(error) {
                    callnotify('Invalid credentials!!', 'error')
                })['finally'](function() {
                    $scope.test = !1;
                    $mdDialog.hide()
                })
            };
            $scope.closeModal = function() {
                $mdDialog.hide({})
            }
        }
        $rootScope.$on('event:social-sign-in-success', function(event, userDetails) {
            $scope.user = {};
            $scope.dataToSend = {};
            $scope.user.name = userDetails.name;
            $scope.user.email = userDetails.email;
            $scope.user.social_login = 1;
            $scope.user.provider = userDetails.provider;
            $rootScope.userImage = userDetails.imageUrl;
            $localStorage.isSocial = 1;
            $localStorage.provider = userDetails.provider;
            $localStorage.socialImage = userDetails.imageUrl;
            $scope.dataToSend.user = $scope.user;
            $http.post(api_url + "/website/users", $scope.dataToSend).then(function(success) {
             console.log(success.data.user.profile.email,"*");
                $sessionStorage.DHuserSession = success.data.user;
                $scope.DHuserData = $sessionStorage.DHuserSession;
                $rootScope.$emit('afterLogin', $scope.DHuserData);
                
                if (!success.data.user.profile.email) {
                    $localStorage.profile_email = false;
                    setTimeout(function() {
                        $location.path('myaccount');
                    }, 1000);

                } else {
                    $localStorage.profile_email = true;

                }



                if (!success.data.user.profile.mobile) {
                    // $localStorage.profile_mobile=false;
                    setTimeout(function() {
                        $location.path('myaccount');
                    }, 1000);

                }
                setTimeout(function() {
                    callnotify("Logging in.. Please wait!", 'success')
                }, 200);
                $mdDialog.hide()
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {
                $scope.test = !1
            })
        })
    }
    $scope.showRegister = function(ev) {
        $mdDialog.show({
            controller: DialogControllerSignUp,
            templateUrl: 'views/signUp.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: !0,
            fullscreen: $scope.customFullscreen
        }).then(function(answer) {}, function() {})
    };

    function DialogControllerSignUp($scope, $mdDialog) {
        $scope.signUp = function() {
            $scope.dataToSend = {};
            var name = document.getElementById("signupName").value;
            var phNo = document.getElementById("signupPhone").value;
            var email = document.getElementById("signupEmail").value;
            var validEmail = validateEmail(email);
            var signupPassword = document.getElementById("signupPassword").value;
            var signupConfirmPassword = document.getElementById("signupConfirmPassword").value;
            if (name.length < 1) {
                callnotify('Invalid or Empty Name!', 'error');
                return
            } else if (phNo.length < 1) {
                callnotify('Invalid or Empty Phone Number!', 'error');
                return
            } else if (!validEmail) {
                callnotify('Inalid Email!', 'error');
                return
            } else if (signupPassword.length < 1) {
                callnotify('Password Required!', 'error');
                return
            } else if (signupConfirmPassword.length < 1) {
                callnotify('Confirm password Required!', 'error');
                return
            } else if (signupPassword != signupConfirmPassword) {
                callnotify('Passwords do not match!', 'error');
                return
            } else {
                $scope.newuser.social_login = 0;
                $scope.dataToSend.user = $scope.newuser;
                $http.post(api_url + "/website/users", $scope.dataToSend).then(function(success) {
                    if (success.status == 201) {
                        setTimeout(function() {
                            callnotify("Signing you in.. Please wait!", 'success')
                        }, 200);
                        $scope.dataToLogin = {};
                        $scope.session = {};
                        $scope.session.email = $scope.newuser.email;
                        $scope.session.password = $scope.newuser.password;
                        $scope.dataToLogin.session = $scope.session;
                        $http.post(api_url + "/website/sessions", $scope.dataToLogin).then(function(success) {
                            if (success.status == 200) {
                                $sessionStorage.DHuserSession = success.data.user;
                                $scope.DHuserData = $sessionStorage.DHuserSession;
                                $rootScope.$emit('afterLogin', $scope.DHuserData);
                                setTimeout(function() {
                                    callnotify("Logging in.. Please wait!", 'success')
                                }, 200);
                                $localStorage.isSocial = 0;
                                $mdDialog.hide()
                            } else {
                                callnotify('Error logging in!!', 'error')
                            }
                        }, function(error) {
                            console.log(JSON.stringify(error))
                        })['finally'](function() {
                            $scope.test = !1
                        })
                    } else {
                        console.log(JSON.stringify(success.errors))
                    }
                }, function(error) {
                    console.log(JSON.stringify(error))
                })['finally'](function() {
                    $scope.test = !1
                })
            }
        }
        $scope.closeModal = function() {
            $mdDialog.hide({})
        }
    }
})