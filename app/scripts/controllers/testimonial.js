'use strict';
angular.module('dakshinHondaWebsiteApp').controller('testimonialCtrl', function($scope, $location, $http, growl, $mdDialog, $sessionStorage, $rootScope, localStorageService, $timeout) {
    $scope.testimonialList = {};
    var tempTestimonialList = [];
    $scope.currentIndex = 0;
    $scope.setCurrentSlideIndex = function(index) {
        $scope.currentIndex = index
    };
    $scope.isCurrentSlideIndex = function(index) {
        return $scope.currentIndex === index
    };
    $scope.prevArrow = function() {
        $scope.currentIndex = ($scope.currentIndex < $scope.testimonialListFinal.length - 1) ? ++$scope.currentIndex : 0
    };
    $scope.nextArrow = function() {
        $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.testimonialListFinal.length - 1
    };
    $http.get(api_url + "/website/get_limit_testmonials").then(function(success) {
        $scope.testimonialList = success.data.testmonials;
        angular.forEach($scope.testimonialList, function(key, value) {
            tempTestimonialList.push({
                'testimonialDescription': key.description,
                'testimonialTitle': key.name,
                'image': api_url + key.image.url
            })
        });
        $scope.testimonialListFinal = tempTestimonialList
    }, function(error) {})['finally'](function() {
        testimonialsDataLoaded()
    });
    var timer;
    var sliderFunc = function() {
        timer = $timeout(function() {
            $scope.prevSlide();
            timer = $timeout(sliderFunc, 2000)
        }, 2000)
    };
    sliderFunc();
    $scope.$on('$destroy', function() {
        $timeout.cancel(timer)
    })
})