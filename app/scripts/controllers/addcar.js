'use strict'; /** * @ngdoc function * @name dakshinHondaWebsiteApp.controller:MyaccountCtrl * @description * # MyaccountCtrl * Controller of the dakshinHondaWebsiteApp */
angular.module('dakshinHondaWebsiteApp').controller('addcarCtrl', function($scope, $route, $sessionStorage, $http, growl, $location, $rootScope, $mdDialog, localStorageService, $filter, $localStorage) {
    $localStorage.DHuserSession = $sessionStorage.DHuserSession;
    $rootScope.$on("CallParentMethod", function() {
        $scope.addCar();
    });
    $scope.addCar = function(ev, growl, $sessionStorage) {
        console.log("adding car.............");
        console.log(ev, growl, $sessionStorage);
        if ($localStorage.DHuserSession) {
            $mdDialog.show({
                controller: carForm,
                templateUrl: 'views/addMyCar.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            }).then(function(answer) {}, function() {});
        } else {
            callnotify('Login to add a car!', 'information');
        } /*}catch (e){showGrowl();}*/
    }

    function showGrowl() { /*growl.warning('Login to add car.',{disableCountDown: true});*/ }

    function carForm($scope, $mdDialog, $sessionStorage, growl, $rootScope, $route) {
        $scope.newCar = {};
        $scope.carsModelList = [];
        $scope.newCar.user_id = $localStorage.DHuserSession.id;
        $http.get(api_url + "/website/cars").then(function(success) {
            $scope.carsList = success.data.cars;
        }, function(error) {})['finally'](function() {
            $scope.test = false;
        });
        $scope.testFuels = function() {
            var addCar = $scope.carsList;
            var addCarFuels = [];
            for (var i = 0; i < addCar.length; i++) {
                if (addCar[i].car_name == $scope.newCar.model) {
                    addCarFuels.push(addCar[i].fuel_types);
                    break;
                }
            }
            $scope.addCarFuelTypes = addCarFuels[0];
        }
        $scope.closeCarModal = function() {
            $mdDialog.cancel({})
        };
        $scope.addMyCar = function() {
            console.log("data",$scope.newCar);
             
            if ($localStorage.DHuserSession) {
                $scope.dataToSend = {};
                if (!document.getElementById("carModel").value) {
                    callnotify('Car Model Required!', 'error');
                } else if (!document.getElementById("fueltype").value) {
                    callnotify('Car Fuel Type Required!', 'error');
                } else if (!document.getElementById("regNumber").value) {
                    callnotify('Car Registration Number Required!', 'error');
                } else {
                    $scope.newCar.insurance_expiry = $('#expairyDate').val();
                    if ($scope.newCar.date_of_purchase && $scope.newCar.last_service_date && $scope.newCar.date_of_purchase.split("-").reverse().join("") > $scope.newCar.last_service_date.split("-").reverse().join("")) {
                        $scope.serviceDate = !0;
                        return !1
                    } else {
                        $scope.serviceDate = !1
                    }
                    if ($scope.newCar.date_of_purchase && $scope.newCar.insurance_expiry && $scope.newCar.date_of_purchase.split("-").reverse().join("") > $scope.newCar.insurance_expiry.split("-").reverse().join("")) {
                        $scope.insurance_expiry = !0;
                        console.log("experied");
                        // callnotify('Invalid Insurance date ', 'error');
                        return !1
                    } else {
                        console.log("not experied");
                        $scope.insurance_expiry = !1;

                    }
             
                     
                    $scope.dataToSend.my_car = $scope.newCar;
                    console.log($scope.dataToSend)
                    if ($localStorage.DHuserSession.authentication_token) {
                        callnotify('Adding Car... ', 'info');
                        $http.post(api_url + "/website/my_cars?auth_token=" + $localStorage.DHuserSession.authentication_token, $scope.dataToSend).then(function(success) {
                            callnotify('Car Added', 'success');
                        console.log("success",success);
                            $mdDialog.hide();
                            setTimeout(function() {
                                window.location.reload();
                            }, 2000);
                        }, function(error) {
                            console.log(JSON.stringify(error));
                        })['finally'](function() {
                            $scope.test = false;
                        });
                    }
                }
            } else {
                callnotify('Login to continue!', 'information');
            }
        }
    }
 
   
});