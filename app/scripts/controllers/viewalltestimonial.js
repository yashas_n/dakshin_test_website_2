'use strict';
angular.module('dakshinHondaWebsiteApp').controller('viewalltestimonialCtrl', function($scope, $location, $http, growl, $mdDialog, $sessionStorage, $rootScope, localStorageService) {
    window.scrollTo(0, 0);
    $scope.testimonialViewAllList = {};
    var tempTestimonialList = [];
    $scope.dataLoaded = !1;
    $scope.currentIndex = 0;
    $scope.setCurrentSlideIndex = function(index) {
        $scope.currentIndex = index
    };
    $scope.isCurrentSlideIndex = function(index) {
        return $scope.currentIndex === index
    };
    $scope.prevArrow = function() {
        $scope.currentIndex = ($scope.currentIndex < $scope.testimonialListViewAllFinal.length - 1) ? ++$scope.currentIndex : 0
    };
    $scope.nextArrow = function() {
        $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.testimonialListViewAllFinal.length - 1
    };
    $http.get(api_url + "/website/get_limit_testmonials").then(function(success) {
        $scope.testimonialList = success.data.testmonials;
        console.log("img",$scope.testimonialList);
        for(var i=0;i<$scope.testimonialList.length;i++){
         console.log("testimonialList[9]",$scope.testimonialList[i]);
        }
        angular.forEach($scope.testimonialList, function(key, value) {
            console.log("img",key.image);
            tempTestimonialList.push({
                'testimonialDescription': key.description,
                'testimonialTitle': key.name,
                'image': api_url + key.image.url
            })
        });
        $scope.testimonialListFinal = tempTestimonialList
    }, function(error) {})['finally'](function() {
        pageDataLoaded();
        $scope.dataLoaded = !0
    });
    $scope.testimonialPageBanner = {
        "category": "Testmonial"
    }
})