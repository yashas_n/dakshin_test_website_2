'use strict';
angular.module('dakshinHondaWebsiteApp').controller('servicebookingCtrl', function ($scope, $location, $http, growl, $rootScope, $sessionStorage, localStorageService, $timeout) {
    window.scrollTo(0, 0);
    $scope.showAddress = !1;
    $scope.hideLogin = !1;
    $scope.testDrivebooking = function () {
        $location.path('/testDrive')
    }
    $scope.servicebooking = function () {
        $location.path('/servicebooking')
    }
    $scope.insurancebooking = function () {
        $location.path('/insurance')
    }
    $scope.emicalculatorServices = function () {
        $location.path('/emicalculator')
    }
    $scope.onRoadPriceQuoteServices = function () {
        $location.path('/onRoadPriceQuote')
    }
    $scope.feedback = function () {
        $location.path('/feedback')
    }
    $scope.contactUsServices = function () {
        $location.path('/contactUs')
    }
    Date.prototype.addDays = function (days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat
    }
    var dat = new Date();
    $scope.minDate = dat.addDays(0).toDateString();
    $scope.changeserviceDate = function (date) {
        var x = date.split(',', 2);
        $scope.serviceFormData.date = x[0];
        $scope.testDay = x[1].toLowerCase().trim()
    }

    function dateConvertService() {
        var d = new Date();
        var weekday = new Array(7);
        weekday[6] = "Sunday";
        weekday[0] = "Monday";
        weekday[1] = "Tuesday";
        weekday[2] = "Wednesday";
        weekday[3] = "Thursday";
        weekday[4] = "Friday";
        weekday[5] = "Saturday";
        var n = weekday[d.getDay()];
        $scope.available_service_day = n
    }

    function timeDateControl() {
        $scope.timeControlData = {
            "category": "Book test drive",
            "weekday": $scope.testDay
        };
        $scope.test = !0;
        $scope.message = 'Checking available date & time...'
        $http.post(api_url + "/mobile/find_book_time_controls.json", $scope.timeControlData).then(function (success) {
            var saveResponse = success.data;
            var choosedTime = document.getElementById("myTime").value;
            for (var j = 0; j < saveResponse["Booking Time Controls"].length; j++) {
                if (saveResponse["Booking Time Controls"][j].weekday == $scope.testDay) {
                    if (convertTimeAMPM1(saveResponse["Booking Time Controls"][j].open_time) < ConvertTimeformat('24', choosedTime) && ConvertTimeformat('24', choosedTime) < convertTimeAMPM1(saveResponse["Booking Time Controls"][j].close_time)) {
                        callBookServices(ConvertTimeformat('24', choosedTime))
                    } else {
                        if (choosedTime) {
                            var openTime = convertTimeAMPM1(saveResponse["Booking Time Controls"][j].open_time);
                            $scope.open_time = openTime.slice(0, 5);
                            var closeTime = convertTimeAMPM1(saveResponse["Booking Time Controls"][j].close_time);
                            $scope.close_time = closeTime.slice(0, 5);
                            $scope.showInfoServiceError = !0;
                            dateConvertService();
                            $timeout(function () {
                                $scope.showInfoServiceError = !1
                            }, 5000)
                        }
                    }
                } else { }
            }
        }, function (error) { })['finally'](function () {
            $scope.test = !1
        })
    }

    function convertTimeAMPM1(recievedTime) {
        var timeString = recievedTime;
        if (timeString) {
            var timeData = timeString.split('T', 2);
            var saveData = timeData[1].split('.', 2)
        }
        return saveData[0].slice(0, 5)
    }

    function ConvertTimeformat(format, str) {
        var time = str;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/: (\d+)/)[1]);
        var AMPM = time.slice(-2, 12);
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        return (sHours + ":" + sMinutes)
    }
    $scope.user = {};
    $scope.carsList = {};
    $scope.sendData = {};
    $scope.sendData.book_test_drife = {};
    var serviceStation = [];
    $scope.formtypefilter = {
        "city": 'Bengaluru',
        "type": 'Service'
    };
    $http.post(api_url + "/mobile/dealer_type_location", $scope.formtypefilter).then(function (success) {
        $scope.dealerList = success.data.dealers
    }, function (error) {
        console.log(JSON.stringify(error))
    })['finally'](function () { });
    $scope.getCarModel = function () {
        $scope.dataToSend = {};
        if ($sessionStorage.DHuserSession) {
            $scope.dataToSend.user_id = $sessionStorage.DHuserSession.id;
            $http.post(api_url + "/website/user_cars?auth_token=" + $sessionStorage.DHuserSession.authentication_token, $scope.dataToSend).then(function (success) {
                $scope.mycars = success.data.my_cars;
                if ($scope.mycars.length >= 0) {
                    $scope.showDropdown = !0
                }
                if ($scope.mycars.length == 0) {
                    callnotify("Please add car(s), in your profile, to continue with the booking!!", 'warning')
                }
                if ($scope.mycars.length == 1) {
                    $scope.modelData = $scope.mycars[0];
                    setTimeout(function () {
                        var reg = document.getElementById('regNumber1')
                        reg.value = $scope.mycars[0].registration_number;
                        var kms = document.getElementById('kmNumber')
                        kms.value = $scope.mycars[0].number_of_kms
                    }, 200)
                }
            }, function (error) {
                console.log(JSON.stringify(error))
            })['finally'](function () { })
        }
    }
    $scope.checkUserCars = function () {
        if ($scope.mycars.length == 0) {
            alert("No Cars")
        }
    }
    var serviceCarModelAllData = [];
    $scope.serviceCarData=[];
    $scope.modalServiceBookingData = function (model) {
        $scope.serviceCarData = model;
        setTimeout(function () {
            var reg = document.getElementById('regNumber1')
            reg.value = model.registration_number;
            var kms = document.getElementById('kmNumber')
            kms.value = model.number_of_kms
        }, 200)
    }
    $scope.serviceFormData = {};

    function updateServiceData() {
        $scope.userData = $sessionStorage.DHuserSession;
        $scope.serviceFormData.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.serviceFormData.email = $sessionStorage.DHuserSession.profile.email;
        $scope.serviceFormData.mobile = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $scope.serviceFormData.user_id = $sessionStorage.DHuserSession.profile.id;
        $scope.serviceFormData.address = $sessionStorage.DHuserSession.profile.address;
        $scope.serviceFormData.request_pick_up = !1;
        $scope.hideLogin = !0;
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token
    }
    if ($sessionStorage.DHuserSession) {
        updateServiceData();
        $scope.getCarModel()
    }
    $rootScope.$on('afterLogin', function (event, data) {
        updateServiceData()
    });
    $scope.$watch('serviceFormData.address', function (address) {
        var address = address;
        if (address.length > 0) {
            $scope.showAddress = !1
        }
    });
    $scope.bookAService = function () {
        console.log("booking a service..............",$scope.serviceFormData);
        $scope.dataToSend = {};
        var address = document.getElementById('userAddress').value;
        if (($scope.serviceFormData.request_pick_up == !0) && (address.length == 0)) {
            callnotify('Address required if pick up is requested!!', 'warning');
            $scope.showAddress = !0;
            return
        }
        console.log("$scope.userForm",$scope.userForm);
        if($scope.serviceFormData.name && $scope.serviceFormData.email && $scope.serviceFormData.mobile && $scope.serviceFormData.service_date && $scope.serviceFormData.service_type && $scope.serviceFormData.service_station && $scope.serviceFormData.service_date &&  $scope.serviceFormData.service_time){
            $scope.userForm.$valid=true;
        } else {
            $scope.userForm.$valid=false;
        }
        if ($scope.userForm.$valid) { 
            console.log("valid........");


// var myLink = document.getElementById('servicebooking');
// var google_conversion_id = 825753188;
// var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
// var google_remarketing_only = false;
// myLink.onclick = function(){

//     var script = document.createElement("script");
//     script.type = "text/javascript";
//     script.src = "//www.googleadservices.com/pagead/conversion.js"; 
//     document.getElementsByTagName("head")[0].appendChild(script);
//     return false;

// }

            $scope.serviceFormData.my_car_id = $scope.modelData.id;
            console.log("$scope.modelData",$scope.modelData.id);
            console.log($scope);
            if ($scope.serviceCarData.model) {
                $scope.serviceFormData.model = $scope.serviceCarData.model
            } else {
                $scope.serviceFormData.model = $scope.modelData.model;
            }
            if (!$sessionStorage.DHuserSession) {
                $scope.serviceFormData.my_car_id = 'nil'
            }

            var reg = document.getElementById('regNumber1').value;
             
            var kms = document.getElementById('kmNumber').value;
             
            $scope.serviceFormData.registration_number = reg;
            $scope.serviceFormData.milage =kms;
            $scope.serviceFormData.location = "bengalore";
            $scope.dataToSend.service_booking = $scope.serviceFormData;
            callnotify('Requesting service booking, please wait...', 'information');
            console.log("JSON.stringify",($scope.dataToSend));
            $http.post(api_url + "/website/service_bookings", $scope.dataToSend).then(function (success) {
                callnotify('Service booked!', 'success');
                $location.path('/main')
            }, function (error) {
                console.log("its an error!",(error))
            })['finally'](function () { })
        } else {
            console.log("else");
            callnotify('Form Incomplete!!', 'error')
        }
    }
    $scope.bookingTimeControl = function () {
        console.log("clicked the btn");
        var time = document.getElementById("myTime").value;
        if (!$scope.serviceFormData.service_date) {
            callnotify('Date required!', 'error')
        } else if (!time) {
            callnotify('Time required!', 'error')
        } else {
            time = time.replace(/ /g, '');
            time = time.split(':')
            var new_time = time[0] + ":" + time[1] + time[2]
            $scope.serviceFormData.service_time = new_time;
            $scope.checkData = {};
            $scope.checkData.category = "Book Service";
            $scope.checkData.time = $scope.serviceFormData.service_time;
            $scope.checkData.count = 0;
            $scope.checkData.date = $scope.serviceFormData.service_date;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            var today = dd + '-' + mm + '-' + yyyy;
            $scope.checkData.current_date = today;
            console.log("$scope.checkData",$scope.checkData);
            $http.post(api_url + "/mobile/book_time_control", $scope.checkData).then(function (success) {
                console.log("succes",success);
                if (success.data.status == !1) {
                    console.log("succes",success);
                    callnotify(success.data.message, 'warning')
                } else if (success.data.status == !0) {
                    $scope.bookAService()
                } else { }
            }, function (error) {
                console.log(JSON.stringify(error))
            })['finally'](function () { })
        }
    }
})