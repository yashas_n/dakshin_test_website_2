'use strict';
angular.module('dakshinHondaWebsiteApp').controller('onRoadPriceQuoteCtrl', function($scope, $location, $http, $window, growl, $rootScope, localStorageService, $sessionStorage) {
    window.scrollTo(0, 0);
    $scope.onRoadPrice = {
        "category": "OnRoadPriceQuote"
    };

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email)
    }
    $scope.testDrivebooking = function() {
        $location.path('/testDrive')
    }
    $scope.servicebooking = function() {
        $location.path('/servicebooking')
    }
    $scope.insurancebooking = function() {
        $location.path('/insurance')
    }
    $scope.emicalculatorServices = function() {
        $location.path('/emicalculator')
    }
    $scope.onRoadPriceQuoteServices = function() {
        $location.path('/onRoadPriceQuote')
    }
    $scope.feedback = function() {
        $location.path('/feedback')
    }
    $scope.contactUsServices = function() {
        $location.path('/contactUs')
    }
    $scope.citydata = {
        "location": 'Bengaluru'
    };
    $http.post(api_url + "/website/emi_data", $scope.citydata).then(function(success) {
        $scope.emidata = success.data
    }, function(error) {})['finally'](function() {});
    $scope.emiModel = function() {
        var emiCarData = $scope.emidata.cars;
        var emicarVariants = [];
        for (var i = 0; i < emiCarData.length; i++) {
            if (emiCarData[i].car_name == $scope.emiCarModel) {
                emicarVariants.push(emiCarData[i].varients);
                break
            }
        }
        $scope.emicarVariantData = emicarVariants[0]
    }
    $scope.onRoadFormData = {};

    function updateFormData() {
        $scope.onRoadFormData.user_id = $sessionStorage.DHuserSession.id;
        $scope.onRoadFormData.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.onRoadFormData.email = $sessionStorage.DHuserSession.profile.email;
        $scope.onRoadFormData.mobile_number = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token
    }
    if ($sessionStorage.DHuserSession) {
        updateFormData()
    }
    $rootScope.$on('afterLogin', function(event, data) {
        updateFormData()
    });
    $scope.getPriceQuote = function() {
        if ($scope.userForm.$valid) {

// var myLink = document.getElementById('onRoadPrice');
// var google_conversion_id = 825753188;
// var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
// var google_remarketing_only = false;
// myLink.onclick = function(){

//     var script = document.createElement("script");
//     script.type = "text/javascript";
//     script.src = "//www.googleadservices.com/pagead/conversion.js"; 
//     document.getElementsByTagName("head")[0].appendChild(script);
//     return false;

// }
            getPrice()
        }
    };

    function getPrice() {
        var validEmail = validateEmail($scope.onRoadFormData.email);
        if (!validEmail) {
            callnotify('Inalid Email!', 'error');
            return
        } else {
            callnotify('Requesting Price Quote, please wait!', 'information');
            console.log("data ",$scope.onRoadFormData);
            $http.post(api_url + "/website/email_price", $scope.onRoadFormData).then(function(success) {
                var msg = 'Price quote sent to ' + $scope.onRoadFormData.email + '!';
                callnotify(msg, 'success');
                var myLink = document.getElementById('onRoadPrice');
var google_conversion_id = 825753188;
var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
var google_remarketing_only = false;
myLink.onclick = function(){

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "//www.googleadservices.com/pagead/conversion.js"; 
    document.getElementsByTagName("head")[0].appendChild(script);
    return false;

}
                $location.path('/main')
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {})
        }
    }
})