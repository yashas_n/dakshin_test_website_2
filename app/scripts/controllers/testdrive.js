// "use strict";
// angular.module("dakshinHondaWebsiteApp").controller("TestdriveCtrl", ["$scope", "$location", "$http", "growl", "$sessionStorage", "$rootScope", "localStorageService", "$filter", "$timeout", function (e, t, a, i, r, o, s, n, c) {
   
//     e.testDriveFormdata = {}, e.testDriveFormdata.address = "", e.testDriveFormdata.request_pick_up = !1, e.hideLogin = !1, e.showAddress = !1, window.scrollTo(0, 0), e.testDriveFormdata = {};
//     e.user = {}, e.carsList = {}, e.sendData = {}, Date.prototype.addDays = function (e) {
//         var t = new Date(this.valueOf());
//         return t.setDate(t.getDate() + e), t
//     };
//     var l = new Date;
//     e.minDate = l.addDays(0).toDateString(), e.changeDate = function (t) {
//         var a = t.split(",", 2);
//         e.testDriveFormdata.date = a[0], e.testDay = a[1].toLowerCase().trim()
//     }, e.test = !0, e.message = "Loading Car Models...", a.get(api_url + "/website/cars").then(function (t) {
//         alert("asdf");
//         console.log("t","*******8",t.data.cars,$rootScope.frombannerData);
//         // if($rootScope.frombannerData)
//         e.carsList = t.data.cars;
    
//     }, function (e) { }).finally(function () {
//         e.test = !1
//     });
//     var d = [];
 
//     function u() {
//         e.testDriveFormdata.name = r.DHuserSession.profile.full_name, e.testDriveFormdata.email = r.DHuserSession.profile.email, e.testDriveFormdata.mobile = parseInt(r.DHuserSession.profile.mobile), e.testDriveFormdata.user_id = r.DHuserSession.profile.id, e.testDriveFormdata.address = r.DHuserSession.profile.address, e.testDriveFormdata.request_pick_up = !1, e.hideLogin = !0, a.defaults.headers.common["X-USER-TOKEN"] = r.DHuserSession.authentication_token
//     }
//     e.testFuels = function () {
//         var t = e.carsList;
//         d = [];

//         for (var a = 0; a < t.length; a++)
//             if (t[a].car_name == e.testDriveFormdata.model) {
//                 d.push(t[a].fuel_types);
//                 break
//             }
//         e.testDriveFuelTypes = d[0]
//     }, e.testBanner = {
//         category: "Test Drive Booking"
//     }, e.formtypefilter = {
//         city: "Bengaluru",
//         type: "Sales"
//     }, a.post(api_url + "/mobile/dealer_type_location", e.formtypefilter).then(function (t) {
//         e.dealerList = t.data.dealers
//     }, function (e) {
//         console.log(JSON.stringify(e))
//     }).finally(function () { }), r.DHuserSession && u(), o.$on("afterLogin", function (e, t) {
//         u()
//     }), e.testDrivebooking = function () {
//         t.path("/testDrive")
//     }, e.servicebooking = function () {
//         t.path("/servicebooking")
//     }, e.insurancebooking = function () {
//         t.path("/insurance")
//     }, e.emicalculatorServices = function () {
//         t.path("/emicalculator")
//     }, e.onRoadPriceQuoteServices = function () {
//         t.path("/onRoadPriceQuote")
//     }, e.feedback = function () {
//         t.path("/feedback")
//     }, e.contactUsServices = function () {
//         t.path("/contactUs")
//     }, e.$watch("testDriveFormdata.address", function (t) {
//         t = t;
//         try {
//             t.length > 0 && (e.showAddress = !1)
//         } catch (e) { }
//     }), e.testDrive = function () {
//         e.dataToSend = {};
//         var i = document.getElementById("userAddress").value;
//         if (1 == e.testDriveFormdata.request_pick_up && 0 == i.length) return callnotify("Address required if pick up is requested!!", "warning"), void (e.showAddress = !0);
//         e.userForm.$valid ? (e.dataToSend.test_drive = e.testDriveFormdata, callnotify("Requesting test drive, please wait...", "information"), a.post(api_url + "/website/test_drives", e.dataToSend).then(function (e) {
//             callnotify("Test drive booked!", "success"), t.path("/main")
//         }, function (e) {
//             console.log(JSON.stringify(e))
//         }).finally(function () { })) : callnotify("Form Incomplete!!", "error")
//     }, e.bookingTimeControl = function () {
//         var t = document.getElementById("timepicker").value;
//         if (e.testDriveFormdata.date)
//             if (t) {
//                 var i = (t = (t = t.replace(/ /g, "")).split(":"))[0] + ":" + t[1] + t[2];
//                 e.testDriveFormdata.time = i, e.checkData = {}, e.checkData.category = "Book Test Ride", e.checkData.time = e.testDriveFormdata.time, e.checkData.count = 0, e.checkData.date = e.testDriveFormdata.date;
//                 var r = (n = new Date).getDate(),
//                     o = n.getMonth() + 1,
//                     s = n.getFullYear();
//                 r < 10 && (r = "0" + r), o < 10 && (o = "0" + o);
//                 var n = r + "-" + o + "-" + s;
//                 e.checkData.current_date = n, a.post(api_url + "/mobile/book_time_control", e.checkData).then(function (t) {
//                     console.log(JSON.stringify(t)), 0 == t.data.status ? callnotify(t.data.message, "warning") : 1 == t.data.status && e.testDrive()
//                 }, function (e) {
//                     console.log(JSON.stringify(e))
//                 }).finally(function () { })
//             } else callnotify("Time required!", "error");
//         else callnotify("Date required!", "error")
//     }
// }]); 

'use strict';

angular.module('dakshinHondaWebsiteApp')
    .controller('TestdriveCtrl', function($scope, $location, $http, growl, $sessionStorage, $rootScope, localStorageService, $filter, $timeout) {
        $scope.testDriveFormdata = {};
        $scope.testDriveFormdata.address = "";
        $scope.testDriveFormdata.request_pick_up = false;
        $scope.hideLogin = false;
        $scope.showAddress = false;
        window.scrollTo(0, 0);
        $scope.testDriveFormdata = {};
        $scope.fromBanner=false;
    
        var serviceStation = [];
        $scope.user = {};
        $scope.carsList = {};
        $scope.sendData = {};

        Date.prototype.addDays = function(days) {
            var dat = new Date(this.valueOf());
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        var dat = new Date();
        $scope.minDate = dat.addDays(0).toDateString();

        $scope.changeDate = function(date) {
            var x = date.split(',', 2);
            $scope.testDriveFormdata.date = x[0];
            $scope.testDay = x[1].toLowerCase().trim();
        }


        //get New cars
        
        $scope.test = true;
        $scope.message = 'Loading Car Models...'
        $http.get(api_url + "/website/cars")
            .then(function(success) {
                    console.log("$rootScope.frombannerData",$rootScope.frombannerData);
                if($rootScope.frombannerData==null){
                    $scope.carsList={};
                    $scope.carsList = success.data.cars;
                    $scope.fromBanner=false;
                    
                } else {
                    $scope.fromBanner=true;
                    console.log("else");
                    console.log($rootScope.frombannerData,"***",success.data.cars);
                    for(var index=0;index<success.data.cars.length;index++){
                        if(success.data.cars[index].id==$rootScope.frombannerData){
                            $scope.carsList=success.data.cars[index];
                        }
                    }
                    $scope.testDriveFormdata.model =$scope.carsList.car_name;
                    $scope.testFuelsbanner($scope.testDriveFormdata.model);
                    console.log("$scope.carsList",$scope.carsList);
                 
                }

            }, function(error) {})['finally'](function() {
                $scope.test = false;
            });

        var testDriveFuels = [];
        $scope.testFuels = function() {
            var testDrive = $scope.carsList;
            testDriveFuels = [];

            for (var i = 0; i < testDrive.length; i++) {

                if (testDrive[i].car_name == $scope.testDriveFormdata.model) {
                    testDriveFuels.push(testDrive[i].fuel_types);
                    break;
                }
            }
            $scope.testDriveFuelTypes = testDriveFuels[0];
        }
        $scope.testFuelsbanner = function(selectedcar) {
            console.log(selectedcar);
            var testDrive = $scope.carsList;
            testDriveFuels = [];
            console.log("testDrive",testDrive);
            testDriveFuels.push(testDrive.fuel_types);

             
          
            $scope.testDriveFuelTypes = testDriveFuels[0];
            console.log("$scope.testDriveFuelTypes",testDriveFuels);
        }

        $scope.testBanner = {
            "category": "Test Drive Booking"
        };

        $scope.formtypefilter = {
            "city": 'Bengaluru',
            "type": 'Sales'
        };

        $http.post(api_url + "/mobile/dealer_type_location", $scope.formtypefilter)
        .then(function(success) {
            $scope.dealerList = success.data.dealers;
        }, function(error) {
            console.log(JSON.stringify(error));
        })['finally'](function() {
        });


        function updateTestDriveData() {
            $scope.testDriveFormdata.name = $sessionStorage.DHuserSession.profile.full_name;
            $scope.testDriveFormdata.email = $sessionStorage.DHuserSession.profile.email;
            $scope.testDriveFormdata.mobile = parseInt($sessionStorage.DHuserSession.profile.mobile);
            $scope.testDriveFormdata.user_id = $sessionStorage.DHuserSession.profile.id;
            $scope.testDriveFormdata.address = $sessionStorage.DHuserSession.profile.address;
            $scope.testDriveFormdata.request_pick_up = false;
            $scope.hideLogin = true;
            $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token;

        }

        if ($sessionStorage.DHuserSession) {
            updateTestDriveData();
        }

        $rootScope.$on('afterLogin', function(event, data) {
            updateTestDriveData();
        });


        $scope.testDrivebooking = function() {
            $location.path('/testDrive');
        }

        $scope.servicebooking = function() {
            $location.path('/servicebooking');
        }

        $scope.insurancebooking = function() {
            $location.path('/insurance');
        }


        $scope.emicalculatorServices = function() {
            $location.path('/emicalculator');
        }

        $scope.onRoadPriceQuoteServices = function() {
            $location.path('/onRoadPriceQuote');
        }

        $scope.feedback = function() {
            $location.path('/feedback');
        }

        $scope.contactUsServices = function() {
            $location.path('/contactUs');
        }

        $scope.$watch('testDriveFormdata.address', function(address) {
            var address = address;
            try
            {
                if (address.length > 0) {
                     $scope.showAddress = false;
                }
            } 
            catch(e)
            {
                
            }
        });


        $scope.testDrive = function() {
            $scope.dataToSend = {};
            var address = document.getElementById('userAddress').value;
            if(($scope.testDriveFormdata.request_pick_up == true) && (address.length == 0))
            {
                callnotify('Address required if pick up is requested!!', 'warning');  
                $scope.showAddress = true;
                return;
            }
            console.log("$scope.userForm.$valid",$scope.userForm.$valid);
            if ($scope.userForm.$valid) {

var myLink = document.getElementById('testdrive');



                $scope.dataToSend.test_drive = $scope.testDriveFormdata;
                //$scope.beforeBookTestDrive();
                callnotify('Requesting test drive, please wait...', 'information');
                $http.post(api_url + "/website/test_drives", $scope.dataToSend)
                    .then(function(success) {
                        callnotify('Test drive booked!', 'success');
                        var google_conversion_id = 825753188;
var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
var google_remarketing_only = false;
myLink.onclick = function(){

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "//www.googleadservices.com/pagead/conversion.js"; 
    document.getElementsByTagName("head")[0].appendChild(script);
    return false;

}
                        window.location.reload();
                        $location.path('/main');
                        // setTimeout(function(){$location.path('/main');}, 2000);
                        // setTimeout(function(){ $location.path('/main');}, 3000);
                       
                    }, function(error) {
                        console.log(JSON.stringify(error));
                    })['finally'](function() {
                    });

            } else {
                callnotify('Form Incomplete!!', 'error');
            }
        }

        $scope.bookingTimeControl = function() 
        {
                var time = document.getElementById("timepicker").value;
                if (!$scope.testDriveFormdata.date)
                {
                    callnotify('Date required!', 'error');
                }
                else if(!time)
                {
                    callnotify('Time required!', 'error');
                }
                else
                {
                    time = time.replace(/ /g, '');
                    time = time.split(':')                
                    var new_time = time[0]+":"+time[1]+time[2]
                    $scope.testDriveFormdata.time = new_time;
                    $scope.checkData = {};
                    $scope.checkData.category = "Book Test Ride";
                    $scope.checkData.time = $scope.testDriveFormdata.time;
                    $scope.checkData.count = 0;                    
                    $scope.checkData.date = $scope.testDriveFormdata.date;
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1;
                    var yyyy = today.getFullYear();
                    if(dd<10){
                        dd='0'+dd;
                    } 
                    if(mm<10){
                        mm='0'+mm;
                    } 
                    var today = dd+'-'+mm+'-'+yyyy;
                    $scope.checkData.current_date=today;
                    $http.post(api_url + "/mobile/book_time_control", $scope.checkData)
                    .then(function(success) {
                        console.log(JSON.stringify(success));
                        if(success.data.status == false)
                        {
                            callnotify(success.data.message, 'warning');
                        }
                        else if(success.data.status == true)
                        {
                            $scope.testDrive();                            
                        }
                        else{}
                    }, function(error) {
                        console.log(JSON.stringify(error));
                    })['finally'](function() {
                    });
                }        
        }



    });