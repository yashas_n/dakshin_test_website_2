'use strict';angular.module('dakshinHondaWebsiteApp').controller('paymentdetailsctrl',function($scope,$sessionStorage,$location,$localStorage,growl,$http,$rootScope)
{window.scrollTo(0,0);$scope.dataLoaded=!1;$scope.paymentdata={};$http.defaults.headers.common['Access-Control-Allow-Origin']='*';$http.defaults.headers.post.dataType='json'
$scope.userdata={};var textImage="images/Text.png";var wordImage="images/Word.png";var pdfImage="images/PDF_icon.png";var jpgImage="images/JPEG_icon.png";$('#formsubmit').prop('disabled',!1);if($sessionStorage.DHuserSession)
{$scope.userFeedbcakData=$sessionStorage.DHuserSession.profile;$scope.userdata.name=$scope.userFeedbcakData.full_name;$scope.userdata.email=$scope.userFeedbcakData.email;$scope.userdata.mobile=parseInt($scope.userFeedbcakData.mobile)}
if($sessionStorage.DHuserSession)
{$scope.DHuserData=$sessionStorage.DHuserSession;$http.post(api_url+"/website/payment_pending_with_record",JSON.stringify({"payment_id":$localStorage.transactionid})).then(function(success)
{if(success.data.status=="Pending")
{var paymentdata=success.data;var paymenttrans={"phone":paymentdata.phone,"transaction_id":paymentdata.txn_id,"checksum":paymentdata.checksum};$scope.test=!0;$scope.message='Loading ...';$http.post("https:/partners.vpaynow.com/web_check_transaction",paymenttrans,{headers:{'Content-Type':'application/json','X-App-Token':paymentdata.app_token}}).then(function(success)
{var updatestatus={"status_code":success.data.request.code,"payment_id":$localStorage.transactionid,"status":success.data.status};$scope.test=!0;$scope.message='Loading ...';$http.post(api_url+"/website/update_transaction_status",updatestatus).then(function(success)
{loadfiles()},function(error)
{})['finally'](function()
{$scope.test=!1})},function(error)
{loadfiles()})['finally'](function()
{$scope.test=!1})}
else{loadfiles()}},function(error)
{loadfiles()})['finally'](function()
{$scope.test=!1})}
$scope.showimg=!1;function loadfiles()
{$scope.test=!0;$scope.message='Loading User Cars...';$http.post(api_url+"/website/get_payment",JSON.stringify({"id":$localStorage.transactionid})).then(function(success)
{console.log(success);$scope.paymentdata=success.data;var paymentdata=success.data;$scope.showimg=!1;if(paymentdata.file_type=='text/plain')
{$scope.imageurl='images/Text.png'}
else if(paymentdata.file_type=='application/pdf')
{$scope.imageurl='images/PDF_icon.png'}
else if(paymentdata.file_type=='application/msword')
{$scope.imageurl='images/Word.png'}
else if(paymentdata.file_type=='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
{$scope.imageurl='images/Word.png'}
else if(paymentdata.file_type=='image/jpeg'&&paymentdata.entity_type!=null)
{$scope.imageurl="https://dakshin-backend.myridz.com/"+paymentdata.image.url+"?"+paymentdata.updated_at}
else if(paymentdata.file_type=='image/png'&&paymentdata.entity_type!=null)
{$scope.imageurl="https://dakshin-backend.myridz.com/"+paymentdata.image.url+"?"+paymentdata.updated_at}
else if(paymentdata.file_type==null)
{$scope.showimg=!0;$scope.imageurl='/images/Upload.svg'}
else{$scope.showimg=!0;$scope.imageurl='/images/Upload.svg'}
console.log($scope.imageurl)},function(error)
{})['finally'](function()
{$scope.dataLoaded=!0;$scope.test=!1})}
$scope.uploadfiles=function(id)
{$('#uploadImage').click()}
$scope.uploaddocument=function(id)
{$scope.fileType=$scope.image.filetype.substr($scope.image.filetype.indexOf("/")+1);var data="data:application/"+$scope.fileType+";base64,("+$scope.image.base64+")";if($scope.image.filetype=="text/plain")
{jQuery("#smallimage").attr('src',textImage)}
else if($scope.image.filetype=="image/jpeg"||$scope.image.filetype=="image/png")
{jQuery("#smallimage").attr('src',jpgImage);jQuery("#smallimage").attr('class','addmydocsimage')}
else if($scope.image.filetype=="application/pdf")
{jQuery("#smallimage").attr('src',pdfImage)}
else if($scope.image.filetype=="application/msword"||$scope.image.filetype=="application/vnd.openxmlformats-officedocument.wordprocessingml.document")
{jQuery("#smallimage").attr('src',wordImage)}
var sendata={"image":data,"filename":$scope.image.filename,"file_type":$scope.image.filetype};var message=growl.info("Uploading, Don't refresh",{ttl:-1});$('#formsubmit').prop('disabled',!0);$http.put("https://dakshin-backend.myridz.com/mobile/payments/"+id+".json",{"payment":sendata}).then(function(success)
{$scope.test=!1;message.destroy();growl.success("File upload successful!",{disableCountDown:!0});$('#formsubmit').prop('disabled',!1);$location.path('/mypayments')},function(error)
{$('#formsubmit').prop('disabled',!1);growl.error("File upload failed. Try again later",{disableCountDown:!0})})['finally'](function()
{$scope.test=!1;$('#formsubmit').prop('disabled',!1)})}
$scope.viewimage=function(url)
{if(url)
{window.open(url)}}
$rootScope.$on('onCarAdd',function()
{$scope.getCarList()})})