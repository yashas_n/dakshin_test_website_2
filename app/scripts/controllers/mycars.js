'use strict';
angular.module('dakshinHondaWebsiteApp').controller('MyCarsCtrl', function ($scope, $mdDialog, $sessionStorage, $http, $rootScope, $localStorage) {
    window.scrollTo(0, 0);
    $scope.dataToSend = {};
    if ($sessionStorage.DHuserSession) {
        $scope.dataToSend.user_id = $sessionStorage.DHuserSession.id;
        $http.post(api_url + "/website/user_cars?auth_token=" + $sessionStorage.DHuserSession.authentication_token, $scope.dataToSend).then(function (success) {
            $scope.mycars = success.data.my_cars;
            console.log("$scope.mycars", $scope.mycars);
        }, function (error) {
            console.log(JSON.stringify(error))
        })['finally'](function () { })
    }
    $rootScope.$on('onCarAdd', function () {
        $scope.getCarList()
    })
    $scope.confirmdelete = function (index) {
        var txt;
        var r = confirm("Are you sure !");
        if (r == true) {
            txt = "You pressed OK!";
            $scope.remove(index);
        } else {
            txt = "You pressed Cancel!";
            return;
        }

        console.log("^^^^^^^^", txt, r);

    }
    $scope.remove = function (index) {
        console.log(index, "********", $scope.mycars);
        if ($scope.mycars.length > 1) {
            // $scope.mycars.splice(index, 1);
            console.log("remove this", $scope.mycars[index]);
            var removeID = $scope.mycars[index].id;

            if ($localStorage.DHuserSession.authentication_token) {
                callnotify('Deleting Car... ', 'info');
                $http.delete(api_url + "/website/my_cars/" + removeID + "?auth_token=" + $localStorage.DHuserSession.authentication_token).then(function (success) {
                    callnotify('Car deleted', 'success');
                    console.log("success", success);
                    $mdDialog.hide();
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }, function (error) {
                    console.log((error));
                })['finally'](function () {
                    $scope.test = false;
                });
            }
        } else {
            callnotify('Atleast one Car is needed ', 'information');
            return (0);
        }
    }
    $scope.updatecardetails = function (ev, growl, $sessionStorage) {
        console.log("data", ev, growl, $sessionStorage);
        $localStorage.updatemycardetails = growl;

        // $location.path('updatemycars')
        console.log("adding car.............");
        console.log(ev, growl, $sessionStorage);
        if ($localStorage.DHuserSession) {
            $mdDialog.show({
                controller: carForm,
                templateUrl: 'views/updateMyCar.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            }).then(function (answer) { }, function () { });
        } else {
            callnotify('Login to add a car!', 'information');
        } /*}catch (e){showGrowl();}*/
    }

    function carForm($scope, $mdDialog, $sessionStorage, growl, $rootScope, $route) {
        $scope.oldCar = {};
        $scope.carsModelList = [];
        $scope.oldCar.user_id = $localStorage.DHuserSession.id;
        $scope.oldCar.id = $localStorage.updatemycardetails.id;
        $scope.oldCar.model = $localStorage.updatemycardetails.model;
        $scope.oldCar.fuel_type = $localStorage.updatemycardetails.fuel_type;
        $scope.oldCar.registration_number = $localStorage.updatemycardetails.registration_number;
        $scope.oldCar.date_of_purchase = $localStorage.updatemycardetails.date_of_purchase;
        $scope.oldCar.last_service_date = $localStorage.updatemycardetails.last_service_date;
        $scope.oldCar.number_of_kms = $localStorage.updatemycardetails.number_of_kms;
        $scope.oldCar.engine_number = $localStorage.updatemycardetails.engine_number;
        $scope.oldCar.chasis_number = $localStorage.updatemycardetails.chasis_number;
        $scope.oldCar.insurance_provider = $localStorage.updatemycardetails.insurance_provider;
        $scope.oldCar.policy_number = $localStorage.updatemycardetails.policy_number;


        $scope.oldCar.insurance_expiry = $localStorage.updatemycardetails.insurance_expiry;
        //  $scope.testFuels();
        console.log("model", $scope.oldCar, "*******", $localStorage.updatemycardetails);
        $http.get(api_url + "/website/cars").then(function (success) {
            $scope.carsList = success.data.cars;
        }, function (error) { })['finally'](function () {
            $scope.test = false;
        });
        $scope.testFuels = function () {
            var addCar = $scope.carsList;
            var addCarFuels = [];
            for (var i = 0; i < addCar.length; i++) {
                if (addCar[i].car_name == $scope.oldCar.model) {
                    addCarFuels.push(addCar[i].fuel_types);
                    break;
                }
            }
            $scope.addCarFuelTypes = addCarFuels[0];
        }
        $scope.closeCarModal = function () {
            $mdDialog.cancel({})
        };
        $scope.experydate = false;
    
        $scope.addMyCar = function () {
            
            console.log("data", $scope.oldCar);
            
            if ($localStorage.DHuserSession) {
                $scope.dataToSend = {};
                if (!document.getElementById("carModel").value) {
                    callnotify('Car Model Required!', 'error');
                } else if (!document.getElementById("fueltype").value) {
                    callnotify('Car Fuel Type Required!', 'error');
                } else if (!document.getElementById("regNumber").value) {
                    callnotify('Car Registration Number Required!', 'error');
                } else {
                    $scope.oldCar.insurance_expiry = $('#expairyDate').val();

                    if ($scope.oldCar.date_of_purchase && $scope.oldCar.insurance_expiry && $scope.oldCar.date_of_purchase.split("-").reverse().join("") > $scope.oldCar.insurance_expiry.split("-").reverse().join("")) {
                        $scope.insurance_expiry = !0;
                        console.log("insurance_expiry experied");
                        // callnotify('Invalid Insurance date ', 'error');
                        return !1
                    } else {
                        console.log("insurance_expiry not experied");
                        $scope.insurance_expiry = !1;

                    }
                    $scope.oldCar.last_service_date = $('#lastDate').val();
                    console.log("$scope.oldCar.date_of_purchase", $scope.oldCar.date_of_purchase);
                    console.log("$scope.oldCar.last_service_date", $scope.oldCar.last_service_date);
                    console.log($scope.oldCar.date_of_purchase && $scope.oldCar.last_service_date && $scope.oldCar.date_of_purchase.split("-").reverse().join("") > $scope.oldCar.last_service_date.split("-").reverse().join(""));
                    if ($scope.oldCar.date_of_purchase && $scope.oldCar.last_service_date && $scope.oldCar.date_of_purchase.split("-").reverse().join("") > $scope.oldCar.last_service_date.split("-").reverse().join("")) {
                        $scope.serviceDate = !0;
                        console.log("experied");
                        return !1
                    } else {
                        console.log("not experied");
                        $scope.serviceDate = !1
                    }
                   




                    $scope.dataToSend.my_car = $scope.oldCar;
                    console.log($scope.dataToSend)
                    if ($localStorage.DHuserSession.authentication_token) {
                        callnotify('updating Car... ', 'info');
                        $http.put(api_url + "/website/my_cars/"+$scope.oldCar.id+"?auth_token=" + $localStorage.DHuserSession.authentication_token, $scope.dataToSend).then(function(success) {
                            callnotify('Car Updated', 'success');
                        console.log("success",success);
                            $mdDialog.hide();
                            setTimeout(function() {
                                window.location.reload();
                            }, 2000);
                        }, function(error) {
                            console.log((error));
                        })['finally'](function() {
                            $scope.test = false;
                        });
                    }
                }
            } else {
                callnotify('Login to continue!', 'information');
            }
        }
    }

})