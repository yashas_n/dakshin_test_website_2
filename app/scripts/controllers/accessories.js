'use strict'; /** * @ngdoc function * @name dakshinHondaWebsiteApp.controller:CarsCtrl * @description * # CarsCtrl * Controller of the dakshinHondaWebsiteApp */
angular.module('dakshinHondaWebsiteApp').controller('AccessoriesCtrl', function($scope, $localStorage, localStorageService, $location, $http, growl, $mdDialog, $rootScope,$sessionStorage) {
    window.scrollTo(0, 0);
    $scope.dataLoaded = false;
    $scope.$on('$routeChangeStart', function(scope, next, current) {
        if (next.$$route.controller != "AccessoriesCtrl") {
            $mdDialog.cancel({});
        }
    });
    $rootScope.$on('afterCarAccessory', function() {
        $scope.dataToSend = {};
        $localStorage.accessory_List = [];
        $localStorage.accessoryNameList = [];
        $scope.accessoriesToShow = {};
        $scope.dataToSend.id = $localStorage.accessoriesCarId;
        $scope.carName = $localStorage.accessoriesCarName;
        $scope.titleNames = {};
        $scope.allAccessories = {}; 
        $http.post(api_url + "/website/accessories_all", $scope.dataToSend).then(function(success) {
            $scope.sortAccessories(success.data);
        }, function(error) {
            console.log(JSON.stringify(error));
        })['finally'](function() {
            pageDataLoaded();
            $scope.dataLoaded = true;
        });
        if ($localStorage.selectedCarIndex) {
            $scope.carIndex = $localStorage.selectedCarIndex;
        }
    })
    $scope.goTOAccessories = function(index, car) {
        $scope.carIndex = index;
        $localStorage.selectedCarIndex = index;
        $localStorage.accessoriesCarName = car.car_name;
        $localStorage.accessoriesCarId = car.id;
        $location.path('/accessories');
        $rootScope.$emit('afterCarAccessory');
    }
    $scope.taxiCarsList = {};
    $http.get(api_url + "/website/cars").then(function(success) {
        $scope.carsList = success.data.cars;
    }, function(error) {})['finally'](function() {
        $scope.dataLoaded = true;
        pageDataLoaded();
    });
    $scope.showEnqueryButton = false;
    $scope.downloadBrochure = function(carId) {
        console.log(carId);
        $scope.data = {};
        $scope.data.car_id = carId;
        console.log(JSON.stringify($scope.data));
        $http.post(api_url + "/web/get_broucher_url/", $scope.data).then(function(success) {
            window.open(success.data.car_brochure_url, '_system', 'location=no');
        }, function(error) {
            console.log(JSON.stringify(error));
        })['finally'](function() {});
    }
    $scope.dataToSend = {};
    $scope.dataToSend.id = $localStorage.accessoriesCarId;
    $scope.carName = $localStorage.accessoriesCarName;
    $scope.titleNames = {};
    $scope.allAccessories = {};
    $http.post(api_url + "/website/accessories_all", $scope.dataToSend).then(function(success) {
        $scope.sortAccessories(success.data);
    }, function(error) {
        console.log(JSON.stringify(error));
    })['finally'](function() {
        pageDataLoaded();
        $scope.dataLoaded = true;
    });
    if ($localStorage.selectedCarIndex) {
        $scope.carIndex = $localStorage.selectedCarIndex;
    }
    $scope.sortAccessories = function(accessoriesData) {
        $scope.accessories = accessoriesData;
        var titleNamesArray = [];
        var allAccessoriesArray = [];
        angular.forEach($scope.accessories, function(key, value) {
            if (key.length > 0) {
                titleNamesArray.push({
                    'name': value
                })
                for (var i = 0; i < key.length; i++) {
                    allAccessoriesArray.push(key[i])
                }
            }
        });
        $scope.titleNames = titleNamesArray;
        $scope.allAccessories = allAccessoriesArray;
        $scope.accessoriesToShow = $scope.allAccessories;
        console.log("$scope.accessoriesToShow",$scope.accessoriesToShow);
    }
    var tempArray = [];
    $scope.filter = function(type, category) {
        $scope.accessoriesToShow = {};
        if (type == 'all') {
            $scope.accessoriesToShow = {};
            $scope.accessoriesToShow = $scope.allAccessories;
        } else {
            if (category != 'none') {
                tempArray = [];
                $scope.accessoriesToShow = {};
                var str = category;
                str = str.replace(/[^a-zA-Z]/g, " ");
                var category = str.toLowerCase();
                angular.forEach($scope.allAccessories, function(key, value) {
                    if (key.accessory_category_name.toLowerCase().indexOf(category) == -1) {
                        tempArray.push(key);
                    }
                });
                $scope.accessoriesToShow = tempArray;
            }
        }
    }
    $scope.enquire = function() {
        // $location.path('/accessoriesEnquiry');
           if ($sessionStorage.DHuserSession) {
            $location.path('/accessoriesEnquiry');
        } else {
            callnotify('Please log in to Enquire.', 'information')
        }
    }
    $localStorage.accessory_List = [];
    $localStorage.accessoryNameList = [];

    function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }
    $scope.isAccessoriesSelected = function(id, name) {
        if ((id) && (name)) {
            if (isInArray(id, $localStorage.accessory_List) == false) {
                $localStorage.accessory_List.push(id);
            } else {
                var index = $localStorage.accessory_List.indexOf(id);
                if (index > -1) {
                    $localStorage.accessory_List.splice(index, 1);
                }
            }
            if (isInArray(name, $localStorage.accessoryNameList) == false) {
                $localStorage.accessoryNameList.push(name);
            } else {
                var index = $localStorage.accessoryNameList.indexOf(name);
                if (index > -1) {
                    $localStorage.accessoryNameList.splice(index, 1);
                }
            }
            if ($localStorage.accessory_List.length >= 1) {
                $scope.showEnqueryButton = true;
            } else {
                $scope.showEnqueryButton = false;
            }
        }
    }
});