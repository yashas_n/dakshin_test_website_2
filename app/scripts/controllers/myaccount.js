'use strict';
angular.module('dakshinHondaWebsiteApp').controller('MyaccountCtrl', function($scope, $sessionStorage, $http, growl, $location, $window, $rootScope, $filter, $localStorage) {
    window.scrollTo(0, 0);
    $scope.image = {};
    $scope.image.file = {};
    $scope.sendData = {};
    $scope.sendData.user = {};
    $scope.userForm = {};
    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat
    }
    var dat = new Date();
    $scope.maxDate = dat.addDays(1).toDateString();
    if ($sessionStorage.DHuserSession) {
        $scope.sendData = $sessionStorage.DHuserSession;
        $scope.sendData.profile.dob = $filter('date')(new Date($scope.sendData.profile.dob), 'dd-MM-yyyy');
        $scope.sendData.profile.marriage_anniversary_date = $filter('date')(new Date($scope.sendData.profile.marriage_anniversary_date), 'dd-MM-yyyy')
        if ($localStorage.isSocial == 1) {
            $scope.isSocial = 1;
            if ($localStorage.provider == "google") {
                var img = $localStorage.socialImage;
                var res = img.split("?");
                $scope.displayUserImage = res[0];
                $localStorage.socialImage = res[0]
            } else {
                $scope.displayUserImage = $localStorage.socialImage
            }
        } else if ($sessionStorage.DHuserSession.profile.profile_image.url == null && $localStorage.isSocial == 0) {
            $scope.isSocial = 0;
            $scope.showEditButton = !0;
            $scope.displayUserImage = "icons/Profile_Picture.png"
        } else if ($sessionStorage.DHuserSession.profile.profile_image.url != null) {
            $scope.isSocial = 0;
            $scope.showEditButton = !0;
              
            $scope.displayUserImage = api_url + $sessionStorage.DHuserSession.profile.profile_image.url + "?" + $scope.sendData.profile.updated_at;
        } else {
            $scope.isSocial = 0;
            $scope.showEditButton = !0;
            $scope.displayUserImage = "icons/Profile_Picture.png"
        }
    } else {
        $location.path('/')
    }

    function updateAllData() {
        $http.get(api_url + "/website/profiles/" + $localStorage.profile_ID + "?auth_token=" + $scope.sendData.authentication_token).then(function(success) {
            console.log( (success));
            $sessionStorage.DHuserSession.profile = success.data.profile;
            $scope.DHuserData = $sessionStorage.DHuserSession;
            setTimeout(function() {
                window.location.reload()
            }, 2000)
        }, function(error) {
            console.log(JSON.stringify(error))
        })['finally'](function() {
            $scope.test = !1
        })
    }
    $scope.submitProfile = function() {
        if ($localStorage.isSocial == 1) {
            if ($scope.userForm.$valid) {
                sendProfile()
            }
        } else {
            if ($scope.userForm.$valid) {
                sendProfile()
            }
        }
    } 

    $scope.updateImage=function() {
        console.log("asdsf");
        var input = document.getElementById("uploadImage");
        var fReader = new FileReader();
        fReader.readAsDataURL(input.files[0]);
        fReader.onloadend = function(event) {
            var img = document.getElementById("profileImage");
            img.src = event.target.result;
            // localStorage.setItem("temp_profile_img", img.src);
            $scope.temp_profileImg=img.src;
            // localStorage.setItem("imagechanged", "yes");
        }
    }
 
    function sendProfile() {
        callnotify('Updating your profile please wait..', 'information');
        $scope.sendData.profile.profile_image=$scope.temp_profileImg;
        $scope.dataToSend = {};
        $scope.dataToSend.profile= $scope.sendData.profile;
        console.log(($scope.dataToSend),"prof id",$localStorage.profile_ID);
        console.log("$scope.DHuserData",$scope.DHuserData);
        $http.put(api_url + "/website/profiles/" + $localStorage.profile_ID + "?auth_token=" + $scope.sendData.authentication_token, $scope.dataToSend).then(function(success) {
          console.log(success);
            callnotify('Profile updated..', 'success');

            setTimeout(function() {
                updateAllData()
            }, 1000) 
        }, function(error) {
            console.log(JSON.stringify(error))
        })['finally'](function() {
            $scope.test = !1
        })
    }
     


})