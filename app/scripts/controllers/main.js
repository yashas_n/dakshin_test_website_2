'use strict';
angular.module('dakshinHondaWebsiteApp').controller('MainCtrl', function ($scope, localStorageService, $location, $http, growl, $mdDialog, $sessionStorage, $rootScope, $window, $interval, $timeout, $localStorage) {
    window.scrollTo(0, 0);
    $scope.taxiCarsList = {};


    $scope.youtubeurl = '';
    $scope.showYoubeFrame = false;

    $http.get(api_url + "/website/cars").then(function (success) {
        $scope.carsList = success.data.cars;
    }, function (error) { })['finally'](function () { });
    $localStorage.DHpaymentdueid = '';
    var param2 = $location.search().payment;
    if (param2 == 1) {
        callnotify("please wait..., Don't refresh", 'information');
        var paymentcheck = $sessionStorage.DHpaymentdetails;
        if (paymentcheck) {
            var paymenttrans = {
                "phone": paymentcheck.phone,
                "transaction_id": paymentcheck.transaction_id,
                "checksum": paymentcheck.checksum
            };
            var apptoken = $sessionStorage.DHpaymentapptoken;
            $http.post("https://partners.vpaynow.com/web_check_transaction", paymenttrans, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-App-Token': apptoken
                }
            }).then(function (success) {
                var result = success.data;
                var updatestatus = {
                    "status_code": result.request.code,
                    "payment_id": $sessionStorage.DHpaymentid,
                    "status": result.status
                };
                $http.post(api_url + "/website/update_transaction_status", updatestatus).then(function (success) {
                    $location.path('/mypayments');
                    return !1;
                }, function (error) {
                    callnotify("Payment Update Failed", 'error');
                    $location.path('/mypayments');
                })['finally'](function () {
                    $scope.test = !1;
                });
            }, function (error) {
                callnotify("Payment Update Failed", 'error');
                $location.path('/mypayments');
            })['finally'](function () {
                $scope.test = !1;
            });
        }
        $location.search('payment', null);
    }
    $scope.quantity = 6;
    angular.element($window).bind('resize', function () {
        if ($window.innerWidth > 991) {
            $scope.quantity = 6;
        }
        if ($window.innerWidth > 767 && $window.innerWidth < 992) {
            $scope.quantity = 4;
        }
        if ($window.innerWidth < 768) {
            $scope.quantity = 3;
        }
    });
    $scope.goHome = function () {
        $location.path("/");
    };
    if ($sessionStorage.DHuserSession) {
        $scope.DHuserData = $sessionStorage.DHuserSession;
        $scope.displayUserName = $scope.DHuserData.name;
    }
    $rootScope.$on('afterLogin', function (event, data) {
        $scope.DHuserData = data;
        $scope.displayUserName = $scope.DHuserData.name;
        localStorageService.set('activeUserId', $scope.DHuserData.id);
        localStorageService.set('activeUserData', $scope.DHuserData);
    });
    $scope.apiUrl = api_url;
    $rootScope.car_List = [];

    function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }
    $scope.isCarSelected = function (carId) {
        if (carId) {
            if (isInArray(carId, $rootScope.car_List) == !1) {
                $rootScope.car_List.push(carId);
            } else {
                var index = $rootScope.car_List.indexOf(carId);
                if (index > -1) {
                    $rootScope.car_List.splice(index, 1);
                }
            }
            if ($rootScope.car_List.length >= 2 && $rootScope.car_List.length <= 3) {
                $scope.showCompareButton = !0;
            } else {
                if ($rootScope.car_List.length > 3) {
                    callnotify("Sorry, Maximum 3 cars can be compared.", 'information');
                }
                $scope.showCompareButton = !1;
            }
        }
    };
    $scope.goToCompare = function () {
        $rootScope.query = [];
        $scope.temp = {};
        for (var i = 0; i < $rootScope.car_List.length; i++) {
            $scope.temp.car_id = $rootScope.car_List[i];
            $rootScope.query.push($scope.temp);
            $scope.temp = {};
        }
        $location.path('/carcompare');
    };
    $scope.servicebooking = function () {
        $location.path('/servicebooking');
    };
    $scope.salesinquiry = function () {
        $location.path('/enquiry');
    };
    $scope.goToPayments = function () {
        if ($sessionStorage.DHuserSession) {
            $location.path('/manualpayment');
        } else {
            callnotify('Please log in to make payments.', 'information');
        }
    };
    $scope.insurance = function () {
        $location.path('/insurance');
    };
    $scope.testDriveServices = function () {
        $location.path('/testDrive');
    };
    $scope.emicalculatorServices = function () {
        $location.path('/emicalculator');
    };
    $scope.onRoadPriceQuoteServices = function () {
        $location.path('/onRoadPriceQuote');
    };
    $scope.feedback = function () {
        $location.path('/feedback');
    };
    $scope.contactUsServices = function () {
        $location.path('/contactUs');
    };
    $scope.goLocateUs = function () {
        $location.path("locateus");
        window.scrollTo(0, 0);
    };
    $scope.bannersList = {};
    $scope.carsList = {};
    $scope.usedcarsList = {};
    $scope.dealerDetails = {};
    var tempImageList = [];
    var tempTestimonialList = [];
    $scope.currentIndex = 0;
    $scope.setCurrentSlideIndex = function (index) {
        $scope.currentIndex = index;
    };
    // $('.imgslider').img({
    //     interval: 2000
    //   });
    var timer;
    var sliderFunc = function() {
        timer = $timeout(function() {
            $scope.prevSlide();
            timer = $timeout(sliderFunc, 5000);
        }, 6000);
    };
    sliderFunc();
    $scope.$on('$destroy', function() {
        $timeout.cancel(timer);
    });
    $scope.isCurrentSlideIndex = function (index) {
        return $scope.currentIndex === index;   
      
    };
    $scope.prevSlide = function () {
        $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
    };
    $scope.nextSlide = function () {
        $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
    };
    $http.get(api_url + "/website/web_banners").then(function (success) {
        console.log("success.data.web_banners",success.data.web_banners);
        $scope.bannersList = success.data.web_banners;
         
        angular.forEach($scope.bannersList, function (key, value) {
          
            tempImageList.push({
                'image': api_url + key.image.url,
                'description': "img" + value,
                'number': value + 1,
                'redirect_to':key.redirect_to,
                'car_id':key.car_id,
                'display_order':key.display_order,
                'video': key.video,
                'video_url': key.video_url,
                'button_text': key.button_text,
                'button_color': key.button_color,
 
            });
        });
        $scope.slides = tempImageList;
         console.log($scope.slides);
    }, function (error) { })['finally'](function () {
        bannerDataLoaded();
    });



    $scope.setVideo = function(ev, url, growl)

    {

        $rootScope.videoUrl = url;

        $mdDialog.show({

          controller: youtubeController,

          templateUrl: 'views/video.tmpl.html',

          parent: angular.element(document.body),

          targetEvent: ev,

          clickOutsideToClose:true,

          fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.

        })

        .then(function(answer) {

          //$scope.status = 'You said the information was "' + answer + '".';

        }, function() {

          //$scope.status = 'You cancelled the dialog.';

        });

    };



    function youtubeController($scope, $mdDialog, $sessionStorage, growl, $sce) 

        {



        $scope.youtubeurl = $sce.trustAsResourceUrl($rootScope.videoUrl);

        $scope.closePriceModal = function(){

          $mdDialog.cancel({});

        };



    }



    $scope.goToCars = function (option) {
        localStorage.setItem("carOption", option);
    };
    $scope.filter = function () {
        $location.path('/preused_cars_filter');
    };
    $scope.cardetails = function (id) {
        localStorageService.set('car_id', id);
        $location.path('/cardetails');
    };
    $scope.carenquire = function (id) {
        localStorageService.set('car_id', id);
        $location.path('/usedcarenquiryform');
    };
    $scope.userInfo = function (ev, data) {
        $rootScope.usedCarData = data;
        $mdDialog.show({
            controller: userInfoController,
            templateUrl: 'views/used_car_info.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: !0,
            fullscreen: $scope.customFullscreen
        }).then(function (answer) { }, function () { });
    };

    function userInfoController($scope, $mdDialog, $sessionStorage) {
        $scope.usedCarModal = {};
        $scope.usedCarModal = $rootScope.usedCarData;
        $scope.closeCarModal = function () {
            $mdDialog.cancel({});
        };
    }

    $scope.gotourl=function(id,redirect_to){
       console.log(id,redirect_to);

        // $location.path(url);
        // window.open(url, '_self');
    if(id!=null && redirect_to=='testdrive'){
        // localStorageService.set('car_id', id);
        // $location.path('/cardetails');
        $rootScope.frombannerData=id;
        $location.path('/testDrive');
            
    } 
    if(id!=null && redirect_to=='cardetails'){
        localStorageService.set('car_id', id);
        $location.path('/cardetails');
    }
    if(id!=null && redirect_to=='contactus'){
        localStorageService.set('car_id', id);
        $location.path('/contactUs');
    }

    };
    $scope.goToAlltestimonial = function () {
   
        $location.path('/viewalltestimonial');
    };
    // https://dakshin-backend.myridz.com/web/social_media_links
  
    $http.get(api_url + "/website/social_media_links").then(function (success) {
       console.log("suc in socila media",success);
       $scope.social_media=success.data.social_media_links;
    }, function (error) {
        console.log("suc in socila media",error);
    })['finally'](function () {
         
    });
});