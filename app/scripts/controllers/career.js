angular.module('dakshinHondaWebsiteApp').controller('careerCtrl', function($localStorage, $scope, $location, $http, growl, $sessionStorage, $rootScope, localStorageService) {
    window.scrollTo(0, 0);
    $scope.years = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "10+"];
    $scope.months = ["0", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    $scope.careerData = {};
    $scope.jobDetails = $localStorage.jobDetails;
    console.log(JSON.stringify($scope.jobDetails));
    $scope.careerData.job_id = $scope.jobDetails.id;

    function updateCareerData() {
        $scope.careerData.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.careerData.email = $sessionStorage.DHuserSession.profile.email;
        $scope.careerData.mobile = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $scope.careerData.user_id = $sessionStorage.DHuserSession.profile.id;
        $scope.hideLogin = !0;
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token
    }
    if ($sessionStorage.DHuserSession) {
        updateCareerData()
    }
    $rootScope.$on('afterLogin', function(event, data) {
        updateCareerData()
    });
    $scope.sendCv = function() {
        $scope.fileType = $scope.image.filetype.substr($scope.image.filetype.indexOf("/") + 1);
        
        $scope.careerData.cv_file = "data:application/" + $scope.fileType + ";base64,(" + $scope.image.base64 + ")";
        $scope.careerData.file_type= $scope.fileType;
        console.log(JSON.stringify($scope.careerData));
        if ($scope.userForm.$valid) {
            sendCareerForm()
        }
    }

    function sendCareerForm() {
        
        callnotify('Submitting, please wait...', 'information');
        $http.post(api_url + "/website/careers", JSON.stringify({
            "career": $scope.careerData
        })).then(function(success) {
            callnotify('Submitted!', 'success');
            $location.path('/careers_listing')
        }, function(error) {
            console.log(JSON.stringify(error))
        })['finally'](function() {})
    }
})