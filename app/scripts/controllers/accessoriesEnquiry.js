'use strict';
angular.module('dakshinHondaWebsiteApp').controller('AccessoriesEnquiryCtrl', function($localStorage, $scope, $location, $http, growl, $mdDialog, $sessionStorage, $rootScope, localStorageService) {
    window.scrollTo(0, 0);
    $scope.wishlist = {}; /*-----------------------------------------Get Enquire Fields---------------------------------*/
    $scope.enquireItemDataIds = $localStorage.accessory_List;
    $scope.enquireItemData = $localStorage.accessoryNameList;
    $scope.model = $localStorage.accessoriesCarName;
    var accessoriesItemArray = [];
    angular.forEach($scope.enquireItemData, function(key, value) {
        accessoriesItemArray.push({
            'name': key,
            'id': $scope.enquireItemDataIds[value]
        })
    });
    $scope.accessoriesItemToShow = accessoriesItemArray;
    $scope.remove = function(index) {
        console.log(index,"********",$scope.accessoriesItemToShow);
        if($scope.accessoriesItemToShow.length>1){
        $scope.accessoriesItemToShow.splice(index, 1);
    } else{
        callnotify('Atleast one item is needed for Enquiry', 'information');
        return (0);
    }
    } /*-----------------------------------------Form functions---------------------------------*/
    $scope.contactLoginData = {};
 
    function updateEnquire() {
        $scope.wishlist.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.wishlist.email = $sessionStorage.DHuserSession.profile.email;
        $scope.wishlist.mobile = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $scope.wishlist.user_id = $sessionStorage.DHuserSession.profile.id;
        $scope.hideLogin = true;
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token;
    }
    if ($sessionStorage.DHuserSession) {
        updateEnquire();
    }
    $rootScope.$on('afterLogin', function(event, data) {
        updateEnquire();
    });
    $scope.enquireRequest = function() {
        if ($scope.userForm.$valid) {
//                         var myLink = document.getElementById('enquireRequest');
// var google_conversion_id = 825753188;
// var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
// var google_remarketing_only = false;
// myLink.onclick = function(){

//     var script = document.createElement("script");
//     script.type = "text/javascript";
//     script.src = "//www.googleadservices.com/pagead/conversion.js"; 
//     document.getElementsByTagName("head")[0].appendChild(script);
//     return false;

// }
            enquireWishlist();
        }
    }

    function enquireWishlist() {
        $scope.dataToSend = {};
        var idArray = [];
        angular.forEach($scope.accessoriesItemToShow, function(key, value) {
            idArray.push(key.id)
        });
        console.log("idArray",idArray);
     
        $scope.wishlist.accessory_id = idArray;
        $scope.wishlist.mobile_number=$scope.wishlist.mobile;
        $scope.wishlist.user_id= $localStorage.user_ID;
        callnotify('Requesting, please wait...', 'information');
        
        $scope.dataToSend.accessory_enquiry = $scope.wishlist;
        console.log(JSON.stringify($scope.dataToSend));
        $http.post(api_url + "/website/accessory_enquiries", $scope.dataToSend).then(function(success) {
            callnotify('Enquiry Submitted, we will get back soon.', 'success');
            $location.path('/accessories');
        }, function(error) {
            console.log(JSON.stringify(error));
        })['finally'](function() {});
    }
});