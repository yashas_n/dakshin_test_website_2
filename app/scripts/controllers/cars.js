'use strict';
angular.module('dakshinHondaWebsiteApp').controller('CarsCtrl', function($scope, localStorageService, $location, $http, growl, $rootScope, $localStorage) {
    window.scrollTo(0, 0);
    $scope.carsList = {};
    $scope.usedcarsList = {};
    $scope.showCompareButton = !1;
    $scope.dataLoaded = !1;
    var carOption = localStorage.getItem("carOption", "");
    setTimeout(function() {
        if (carOption == 'new') {
            openCityDirect(carOption, 'newCar')
        } else if (carOption == 'old') {
            openCityDirect(carOption, 'usedCar')
        } else {
            openCityDirect(carOption, 'taxiCar')
        }
    }, 200);
    $scope.taxiCarsList = {};
    $http.get(api_url + "/website/cars").then(function(success) {
        $scope.carsList = success.data.cars;
        var taxiCarsListarr = [];
        angular.forEach($scope.carsList, function(key, value) {
            if (key.nexa == !0) {
                taxiCarsListarr.push(key)
            }
        });
        $scope.taxiCarsList = taxiCarsListarr
    }, function(error) {})['finally'](function() {
        $scope.dataLoaded = !0;
        pageDataLoaded()
    });
    $scope.test = !0;
    $scope.message = 'Loading Cars...';
    $http.get(api_url + "/website/used_cars").then(function(success) {
        $scope.usedcarsList = success.data.used_cars;
        $scope.usedcarsPresent = $scope.usedcarsList.length
    }, function(error) {})['finally'](function() {
        $scope.test = !1
    });
    $scope.cardetails = function(id, isTaxi) {
        localStorageService.set('isTaxi', isTaxi);
        localStorageService.set('car_id', id);
        $location.path('/cardetails')
    }
    $scope.enquireUsedCar = function(car) {
        $localStorage.usedCarToEnq = car;
        $location.path('/usedcarenquiryform')
    }
    $rootScope.car_List = [];

    function isInArray(value, array) {
        return array.indexOf(value) > -1
    }
    $scope.isCarSelected = function(carId) {
        if (carId) {
            if (isInArray(carId, $rootScope.car_List) == !1) {
                $rootScope.car_List.push(carId)
            } else {
                var index = $rootScope.car_List.indexOf(carId);
                if (index > -1) {
                    $rootScope.car_List.splice(index, 1)
                }
            }
            if ($rootScope.car_List.length >= 2 && $rootScope.car_List.length <= 3) {
                $scope.showCompareButton = !0
            } else {
                if ($rootScope.car_List.length > 3) {
                    callnotify("Maximum 3 cars can be compared.", 'warning')
                }
                $scope.showCompareButton = !1
            }
        }
    }
    $scope.goToCompare = function() {
        $rootScope.query = [];
        $scope.temp = {};
        for (var i = 0; i < $rootScope.car_List.length; i++) {
            $scope.temp.car_id = $rootScope.car_List[i];
            $rootScope.query.push($scope.temp);
            $scope.temp = {}
        }
        $location.path('/carcompare')
    }
})