'use strict';
angular.module('dakshinHondaWebsiteApp').controller('FeedbackCtrl', function($scope, $http, growl, $location, localStorageService, $sessionStorage, $rootScope) {
    window.scrollTo(0, 0);
    $scope.hideLogin = !1;
    $scope.testDrivebooking = function() {
        $location.path('/testDrive')
    }
    $scope.servicebooking = function() {
        $location.path('/servicebooking')
    }
    $scope.insurancebooking = function() {
        $location.path('/insurance')
    }
    $scope.emicalculatorServices = function() {
        $location.path('/emicalculator')
    }
    $scope.onRoadPriceQuoteServices = function() {
        $location.path('/onRoadPriceQuote')
    }
    $scope.feedback = function() {
        $location.path('/feedback')
    }
    $scope.contactUsServices = function() {
        $location.path('/contactUs')
    }
    $scope.maplocation = {};
    $scope.changedealers = function() {
        var formtype = $scope.feedbackData.feedback_type;
        if (formtype == "Sales" || formtype == "Service") {
            $scope.formtypefilter = {
                "city": 'Bengaluru',
                "type": formtype
            };
            console.log(JSON.stringify($scope.formtypefilter));
            $http.post(api_url + "/mobile/dealer_type_location", $scope.formtypefilter).then(function(success) {
                console.log(JSON.stringify(success));
                $scope.dealerList = success.data.dealers
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {})
        } else if (formtype == "Others") {
            $scope.maplocation = {
                "city": 'Bengaluru'
            };
            console.log(JSON.stringify($scope.maplocation));
            $http.post(api_url + "/mobile/dealer_location", $scope.maplocation).then(function(success) {
                console.log(JSON.stringify(success));
                $scope.dealerList = success.data.dealers
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {})
        }
    };
    $scope.feedbackData = {};

    function updateContactData() {
        $scope.feedbackData.name = $sessionStorage.DHuserSession.profile.full_name;
        $scope.feedbackData.email = $sessionStorage.DHuserSession.profile.email;
        $scope.feedbackData.mobile = parseInt($sessionStorage.DHuserSession.profile.mobile);
        $scope.hideLogin = !0;
        $http.defaults.headers.common['X-USER-TOKEN'] = $sessionStorage.DHuserSession.authentication_token
    }
    if ($sessionStorage.DHuserSession) {
        updateContactData()
    }
    $rootScope.$on('afterLogin', function(event, data) {
        updateContactData()
    });
    $scope.submitFeedback = function() {
        $scope.dataToSend = {};
        if ($scope.userForm.$valid) {
//                         var myLink = document.getElementById('feedback');
// var google_conversion_id = 825753188;
// var google_conversion_label = "SdPiCIux9XkQ5PzfiQM";
// var google_remarketing_only = false;
// myLink.onclick = function(){

//     var script = document.createElement("script");
//     script.type = "text/javascript";
//     script.src = "//www.googleadservices.com/pagead/conversion.js"; 
//     document.getElementsByTagName("head")[0].appendChild(script);
//     return false;

// }
            $scope.dataToSend.feedback = $scope.feedbackData;
            callnotify('Submitting feedback, please wait...', 'information');
            $http.post(api_url + "/website/feedbacks", $scope.dataToSend).then(function(success) {
                callnotify('Feedback Submitted!', 'success');
                $location.path('/main')
            }, function(error) {
                console.log(JSON.stringify(error))
            })['finally'](function() {
                $scope.test = !1
            })
        } else {
            callnotify('Form Incomplete!!', 'error')
        }
    }
})