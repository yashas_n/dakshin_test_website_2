'use strict';
angular.module('dakshinHondaWebsiteApp').controller('LocateUsCtrl', function($scope, localStorageService, $location, $http, growl, $mdDialog, $sessionStorage, $rootScope, $window, $interval, $timeout, $localStorage) {
    window.scrollTo(0, 0);
    $scope.dealersToShow = {};
    $scope.facilityToShow = {};

    function isInArray(value, array) {
        return array.indexOf(value) > -1
    }
    $scope.maplocation = {
        "city": 'Bengaluru'
    };
    $http.post(api_url + "/mobile/dealer_location", $scope.maplocation).then(function(success) {
        console.log("suces",success.data);
        $scope.dealerDetails = success.data.dealers;
        var tempdealerDetailsList = [];
        var tempfacilityList = [];
        angular.forEach($scope.dealerDetails, function(key, value) {
            tempdealerDetailsList.push(key);
            for (var i = 0; i < key.dealer_type.length; i++) {
                if (isInArray(key.dealer_type[i], tempfacilityList) == !1) {
                    tempfacilityList.push(key.dealer_type[i])
                }
            }
        });
        $scope.facilityToShow = tempfacilityList;
        $scope.dealerDetailsListFinal = tempdealerDetailsList;
        initialize($scope.dealerDetailsListFinal);
        $scope.dealersToShow = $scope.dealerDetailsListFinal
    }, function(error) {})['finally'](function() {
        $scope.test = !1
    });
    $scope.changeMap = function(nameData, newLat, newLng, myId) {
        $scope.mapData = nameData;
        var newLocation = new google.maps.LatLng(newLat, newLng);
        map.setCenter(newLocation);
        map.setZoom(16)
    }
    $scope.facilityChange = function() {
        $scope.dealerDetails = {};
        if ($scope.facility == 'all') {
            $scope.dealerDetails = $scope.dealerDetailsListFinal
        } else {
            $scope.dealerDetails = $scope.dealerDetailsListFinal;
            var tempArray = [];
            angular.forEach($scope.dealerDetailsListFinal, function(key, value) {
                if (isInArray($scope.facility, key.dealer_type) == !0) {
                    tempArray.push(key)
                }
            });
            $scope.dealerDetails = tempArray
        }
        initialize($scope.dealerDetails)
    }
    $scope.changeLocation = function() {
        $scope.dealerDetails = {};
        if ($scope.location == 'all') {
            $scope.dealerDetails = {};
            $scope.dealerDetails = $scope.dealerDetailsListFinal
        } else {
            var tempArray = [];
            $scope.dealerDetails = $scope.dealerDetailsListFinal;
            angular.forEach($scope.dealerDetailsListFinal, function(key, value) {
                if (key.id == $scope.location) {
                    $scope.changeMap(key.dealer_name, key.lat, key.lon, key.id);
                    tempArray.push(key);
                    expandSpecific(key.id)
                }
            });
            $scope.dealerDetails = tempArray
        }
        initialize($scope.dealerDetails)
    }
    var geocoder;
    var map;
    var bounds = new google.maps.LatLngBounds();

    function initialize(dealerDetailsList) {
        $scope.dealerDetails = dealerDetailsList;
        var locations = [];
        angular.forEach($scope.dealerDetails, function(key, value) {
            locations.push([key.dealer_name, key.lat, key.lon, value + 1])
        });
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: new google.maps.LatLng(12.9279, 77.6271),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: !1,
            zoomControl: !0,
            scaleControl: !0,
            overviewMapControl: !0,
            disableDefaultUI: !0,
            streetViewControl: !1
        });
        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });
            google.maps.event.addListener(marker, 'mousedown', (function(marker, i) {
                return function() {
                    var popDealerID = locations[i][5];
                    var tempText = JSON.stringify({
                        arr: locations[i]
                    });
                    infowindow.setContent("<p style='padding: 20px;font-size:16px;'> <strong>DAKSHIN HONDA<br> </strong> " + locations[i][0] + "</p>");
                    var marklatLng = marker.getPosition();
                    map.setCenter(marklatLng);
                    infowindow.open(map, marker)
                }
            })(marker, i))
        }
    }
    $scope.filterEmail = function(items) {
        var result = {};
        var temp = [];
        var emails = items[0];
        emails = emails.split(",");
        for (var i = 0; i < emails.length; i++) {
            temp.push({
                'email': emails[i]
            })
        }
        result = temp;
        return result
    }
    $scope.filterAddress = function(data) {
        var result = {};
        var temp = [];
        var address = data;
        address = address.split(",");
        for (var i = 0; i < address.length; i++) {
            temp.push({
                'add': address[i]
            })
        }
        result = temp;
        return result
    }
    $scope.filterWorkingHours = function(data) {
        var result = '';
        var time = data;
        time = time.split(",");
        result = time[0] + " AM to " + time[1] + " PM";
        return result
    }
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService();
    var geocoder = new google.maps.Geocoder();
    $scope.openMap = function(lat, lon) {
        $scope.directions = {
            origin: new google.maps.LatLng(lat, lon),
            destination: new google.maps.LatLng(lat, lon)
        }
        var request = {
            origin: new google.maps.LatLng(lat, lon),
            destination: $scope.directions.destination,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                window.open('http://maps.google.com/maps?daddr=' + $scope.directions.destination + '&mode=driving', '_blank')
            } else {}
        })
    }
})