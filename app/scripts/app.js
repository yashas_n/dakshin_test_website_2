'use strict';
var myapp = angular.module('dakshinHondaWebsiteApp.controllers', [])
angular.module('dakshinHondaWebsiteApp', ['ngAnimate', 'ngAria', 'ngCookies', 'ngMessages', 'ngResource', 'ngRoute', 'ngSanitize', 'ngLoader', 'angular-growl', 'SlideViewer', 'ngMaterial', 'socialLogin', 'ngStorage', 'LocalStorageModule', 'angular.filter', 'ui.bootstrap', 'bootstrapLightbox', 'rzModule', 'ur.file', 'naif.base64', '720kb.datepicker', 'picardy.fontawesome']).config(function($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/main.html?v1.29',
        cache: !1,
        controller: 'MainCtrl'
    }).when('/cars', {
        templateUrl: 'views/cars.html?v1.29',
        cache: !1,
        controller: 'CarsCtrl'
    }).when('/cardetails', {
        templateUrl: 'views/cardetails.html?v1.29',
        cache: !1,
        controller: 'CardetailCtrl'
    }).when('/preused_cars_filter', {
        templateUrl: 'views/preused_cars_filter.html?v1.29',
        cache: !1,
        controller: 'Preused_filterCtrl'
    }).when('/servicebooking', {
        templateUrl: 'views/servicebooking.html?v1.29',
        cache: !1,
        controller: 'servicebookingCtrl'
    }).when('/usedcarenquiryform', {
        templateUrl: 'views/usedcarenquiryform.html?v1.29',
        cache: !1,
        controller: 'usedcarenquiryCtrl'
    }).when('/emicalculator', {
        templateUrl: 'views/emicalculator.html?v1.29',
        cache: !1,
        controller: 'emicalculatorCtrl'
    }).when('/onRoadPriceQuote', {
        templateUrl: 'views/onRoadPriceQuote.html?v1.29',
        cache: !1,
        controller: 'onRoadPriceQuoteCtrl'
    }).when('/locateus', {
        templateUrl: 'views/locateus.html?v1.29',
        cache: !1,
        controller: 'LocateUsCtrl'
    }).when('/insurance', {
        templateUrl: 'views/insurance.html?v1.29',
        cache: !1,
        controller: 'insuranceCtrl'
    }).when('/accessories', {
        templateUrl: 'views/accessories.html?v1.29',
        cache: !1,
        controller: 'AccessoriesCtrl'
    }).when('/services', {
        templateUrl: 'views/services.html?v1.29',
        cache: !1,
        controller: 'ServicesCtrl'
    }).when('/contactUs', {
        templateUrl: 'views/contactus.html?v1.29',
        controller: 'ContactusCtrl'
    }).when('/aboutus', {
        templateUrl: 'views/aboutus.html?v1.29',
        cache: !1,
        controller: 'AboutusCtrl'
    }).when('/privacypolicy', {
        templateUrl: 'views/privacy_policy.html?v1.29',
        cache: !1,
        controller: 'AboutusCtrl'
    }).when('/carcompare', {
        templateUrl: 'views/carcompare.html?v1.29',
        cache: !1,
        controller: 'CarcompareCtrl'
    }).when('/services', {
        templateUrl: 'views/services.html?v1.29',
        cache: !1,
        controller: 'AboutusCtrl'
    }).when('/homePage', {
        templateUrl: 'views/homepage.html?v1.29',
        cache: !1,
        controller: 'HomepageCtrl'
    }).when('/testDrive', {
        templateUrl: 'views/testdrive.html?v1.29',
        cache: !1,
        controller: 'TestdriveCtrl'
    }).when('/myaccount', {
        templateUrl: 'views/myaccount.html?v1.29',
        cache: !1,
        controller: 'MyaccountCtrl'
    }).when('/myprofile', {
        templateUrl: 'views/myprofile.html?v1.29',
        cache: !1,
        controller: 'MyprofileCtrl'
    }).when('/mycars', {
        templateUrl: 'views/mycars.html?v1.29',
        cache: !1,
        controller: 'MyCarsCtrl'
    }).when('/head', {
        templateUrl: " ",
        cache: !1,
        controller: 'headCtrl'
    }).when('/addcar', {
        templateUrl: " ",
        cache: !1,
        controller: 'addcarCtrl'
    }).when('/testimonialpage', {
        templateUrl: 'views/testimonialpage.html?v1.29',
        cache: !1,
        controller: 'testimonialPageCtrl'
    }).when('/viewalltestimonial', {
        templateUrl: 'views/viewalltestimonial.html?v1.29',
        cache: !1,
        controller: 'viewalltestimonialCtrl'
    }).when('/ContactUs', {
        templateUrl: 'views/contactus.html?v1.29',
        cache: !1,
        controller: 'ContactusCtrl'
    }).when('/careerform', {
        templateUrl: 'views/careerform.html?v1.29',
        cache: !1,
        controller: 'careerCtrl'
    }).when('/careers_listing', {
        templateUrl: 'views/careers_listing.html?v1.29',
        cache: !1,
        controller: 'careerListCtrl'
    }).when('/offers', {
        templateUrl: 'views/offers.html?v1.29',
        cache: !1,
        controller: 'offersCtrl'
    }).when('/feedback', {
        templateUrl: 'views/feedback.html?v1.29',
        cache: !1,
        controller: 'FeedbackCtrl'
    }).when('/accessoriesCarList', {
        templateUrl: 'views/accessoriescarlist.html?v1.29',
        cache: !1,
        controller: 'AccessoriescarlistCtrl'
    }).when('/accessoriesEnquiry', {
        templateUrl: 'views/accessoriesEnquiry.html?v1.29',
        cache: !1,
        controller: 'AccessoriesEnquiryCtrl'
    }).when('/enquiry', {
        templateUrl: 'views/Enquiry.html?v1.29',
        cache: !1,
        controller: 'EnquiryCtrl'
    }).when('/manualpayment', {
        templateUrl: 'views/manualpayment.html?v1.29',
        cache: !1,
        controller: 'manualpaymentCtrl'
    }).when('/payment', {
        templateUrl: 'views/payment.html?v1.29',
        cache: !1,
        controller: 'paymentCtrl'
    }).when('/mypayments', {
        templateUrl: 'views/mypayments.html?v1.29',
        cache: !1,
        controller: 'mypaymentsctrl'
    }).when('/paymentdue', {
        templateUrl: 'views/paymentdue.html?v1.29',
        cache: !1,
        controller: 'paymentduectrl'
    }).when('/paymentdetails', {
        templateUrl: 'views/paymentdetails.html?v1.29',
        cache: !1,
        controller: 'paymentdetailsctrl'
    }).otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode({
        enabled: !0,
        requireBase: !1
    })
}).config(['growlProvider', function(growlProvider) {
    growlProvider.globalPosition('bottom-right');
    growlProvider.globalTimeToLive(3000)
}]).animation('.slide-animation', function() {
    return {
        addClass: function(element, className, done) {
            if (className == 'ng-hide') {
                TweenMax.to(element, 0.5, {
                    left: -element.parent().width(),
                    onComplete: done
                })
            } else {
                done()
            }
        },
        removeClass: function(element, className, done) {
            if (className == 'ng-hide') {
                element.removeClass('ng-hide');
                TweenMax.set(element, {
                    left: element.parent().width()
                });
                TweenMax.to(element, 0.5, {
                    left: 0,
                    onComplete: done
                })
            } else {
                done()
            }
        }
    }
}).config(function(socialProvider) {
    socialProvider.setGoogleKey("765001305880-rguqegbic7vb4vv6e2dgi62u69hmk2on.apps.googleusercontent.com");
    socialProvider.setFbKey({
        appId: "1318833344830107",
        apiVersion: "v2.8"
    })
}).config(['$qProvider', function($qProvider) {
    $qProvider.errorOnUnhandledRejections(!1)
}]).directive('scrollToItem', function() {
    return {
        restrict: 'A',
        scope: {
            scrollTo: "@"
        },
        link: function(scope, $elm, attr) {
            $elm.on('click', function() {
                $('html,body').animate({
                    scrollTop: $(scope.scrollTo).offset().top
                }, "slow")
            })
        }
    }
}).filter('doSort', function() {
    return function(val) {
        if (val >= 10000000) {
            val = (val / 10000000).toFixed(2) + ' Cr'
        } else if (val >= 100000) {
            val = (val / 100000).toFixed(2) + ' Lacs'
        } else if (val >= 1000) {
            val = (val / 1000).toFixed(2) + ' K'
        }
        return val
    }
}).filter('locateHours', function() {
    return function(input) {
        if (input != null) {
            var x = input.split("PM", 2);
            return x[0] + 'PM' + x[1] + 'PM'
        }
    }
}).filter('replaceOperator', function() {
    return function(input) {
        if (input != null) {
            return input.replace("+", " ")
        }
    }
}).filter('sortCapacity', function() {
    return function(input) {
        if (input != null) {
            var x = input.replace('["', ' ');
            return x.replace('"]', ' ')
        }
    }
}).filter('sortJobs', function() {
    return function(input) {
        if (input != null) {
            var x = input.filter(Boolean);
            x = x.toString().replace(/-|\s/g, "");
            return x.replace(/,/, ',\n')
        }
    }
}).filter('sortFacility', function() {
    return function(input) {
        if (input != null) {
            var x = input.filter(Boolean);
            x = x.toString().replace(/-|\s/g, "");
            return x.replace(/,/, ',')
        }
    }
}).filter('seperateEmail', function() {
    return function(input) {
        console.log(input);
        if (input != null) {
            var x = input[0];
            x = x.split(",");
            console.log(x);
            return x
        }
    }
}).filter('req_skills', function() {
    return function(input) {
        if (input != null) {
            var x = input.replace(/-/g, '\n- ');
            return x
        }
    }
}).filter('updatedDate', function() {
    return function(input) {
        if (input != null) {
            var x = input.substring(0, 10);
            return x.split('-').reverse().join('-')
        }
    }
}).filter('rupees', function() {
    return function(number) {
        try {
            var x = number.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            return res
        } catch (e) {}
    }
})
myapp = angular.module("date", []).directive("datepicker", function() {
    return {
        restrict: "A",
        link: function(scope, el, attr) {
            el.datepicker({
                dateFormat: 'dd-mm-yy'
            })
        }
    }
}).directive('datetimez', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            element.datetimepicker({
                language: 'en',
                pickDate: !1,
            }).on('changeDate', function(e) {
                ngModelCtrl.$setViewValue(e.date);
                scope.$apply()
            })
        }
    }
})