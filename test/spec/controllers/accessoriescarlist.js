'use strict';

describe('Controller: AccessoriescarlistCtrl', function () {

  // load the controller's module
  beforeEach(module('dakshinHondaWebsiteApp'));

  var AccessoriescarlistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AccessoriescarlistCtrl = $controller('AccessoriescarlistCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AccessoriescarlistCtrl.awesomeThings.length).toBe(3);
  });
});
