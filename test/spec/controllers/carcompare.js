'use strict';

describe('Controller: CarcompareCtrl', function () {

  // load the controller's module
  beforeEach(module('dakshinHondaWebsiteApp'));

  var CarcompareCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CarcompareCtrl = $controller('CarcompareCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CarcompareCtrl.awesomeThings.length).toBe(3);
  });
});
